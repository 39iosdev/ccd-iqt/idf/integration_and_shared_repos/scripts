#!/usr/bin/python3
import argparse
import os
import subprocess
import re

branch_str = "git rev-parse --abbrev-ref HEAD"
git_str = "git push --set-upstream git@gitlab.com:90cos/cyv/test-archives/{workrole}/{eval_name}/{name}.git {branch}"
test_repo_path_desc = "The path to the repository (or local directory) with the questions used for the current exam."
subgroup_name_desc = "The name of the GitLab subgroup that will contain the exam repos to be built (i.e. 202008019, " \
                     "CCD Knowledge 20200819, etc.)"
examinee_names_desc = "A list of names representing those that will take an exam. Use double quotes around each " \
                      "name to include spaces."
workrole_desc = "The name of the work-role this exam is for."


def main():
    '''
    Purpose: This creates repositories for each person taking and exam.
    :return: None
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument("test_repo_path", type=str, help=test_repo_path_desc)
    parser.add_argument("subgroup_name", type=str, help=subgroup_name_desc)
    parser.add_argument("examinee_names", nargs='+', help=examinee_names_desc)
    parser.add_argument("--workrole", default="ccd", type=str, help=workrole_desc)
    args = parser.parse_args()
    
    eval_name = args.subgroup_name
    os.chdir(args.test_repo_path)

    for name in args.examinee_names:
        name = re.sub('[^A-Za-z0-9]+', '', name)
        branch = subprocess.run(branch_str.split(), stdout=subprocess.PIPE).stdout.decode()
        subprocess.run(git_str.format(eval_name=eval_name, name=name, branch=branch, workrole=args.workrole).split())


if __name__ == "__main__":
    main()
