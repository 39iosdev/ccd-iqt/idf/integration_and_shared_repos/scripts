import os
import re
import sys
import json
import argparse
import random
import pymongo
from colorama import Fore
from lib.default_strings import error_str, warning_str, ksats_key, workroles_key, module_key, topic_key, subject_key, \
    topic_path_key, ksats_id_key, description_key
from lib.csv_helpers import csv_escape_special_chars, stringify_list_for_cols
from lib.mttl_db_helpers import HOST_NAME, PORT_NUM, get_all_requirements, get_only_mapped_wkrole_ksat_prof_maps, \
    get_module_lsn_data
from lib.util import write_str_totext

# The column headers used to identify different parts of the Survey
column_headers_default = [
    "Rel-link Path",
    "Module",
    "Topic",
    "Subject",
    "Work-role",
    "KSAT ID",
    "Description",
    "0",
    "I",
    "II",
    "III",
    "IV",
    "Other"
]
zero_quest = "I don't remember this concept being taught"
knowl_options = [
    "I was taught a basic understanding of this concept",
    "I was taught a solid understanding of this concept",
    "I was taught an excellent understanding of this concept",
    "After this training, I feel like I could teach this concept to others"
]
perf_options = [
    "After this training, I would need help with this concept in practice",
    "After this training, I can do some of this concept in practice",
    "After this training, I can do most of this concept in practice",
    "After this training, I can do all of this concept in practice"
]
column_headers_desc = "The names of the column headers identifying the different parts of survey questions. " \
                      f"The default is {column_headers_default}"
description = "This creates Training Survey CSV files for every training relationship in the MTTL. The " \
              "CSV files contain the information/format necessary to generate Surveys using Google Apt Script APIs."
log_file_default = "GenTrngSurveyReport.json"
log_file_desc = f"The name of the report file; the default is '{log_file_default}'."
mongo_host_desc = f"This is the host name where the Mongo Database is running. The default is '{HOST_NAME}'"
mongo_port_desc = f"This is the port where the Mongo Database is running. The default is '{PORT_NUM}'"
mttl_dir_default = "mttl/"
mttl_dir_desc = f"The path to the cloned MTTL Repository. The default is '{mttl_dir_default}'"
nameof_csv_base_def = "trng_surveys.csv"
nameof_csv_base_desc = "The base name of the CSV file used to store the training survey results. Note, this name " \
                       "is prepended with <repo>_, where <repo> is replaced with the repository's " \
                       "name. The default is '{nameof_csv_base_def}'."
out_dir_default = "gen_survey_files/"
out_dir_desc = "The directory to store the resulting Generate Training Survey report and CSV file(s). " \
               f"The default is {out_dir_default}."
repo_name_desc = "This is the name of the repository getting training surveys."
db_name_default = "mttl"
db_name_desc = "This is the name of the data base and is used for DB connection purposes. " \
               f"The default is {db_name_default}"

msg_er_color = f"{Fore.RED + os.path.basename(__file__)} {error_str + Fore.RESET}:"
msg_er = f"{os.path.basename(__file__)} {error_str}:"
msg_wn_color = f"{Fore.YELLOW + os.path.basename(__file__)} {warning_str + Fore.RESET}:"
msg_wn = f"{os.path.basename(__file__)} {warning_str}:"
msg_gd_color = f"{Fore.GREEN + os.path.basename(__file__) + Fore.RESET}:"
msg_gd = f"{os.path.basename(__file__)}:"

log = []
ksats_info = {}


def get_random_invalid_ksat(all_ksats: dict, role_ksats: list) -> dict:
    """
    Purpose: This gets a random KSAT that's not part of the given workrole and creates a CSV string
    representing the chosen KSAT and options.
    :param all_ksats: KSAT OID to KSAT object map of all KSATs
    :param role_ksats: The workrole to {KSAT ID: Prof Code} mappings
    :return: A dictionary representing the random KSAT
    """
    non_role_ksats = []
    random.seed()
    for ksat_obj in all_ksats.values():
        if ksat_obj[ksats_id_key] not in role_ksats:
            non_role_ksats.append(ksat_obj)
    # Guarantee zero or greater for the range end value
    ksat = non_role_ksats[random.randint(0, len(non_role_ksats) - 1 if len(non_role_ksats) > 0 else 0)]
    return ksat


def calc_needed_invalid_ksats(question_ksats: list, threshold: int = 15) -> int:
    """
    Purpose: This calculates the number of invalid KSAT questions to add to the survey.
    :param question_ksats: The list of ksats mapped to a particular question
    :param threshold: An integer representing the number of valid KSAT questions per invalid KSAT question
    :return: An integer representing the number of invalid KSAT questions to include for every threshold amount
    """
    return len(question_ksats) // threshold + 1 if len(question_ksats) > 0 else 0


def get_question_options(ksat_id: str) -> list:
    """
    Purpose: This returns the desired list depending on the KSAT ID's value; it returns knowledge options for knowledge
    and performance options for abilities, skills, and tasks.
    :param ksat_id: A string representing the KSAT ID
    :return: A list representing the appropriate options, depending if it's knowledge or not
    """
    # Determine the list of questions to use
    if re.search(r"^K[0-9]{4,}", ksat_id):
        return knowl_options
    else:
        return perf_options


def get_survey_row(topic_path: str, module: str, topic: str, subject: str, role: str, ksat: dict) -> str:
    """
    Purpose: This builds a row that appears in a CSV file.
    :param topic_path: The path to the topic files
    :param module: A string representing the name of the Module
    :param topic: A string representing the name of the Topic
    :param subject: A string representing the name of the Subject
    :param role: A string representing the name of the work-role
    :param ksat: A dictionary representing the KSAT
    :return: A string representing the CSV compatible row
    """
    csv_row = ""
    try:
        # Ensure any necessary special characters are escaped
        ksat_desc = csv_escape_special_chars(ksat[description_key])
        ksat_id = ksat[ksats_id_key]
        csv_row += f"{topic_path},{module},{topic},{subject},{role},{ksat_id},\"{ksat_desc}\",\"{zero_quest}\""
        # Determine the list of questions to use and then escape necessary characters for each question
        # The last comma is for the other column
        csv_row += f",{stringify_list_for_cols(get_question_options(ksat_id))},\n"
    except KeyError as err:
        msg = "{error} Unable to process '{topic_path}': {err_msg}"
        print(msg.format(error=msg_er_color, topic_path=Fore.CYAN + topic_path + Fore.RESET, err_msg=err))
        log.append(msg.format(error=msg_er, topic_path=topic_path, err_msg=err))
    return csv_row


def build_survey(rel_link_data: dict, ksats_map: dict, wkrole_map: dict) -> str:
    """
    Purpose: This builds a CSV string with the row data built according to the mapped KSATs and work-roles.
    :param rel_link_data: The rel-link data that will have a training survey built.
    :param ksats_map: A KSAT ID to info mapping for all KSATs.
    :param wkrole_map: A work-role name to applicable KSAT: Prof Code mappings for all roles
    :return: csv_data - The CSV formatted string
    """
    global log
    csv_data = ""
    csv_rows = []
    topic_path = ""
    try:
        topic_path = rel_link_data[topic_path_key]
        module = rel_link_data[module_key]
        topic = rel_link_data[topic_key]
        subject = rel_link_data[subject_key]
        if topic.strip() == "":
            # If the topic ends up being a null string then use the subject as the topic
            topic = subject
        # Get a single list of KSATs, don't need prof codes
        trng_ksat_oids = [ksat_data[ksats_id_key] for ksat_data in rel_link_data[ksats_key]]
        honest_effort_qs = calc_needed_invalid_ksats(trng_ksat_oids)
        for role in rel_link_data[workroles_key]:
            # Creates a survey per work-role
            wkrole_data = wkrole_map[role.upper()]
            for ksat_oid in trng_ksat_oids:
                # ksat = ksats_map[ksat_oid][ksats_id_key]
                ksat = ksats_map[ksat_oid]
                if ksat[ksats_id_key] in wkrole_data:
                    # Only add the ksat survey question if it's still part of this work-role
                    csv_rows.append(get_survey_row(topic_path, module, topic, subject, role, ksat))
            for i in range(honest_effort_qs):
                hon_eff_ksat = get_random_invalid_ksat(ksats_map, list(wkrole_data.keys()))
                # Insert the honest check question into a random spot
                random.seed()
                index = random.randint(0, len(csv_rows) - 1 if len(csv_rows) > 0 else 0)  # Guarantee zero or greater
                csv_rows.insert(index, get_survey_row(topic_path, module, topic, subject, role, hon_eff_ksat))
        csv_data = "".join(csv_rows)
    except (KeyError, IndexError, TypeError) as err:
        msg = "{error} Unable to process '{topic_path}': {err_msg}"
        print(msg.format(error=msg_er_color, topic_path=Fore.CYAN + topic_path + Fore.RESET, err_msg=err))
        log.append(msg.format(error=msg_er, topic_path=topic_path, err_msg=err))
    return csv_data


def main(args: argparse.Namespace):
    """
    Purpose: Sets up the environment so training surveys can be built and saved to disk.
    :param args: The arguments passed from the command-line
    :return: None
    """
    # Create the out_dir if it doesn't exist
    if not os.path.exists(args.out_dir):
        os.makedirs(args.out_dir)
    # Get Mongo DB connection
    host = os.getenv('MONGO_HOST', args.Host)
    port = int(os.getenv('MONGO_PORT', args.Port))
    client = pymongo.MongoClient(host, port)
    db = client[args.db_name]
    # Get the work-role map for all available work-roles
    wkrole_map = get_only_mapped_wkrole_ksat_prof_maps(db)
    ksats_map = get_all_requirements(db)  # Get KSAT map for all KSATs
    # Get list of all data for this repo_name
    repo_data = get_module_lsn_data(args.repo_name, db)
    # A string representation of the CSV file. Start with the first row of column header data
    csv_data = f"{','.join(args.column_headers)}\n"
    for item in repo_data:
        csv_data += build_survey(item, ksats_map, wkrole_map)
    name = f"{args.repo_name}_{args.nameof_csv_base}"  # The final name used to write the CSV data
    write_str_totext(csv_data, os.path.join(args.out_dir, name))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("repo_name", type=str, help=repo_name_desc)
    parser.add_argument("-c", "--column_headers", nargs='+', type=str, default=column_headers_default,
                        help=column_headers_desc)
    parser.add_argument("-d", "--db_name", type=str, default=db_name_default, help=db_name_desc)
    parser.add_argument("-H", "--Host", type=str, default=HOST_NAME, help=mongo_host_desc)
    parser.add_argument("-l", "--log_file", type=str, default=log_file_default, help=log_file_desc)
    parser.add_argument("-m", "--mttl_dir", type=str, default=mttl_dir_default, help=mttl_dir_desc)
    parser.add_argument("-n", "--nameof_csv_base", type=str, default=nameof_csv_base_def, help=nameof_csv_base_desc)
    parser.add_argument("-o", "--out_dir", type=str, default=out_dir_default, help=out_dir_desc)
    parser.add_argument("-P", "--Port", type=str, default=PORT_NUM, help=mongo_port_desc)
    params = parser.parse_args()

    main(params)
    if len(log) == 0:
        print(f"{Fore.GREEN}Survey CSV files were saved in {Fore.CYAN + params.out_dir + Fore.GREEN}.{Fore.RESET}")
    else:
        with open(os.path.join(params.out_dir, params.log_file), "w") as log_fp:
            json.dump(log, log_fp, indent=4)
        print(f"{Fore.RED}Errors present{Fore.RESET}: refer to "
              f"'{Fore.CYAN + os.path.join(params.out_dir, params.log_file) + Fore.RESET}' for details.")
    sys.exit(len(log))
