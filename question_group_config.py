#!/usr/bin/python3

import os
import sys
from colorama import Fore
from json import load, dump, JSONDecodeError
import argparse
from lib import qid_helpers
from lib.errors import ErrorCode
from lib.file_list_generator import dir_walk, changed_paths
from lib.default_strings import param_question_dir_desc, param_root_dir_desc, opt_param_changed_desc, group_key, \
    group_name_key, group_questions_key, id_key, snippet_key, snippet_lang_key, warning_str, error_str, \
    param_root_dir_default, keyword_desc, keyword_default
import qid_updater

description = "This module takes a list of questions that are in the same group and configures the group name and " \
              "question IDs. It assigns all questions applicable to this group by adding each question's question ID " \
              "to the [group][questions] list in each JSON file. Additionally, it ensures all questions have the " \
              "same snippet text for individual answer files. If only a few questions are modified, the entire " \
              "question group is dynamically added to ensure all questions are configured with the same information."
out_dir_default = "."
out_dir_desc = "The directory to store the resulting group question config report, relative to root_dir. " \
               "The default is '{}'.".format(out_dir_default)
report_file_default = "GroupQuestionReport.json"
report_file_desc = "The name of the report file; the default is '{}'.".format(report_file_default)

default_number = 1000
default_name = "default-group_"
indent_value = 4

msg_er_color = "{0} {1}:".format(Fore.RED + os.path.basename(__file__), error_str + Fore.RESET)
msg_er = "{0} {1}:".format(os.path.basename(__file__), error_str)
msg_wn_color = "{0} {1}:".format(Fore.YELLOW + os.path.basename(__file__), warning_str + Fore.RESET)
msg_wn = "{0} {1}:".format(os.path.basename(__file__), warning_str)
msg_gd_color = "{0}:".format(Fore.GREEN + os.path.basename(__file__) + Fore.RESET)
msg_gd = "{0}:".format(os.path.basename(__file__))


def question_group_config(json_files: list, group_name: str, next_id_nums: dict, id_key: str = id_key,
                          snip_key: str = snippet_key, grp_key: str = group_key, grp_name_key: str = group_name_key,
                          grp_quest_key: str = group_questions_key) -> (dict, dict):
    '''
    Purpose: This module takes a list of questions that are in the same group and configures the group name and question
    IDs. It assigns all questions applicable to this group by adding each question's ID to the [group][questions] list
    in each JSON file. Additionally, it ensures all questions have the same snippet text for individual answer files.
    :param json_files: A list of JSON file paths that are all in the same group.
    :param group_name: The name of this question group.
    :param next_id_nums: This is a dict of the next available qids; expects dict object from
                         uid_helpers.get_next_id_nums()
    :param id_key: The name of the unique ID key in template.json. Default = "uid"
    :param snip_key: The name of the snippet key within the knowledge dictionary in template.json. Default = "snippet"
    :param grp_key: The name of the group key within the knowledge dictionary in template.json. Default = "group"
    :param grp_name_key: The name of the name key within the group dictionary in template.json. Default = "name"
    :param grp_quest_key: The name of the question key within the group dictionary in template.json.
                          Default = "question"
    :return: this_status - a dictionary with keys representing error codes according to class Error or the exception, when
             present.
    '''
    this_status = {}
    group = {}  # Stores {"file path": question dictionary object} for each question
    qids = []  # Stores all question ID's applicable to this group
    snippet = ""  # Stores the first occurrence of the snippet text
    snippet_lang = ""  # Stores the first occurrence of the snippet language
    same_qids = True  # Stores True if all question groups have the same list of qids
    same_grp_name = True  # Stores True if all question groups have the same group name
    same_snip_lang = True  # Stores True if all question groups have the same snippet language

    try:
        for json_path in json_files:
            # Update and extract the question ID for each question
            if not os.path.exists(json_path):
                # In case this is a bad file path
                msg = "{0} Couldn't find {1}."
                print(msg.format(msg_er_color, Fore.CYAN + os.path.basename(json_path) + Fore.RESET))
                this_status[ErrorCode.INVALID_INPUT.value] = msg.format(msg_er, os.path.basename(json_path))
                return this_status, next_id_nums
            # Get the name of the question's work-role
            wkrole = os.path.basename(os.getcwd())
            # Ensure the question ID is updated and add to list
            qid_status, next_id_nums = qid_updater.update_uid(json_path, next_id_nums, wkrole)
            if (len(qid_status) == 1 and 0 in qid_status) or len(qid_status) == 0:
                # In case the question ID is generated
                update_status(qid_status, this_status)
                with open(json_path, "r") as quest_fp:
                    question = load(quest_fp)  # Get the question's dictionary representation

                if question is not None and id_key in question:
                    # In case the question loaded properly
                    if snippet == "" and snip_key in question and question[snip_key] is not None and \
                            question[snip_key] != "":
                        # Store the first snippet text found
                        snippet = question[snip_key]

                    if snippet_lang == "" and snippet_lang_key in question and question[snippet_lang_key] is not None \
                            and question[snippet_lang_key] != "":
                        # Store the first snippet language found
                        snippet_lang = question[snippet_lang_key]

                    # Add question dictionary to group
                    group[json_path] = question
                    # Add question ID to ID list
                    qids.append(question[id_key])

                else:
                    # In case the question wasn't created for some reason
                    msg = "{0} An error occurred when adding {1} to the group question ID list."
                    print(msg.format(msg_er_color, Fore.CYAN + os.path.basename(json_path) + Fore.RESET))
                    if ErrorCode.INVALID_JSON_FORMAT.value not in this_status:
                        this_status[ErrorCode.INVALID_JSON_FORMAT.value] = []
                    this_status[ErrorCode.INVALID_JSON_FORMAT.value].append(
                        msg.format(msg_er, os.path.basename(json_path)))
                    return this_status, next_id_nums

            else:
                # In case the question ID wasn't able to be generated
                update_status(qid_status, this_status)
                msg = "{0} An error occurred when updating the question ID for {1}."
                print(msg.format(msg_er_color, Fore.CYAN + os.path.basename(json_path) + Fore.RESET))
                if ErrorCode.UNABLE_TO_GEN_UID.value not in this_status:
                    this_status[ErrorCode.UNABLE_TO_GEN_UID.value] = []
                this_status[ErrorCode.UNABLE_TO_GEN_UID.value].append(msg.format(msg_er, os.path.basename(json_path)))
                return this_status, next_id_nums

        for json_path, question in group.items():
            if group_key not in question:
                # In case the group question is missing the group attribute
                same_grp_name = same_qids = False

            elif len(qids) == len(question[grp_key][grp_quest_key]):
                # In case both lists have the same number of items, compare to see if all items are the same
                if qids.sort() != question[grp_key][grp_quest_key].sort():
                    # In case the two lists are not the same
                    same_qids = False

            else:
                # In case the two lists have differing number of items
                same_qids = False

            if group_key in question and group_name != question[grp_key][grp_name_key]:
                # In case the group name is not the same
                same_grp_name = False

            if snippet_lang_key in question and snippet_lang != question[snippet_lang_key]:
                # In case the snippet language is not the same
                same_snip_lang = False

        if not same_qids or not same_grp_name or not same_snip_lang:
            # In case the question group needs updated
            for key, value in group.items():
                # Configure all questions with the same snippet text
                if snippet != "" and snip_key in value:
                    # Only update all snippet's if there's a snippet to update
                    value[snip_key] = snippet

                if snippet_lang != "" and snippet_lang_key in value:
                    # Only update all snippet languages if there's a snippet language to update
                    value[snippet_lang_key] = snippet_lang

                # Configure all questions with the same group name and question ID list
                value[grp_key] = {
                    grp_name_key: group_name,
                    grp_quest_key: qids
                }

                with open(key, "w") as quest_fp:
                    dump(value, quest_fp, indent=indent_value)  # Save data back to disk

            msg = "{0} {1} was configured to include these questions: "
            print(msg.format(msg_gd_color, Fore.GREEN + group_name + Fore.RESET))
            for q_file in json_files:
                print(f"\t{Fore.CYAN + q_file + Fore.RESET}")
            if ErrorCode.SUCCESS.value not in this_status:
                this_status[ErrorCode.SUCCESS.value] = []
            this_status[ErrorCode.SUCCESS.value].append({msg.format(msg_gd, group_name): json_files})

    except JSONDecodeError as err:
        msg = "{0} A problem occurred during JSON decode: {1}"
        print(msg.format(msg_er_color, err))
        if ErrorCode.JSON_DECODE_ERROR.value not in this_status:
            this_status[ErrorCode.JSON_DECODE_ERROR.value] = []
        this_status[ErrorCode.JSON_DECODE_ERROR.value] = msg.format(msg_er, err)

    except (KeyError, ValueError) as err:
        msg = "{0} Invalid input or key name: {1}"
        print(msg.format(msg_er_color, err))
        if ErrorCode.INVALID_INPUT.value not in this_status:
            this_status[ErrorCode.INVALID_INPUT.value] = []
        this_status[ErrorCode.INVALID_INPUT.value] = msg.format(msg_er, err)

    except (FileNotFoundError, FileExistsError, OSError, IOError) as err:
        # In case the file cannot be opened, display a reason and stop processing
        msg = "{0} Something occurred during file processing: ({1}){2}"
        print(msg.format(msg_er_color, err.errno, err))
        if err.errno not in this_status:
            this_status[err.errno] = []
        this_status[err.errno].append(msg.format(msg_er, err.errno, err))

    return this_status, next_id_nums


def get_all_group_questions(group_dir: str, config_file_filter: str = ".json") -> list:
    '''
    Purpose: This function retrieves all JSON files in the input group directory; in case only one file in a group was
    modified.
    :param group_dir: The group directory containing each question's folder in the group.
    :param config_file_filter: The search term to look for that contains each question's configuration information.
                               default = '.json'
    :return: group = A list of question configuration file paths in the root group directory; it's empty if none found.
    '''
    group = []
    # Convert MS backslash directory separators to forward slash
    group_dir = group_dir.replace("\\", "/")

    if not group_dir.endswith("/"):
        # In case the directory path doesn't end in a trailing slash
        group_dir += "/"

    if os.path.exists(group_dir) and os.path.isdir(group_dir):
        for (root, dirs, files) in os.walk(group_dir):
            # Recursively search the current working directory for the JSON schema file
            for file in files:
                if config_file_filter in file:
                    # In case a JSON file was found, add to the list
                    group.append(os.path.join(root, file).replace("\\", "/"))

    return group


def build_question_groups(paths: list, root_dir: str, question_dirs: list):
    '''
    Purpose: This identifies questions that are part of a question group and builds a dictionary of groups listing each
    question path that's part of the respective group.
    :param paths: A list representing the questions to search.
    :param root_dir: A string representing the root directory.
    :param question_dirs: A list representing the desired question directories to search.
    :return: group - A dictionary representing question groups with a list of applicable question paths
             this_status - a dictionary with keys representing error codes according to class Error or the exception,
             when present.
    '''
    this_status = {}
    groups = {}
    for item in paths:
        # Search all paths and build a list of question groups
        if not os.path.exists(item):
            # In case the path is bad
            msg = "Couldn't find {0}."
            print(msg.format(Fore.RED + item + Fore.RESET))
            if ErrorCode.EMPTY_LIST.value not in this_status:
                this_status[ErrorCode.EMPTY_LIST.value] = []
            this_status[ErrorCode.EMPTY_LIST.value].append(msg.format(item))
            return this_status, groups

        if default_name in item:
            # In case the path has a default group name, detect the highest number in the default group name
            global default_number
            temp_num = 0  # Store the current number in the name

            # Separate the directory names into a list
            def_dirs = item.replace("\\", "/").split("/")

            for folder in def_dirs:
                # Find the default name and process the number in this path
                if default_name in def_dirs.lower():
                    # In case this is the default name, store the number
                    temp_num = int(folder.split(default_name))

            # Make sure default_number has the highest value
            if temp_num > default_number:
                default_number = temp_num

        with open(item, "r") as quest_fp:
            data = load(quest_fp)  # Load JSON into a dictionary

        # Detect group questions and store the file's name, directory and parent directory separately
        # The group's path will become the key name in a groups dictionary object containing all group questions
        if group_key in data:
            # In case this is a group question, get the file name
            question = os.path.basename(item)

            # Get the file's directory
            question_dir = os.path.dirname(item).replace("\\", "/")

            # This assumes all group question folders are in a group folder
            # get the group's directory
            group_dir = os.path.dirname(question_dir).replace("\\", "/")

            skip_file = False

            # Determine if the group_dir is the same as the root question directory
            for root_quest_dir in question_dirs:
                if os.path.join(root_dir, root_quest_dir) == group_dir:
                    invalid_grp_json_msg = "Misconfigured group in {0}; skipping file.\nIf this isn't a group, " \
                                           "then remove the 'group' key; otherwise, create a group folder and " \
                                           "place all applicable question folders in the group folder.".format(item)
                    print("{0}: {1}".format(Fore.YELLOW + warning_str, Fore.RESET + invalid_grp_json_msg))
                    if ErrorCode.INVALID_JSON_FORMAT.value not in this_status:
                        this_status[ErrorCode.INVALID_JSON_FORMAT.value] = []
                    this_status[ErrorCode.INVALID_JSON_FORMAT.value].append(invalid_grp_json_msg)
                    skip_file = True

            if skip_file:
                continue

            # Add the question to its group in a dictionary object with the group's path as the key
            if group_dir in groups:
                # In case the key already exists
                groups[group_dir].append(os.path.join(question_dir, question).replace("\\", "/"))

            else:
                # In case the key doesn't exist, create a list of question paths
                groups[group_dir] = [os.path.join(question_dir, question).replace("\\", "/")]
    return this_status, groups


def add_any_missing_questions(grp_dir: str, grp_questions: list):
    '''
    Purpose: This determines if any questions were missed and adds them to the list of question paths.
    :param grp_dir: A string representing the name of the group directory.
    :param grp_questions: A list representing the paths to each question in the group.
    :return: None
    '''
    # Ensure all questions were included in the list
    temp = get_all_group_questions(grp_dir)

    for json_file in temp:
        # Add any missing questions to the list
        if json_file not in grp_questions:
            # In case this question is not in the list
            grp_questions.append(json_file)


def detect_group_name(grp_dir: str, grp_questions: list):
    '''
    Purpose: This determines the name for the question group.
    :param grp_dir: A string representing the name of the group directory.
    :param grp_questions: A list representing the paths to each question in the group.
    :return: name - A string representing the name of the question group.
    '''
    global default_name, default_number
    name = ""
    for grp_question in grp_questions:
        # Load JSON into a dictionary
        with open(grp_question, "r") as quest_fp:
            data = load(quest_fp)

        if group_key in data and group_name_key in data[group_key] and \
                data[group_key][group_name_key] != "":
            # In case we found the group name
            name = data[group_key][group_name_key]
            break  # No need to continue because all files should have the same group name

    if "" == name:
        # In case for some reason the group name doesn't exist
        name = os.path.basename(grp_dir)  # Use the group directory name for the group name

        if "" == name:
            # In case for some reason the group_dir is an empty string
            # At this point, default number has the highest number in the directory tree
            default_number += 1
            name = default_name + default_number

    return name


def update_status(new_status: dict, dst_status: dict):
    '''
    Purpose: This updates the status code in the main status object.
    :param new_status: A dictionary representing the new status codes.
    :param dst_status: A dictionary representing the main status object.
    :return: None
    '''
    for code, msg_list in new_status.items():
        if code not in dst_status:
            dst_status[code] = []
        dst_status[code] += msg_list


def main(args: argparse.Namespace) -> dict:
    '''
    Purpose: Allows configuration of group questions.
    :return: this_status - a dictionary with keys representing error codes according to class Error or the exception,
             when present.
    '''
    this_status = {}
    groups = {}
    if args.question_dirs is None:
        args.question_dirs = ["."]

    try:
        if args.changed:
            # In case only changed files are requested
            paths = changed_paths(repo_base_dir=args.root_dir, keyword=args.filter)
        else:
            # In case all files are requested
            paths = dir_walk(root_dir=args.root_dir, json_dirs=args.question_dirs, keyword=args.filter)
        if len(paths) == 0:
            # In case the list is empty
            msg = "{0} {1}: Received empty file list."
            print(msg.format(Fore.YELLOW + os.path.basename(__file__), warning_str + Fore.RESET))
            if ErrorCode.EMPTY_LIST.value not in this_status:
                this_status[ErrorCode.EMPTY_LIST.value] = []
            this_status[ErrorCode.EMPTY_LIST.value].append(msg.format(os.path.basename(__file__), warning_str))
            return this_status

        next_uid_nums = qid_helpers.get_next_id_nums(args.filter)

    except ValueError as err:
        msg = "{0} {1}: {2}"
        print(msg.format(msg_er_color, ErrorCode.INVALID_INPUT.name, err))
        if ErrorCode.JSON_DECODE_ERROR.value not in this_status:
            this_status[ErrorCode.JSON_DECODE_ERROR.value] = []
        this_status[ErrorCode.JSON_DECODE_ERROR.value].append(msg.format(msg_er, ErrorCode.INVALID_INPUT.name, err))
        return this_status

    except (FileNotFoundError, FileExistsError, OSError, IOError) as err:
        # In case the file cannot be opened, display a reason and stop processing
        msg = "({0}){1}".format(err.errno, err)
        print("{0} {1}".format(msg_er_color, msg))
        if err.errno not in this_status:
            this_status[err.errno] = []
        this_status[err.errno].append("{0} {1}".format(msg_er, msg))
        return this_status

    temp_status, groups = build_question_groups(paths, args.root_dir, args.question_dirs)
    update_status(temp_status, this_status)

    # Find the group name of each group and then configure the group
    for grp_dir, grp_questions in groups.items():
        # Process each question group
        add_any_missing_questions(grp_dir, grp_questions)
        name = detect_group_name(grp_dir, grp_questions)

        # Configure the group questions with the group name and applicable question ID's
        code, next_uid_nums = question_group_config(grp_questions, name, next_uid_nums)
        update_status(code, this_status)

    return this_status


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("-c", "--changed", action="store_true", help=opt_param_changed_desc)
    parser.add_argument("-f", "--filter", type=str, default=keyword_default, help=keyword_desc)
    parser.add_argument("-o", "--out_dir", type=str, default=out_dir_default, help=out_dir_desc)
    parser.add_argument("-q", "--question_dirs", nargs='+', type=str, default=None, help=param_question_dir_desc)
    parser.add_argument("-r", "--report_file", type=str, default=report_file_default, help=report_file_desc)
    parser.add_argument("-R", "--root_dir", type=str, default=param_root_dir_default, help=param_root_dir_desc)
    params = parser.parse_args()
    status = main(params)
    out_dir = params.out_dir
    if params.out_dir != ".":
        # Eliminates the redundant ././ when combining root_dir and out_dir
        out_dir = os.path.join(params.root_dir, params.out_dir)
    try:
        # Save the log
        msg_success = "Question group configuration completed successfully."
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)
        if len(status) == 0:
            # In case status has no errors
            status[ErrorCode.SUCCESS.value] = [msg_success]
        with open(os.path.join(out_dir, params.report_file), "w") as q_fp:
            dump(status, q_fp, indent=4)

    except (JSONDecodeError, FileNotFoundError, FileExistsError, OSError, IOError) as err:
        # In case the file cannot be opened, display a reason and stop processing
        msg = "{0} Unable to save {1}: {2}"
        print(msg.format(msg_er_color, os.path.join(out_dir, params.report_file), err))

    else:
        if (len(status) == 1 and ErrorCode.SUCCESS.value in status) or len(status) == 0:
            # In case the process was successful
            print(Fore.GREEN + msg_success + Fore.RESET)
        print("{0} Report saved to: {1}".format(
            msg_gd_color,
            os.path.join(out_dir, params.report_file)
        ))

    if not (len(status) == 1 and ErrorCode.SUCCESS.value in status):
        # In case status has errors, prevent returning zero
        if ErrorCode.SUCCESS.value in status:
            # For cases where ErrorCode.SUCCESS.value isn't present
            del status[ErrorCode.SUCCESS.value]
    sys.exit(list(status.keys())[0])
