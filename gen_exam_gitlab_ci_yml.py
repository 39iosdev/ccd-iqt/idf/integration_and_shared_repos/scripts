#! /usr/bin/python3

import os
import argparse

description = "This builds the .gitlab-ci.yml file for an exam repository and will be able to include necessary " \
              "question pipeline files as part of the exam pipeline. It's recommended to run this on the repository, " \
              "or folder representation, that holds the current exam material. A search for pipeline files is " \
              "conducted, starting in the 'exam_dir', and the first pipeline file found for each question is added " \
              "to the 'include:' section of .gitlab-ci.yml."
ci_config_file_ext_default = ".yml"
pre_include_data_default = [
    "image: registry.gitlab.com/90cos/docker-definitions/basic-dev\n",
    "\n",
    "stages:\n",
    "  - Build\n",
    "  - Test\n",
    "  - Manual Test\n",
    "\n",
    "noNeeds:\n",
    "  image: alpine\n",
    "  stage: Build\n",
    "  variables:\n",
    "    GIT_STRATEGY: none\n",
    "  before_script: []\n",
    "  script:\n",
    "    - echo \"This job is a workaround for 'https://gitlab.com/gitlab-org/gitlab/issues/30631'\"\n",
    "  cache:\n",
    "    key: noNeeds\n",
    "    paths: []\n",
    "  dependencies: []\n",
    "\n",
    "include:\n"
]
exam_dir_desc = "The root folder of an exam repository or folder containing the exam."
ci_config_file_ext_desc = "The name of the pipeline file extension in use."
pre_include_data_desc = "The content of the .gitlab-ci.yml file up to and including the 'include:' statement. It's " \
                        "assumed all other needed content appears before the 'include:' statement."
ignore_default = ["googletest", ".git/", ".gitlab-ci.yml"]
ignore_desc = "A list of files or folders to ignore"


def is_ignored(path: str, ignored: list) -> bool:
    '''
    Purpose: This determines if the given path should be ignored.
    :param path: A string representing a file's path.
    :param ignored: A list representing files or folders to ignore.
    :return: True - The file should be ignored
             False - The file should not be ignored
    '''
    for ignore in ignored:
        if ignore in path.replace("\\", "/"):
            return True
    return False


def strip_exam_dir(path: str, exam_dir: str) -> str:
    '''
    Purpose: This strips the exam directory from the path, which is necessary for correct YAML syntax.
    :param path: A string representing a file's path.
    :param exam_dir: A string representing the path to the exam directory.
    :return: A string representing the file's path with the exam_dir removed.
    '''
    # An assumption is made that the exam_dir will be the left most value of the path
    # We need to make sure any trailing forward or back slash is removed to get an accurate folder count
    exam_dirs = exam_dir.replace("\\", "/").rstrip("/").split("/")
    return "/".join(path.replace("\\", "/").split("/")[len(exam_dirs):])


def get_gitlab_ci_yml(exam_dir: str, ci_config_file_ext: str = ci_config_file_ext_default, ignored: list = None,
                      pre_include_data: str = "".join(pre_include_data_default)) -> str:
    '''
    Purpose: This builds the .gitlab-ci.yml file for an exam repository and will be able to include necessary question
    pipeline files as part of the exam pipeline. It's recommended to run this on the repository, or folder
    representation, that holds the current exam material. A search for pipeline files is conducted, starting in the
    'exam_dir', and the first pipeline file found for each question is added to the 'include:' section of
    .gitlab-ci.yml.
    :param exam_dir: The root folder of an exam repository or folder containing the exam.
    :param ci_config_file_ext: The name of the pipeline file extension in use.
    :param ignored: A list representing files and folders to ignore.
    :param pre_include_data: The content of the .gitlab-ci.yml up to and including the 'include:' statement. It's
                             assumed all other needed content appears before the 'include:' statement.
    :return: gitlab_ci_yml - A string representing the content of the newly generated .gitlab-ci.yml file.
    '''
    gitlab_ci_yml = pre_include_data
    if ignored is None:
        ignored = ignore_default

    for root, dirs, files in os.walk(exam_dir):
        # Search for the question's pipeline file
        for path in files:
            if is_ignored(os.path.join(root, path), ignored):
                continue
            if ci_config_file_ext in path:
                # It's assumed this script is run from the exam repo or folder representing the exam repo structure
                gitlab_ci_yml += f"  - {strip_exam_dir(os.path.join(root, path), exam_dir)}\n"

    return gitlab_ci_yml


def main(args: argparse.Namespace):
    gitlab_ci_file = ".gitlab-ci.yml"
    try:
        with open(os.path.join(args.exam_dir, gitlab_ci_file), "w") as gitlab_fp:
            gitlab_fp.write(get_gitlab_ci_yml(args.exam_dir, args.ci_config_file_ext, args.ignore,
                                              args.pre_include_data))
    except (FileNotFoundError, FileExistsError, NotADirectoryError, OSError, Exception) as e:
        print(f"An error occurred while processing '{os.path.join(args.exam_dir, gitlab_ci_file)}': {e}")
    else:
        print(f"Finished writing '{os.path.join(args.exam_dir, gitlab_ci_file)}'.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("exam_dir", type=str, help=exam_dir_desc)
    parser.add_argument("-c", "--ci_config_file_ext", type=str, default=ci_config_file_ext_default,
                        help=ci_config_file_ext_desc)
    parser.add_argument("-i", "--ignore", type=list, default=ignore_default, help=ignore_desc)
    parser.add_argument("-d", "--pre_include_data", type=str, default="".join(pre_include_data_default),
                        help=pre_include_data_desc)
    params = parser.parse_args()
    main(params)
