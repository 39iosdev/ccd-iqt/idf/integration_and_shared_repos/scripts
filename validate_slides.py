#!/usr/bin/python3

# usage: python3 validate_slides.py -o validation_report -d slides/

import os
import sys
import argparse
from bs4 import BeautifulSoup, Comment
import mistune
from lib.errors import ErrorCode
from lib.default_strings import param_md_slide_dirs


description = "This module checks the MD books/slides formatting. If the formatting is " \
              "invalid then a report with a list of files, as well as the lines with the " \
              "offending formatting, is written to 'root_dir/out_dir/report_file'. Actual updates to each MD book/slides requires " \
              "a manual process. To view logs, see the log 'root_dir/out_dir/log_file'."

out_dir_default = "."
out_dir_desc = "The directory to store the resulting validation report, relative to root_dir. " \
               "The default is '{}'.".format(out_dir_default)

log_file_default = "ValidationLog.log"
log_file_desc = "The name of the log file; the default is '{}'".format(log_file_default)

show_passes_default = False
show_passes_desc = "A boolean value if you want to see passing cases. (True/False)"

parser = argparse.ArgumentParser(description=description)
parser.add_argument("-d", "--dirs", nargs='+', type=str, default=None, help=param_md_slide_dirs)
parser.add_argument("-o", "--out_dir", type=str, default=out_dir_default, help=out_dir_desc)
parser.add_argument("-l", "--log_file", type=str, default=log_file_default, help=log_file_desc)
parser.add_argument("-p", "--show_passes", type=bool, default=show_passes_default, help=log_file_desc)

max_words_paragraph = 150


def strip_comments(parser):
    '''
    Purpose: Strips all html comments.
    :return: BeautifulSoup parser object
    '''
    for comment in parser(text=lambda text: isinstance(text, Comment)):
        comment.extract()
    return parser


def strip_formating_tags(parser):
    '''
    Purpose: Strips all formatting tags.
    :return: BeautifulSoup parser object
    '''
    # Strip the formatting tags, <b><i><u><font><center><p><br>
    format_tags = ["b", "i", "u", "font", "center", "center", "p", "br"]
    for tag in format_tags:
        for t in parser.select(tag):
            t.extract()
    return parser


def check_big_section(parser, file_name):
    '''
    Purpose: Checks if sections are larger than 30 elements
    :return: A dictionary {"0": Passing...} or {"2": WARNING...}
    '''
    warning_list = []
    parser = strip_comments(parser)
    parser = strip_formating_tags(parser)
    sections = parser.find_all('section')
    for section in sections:
        # This is an outer section tag, ignore.
        if section.parent.name == 'div':
            continue
        # No inner sections so check the length
        if len(section.findChildren('section', recursive=False)) == 0:
            if len(max(list(section.children), key=len)) >= 30:
                warning_list.append('WARNING Section length too big: '
                                    'please consider reducing the size at line {} in {}'
                                    .format(section.sourceline, file_name))

    if len(warning_list) > 0:
        return {"2": warning_list}

    return {"0": "PASS All Section length limit: {}".format(file_name)}


# If <ul> is inside of <li> it is acceptable.
def check_single_item_list(parser, file_name):
    '''
    Purpose: Checks all list tags to see if they only have one element
    return: A dictionary {"0": Passing...} or {"1": ERROR...}
    '''
    tag_list = ['ul', 'ol']
    error_list = []
    list_tags = parser.find_all(tag_list)

    for list_tag in list_tags:
        # Check the previous tag to see if <ul> is inside of another <ul>
        if list_tag.parent.name in tag_list:
            continue
        # Check if contents of the tag are less than 3. 2 "\n"s and the "<li></li>" tag itself
        if len(list_tag.contents) <= 3:
            error_list.append('ERROR List has a single item: '
                                'please consider modifying list at line {} in {}'
                                .format(list_tag.sourceline, file_name))

    if len(error_list) > 0:
        return {"1": error_list}

    return {"0": "PASS All Lists Valid: {}".format(file_name)}


def check_for_title(parser, file_name):
    '''
    Purpose: Checks to see if a title tag is present
    return: A dictionary {"0": Passing...} or {"1": ERROR...}
    '''
    header_tags = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6']
    error_list = []
    #Check for title tag(HTML). Check for header tag(markdown)
    if not parser.title and len(parser.find_all(header_tags)) <= 0:
        error_list.append('ERROR Invalid title: No title in file {}'.format(file_name))

    sections = parser.find_all('section')
    for section in sections:
        # This is an outer section tag, ignore.
        if section.parent.name == 'div':
            continue
        if not section.find_all(header_tags):
            error_list.append('ERROR Invalid title: No title in section at line {} in {}'
                              .format(section.sourceline, file_name))

    if len(error_list) > 0:
        return {"1": error_list}

    return {"0": "PASS Title Valid: {}".format(file_name)}


def check_for_large_paragraph(parser, file_name):
    '''
    Purpose: Checks all paragraph tags to see if they are too big
    return: A dictionary {"0": Passing...} or {"2": WARNING...}
    '''
    warning_list = []
    paragraphs = parser.find_all('p')
    for p in paragraphs:
        if len(p.text.split()) >= max_words_paragraph:
            warning_list.append('WARNING Paragraph length too long: '
                                'please consider reducing the length of paragraph at line {} in {}'
                                .format(p.sourceline, file_name))

    if len(warning_list) > 0:
        return {"2": warning_list}

    return {"0": "PASS Valid Paragraph: {}".format(file_name)}


# Passes as an optional flag
def main() -> dict:
    '''
    Purpose: Validate files and writes the results to a file, see 'description' for more
    information.
    :return: status - 0 = success, 1 = fail
    '''
    global args
    status = 0
    if args.dirs is None:
        args.dirs = ["."]

    out_dir = args.out_dir
    if args.out_dir != ".":
        # Eliminates the redundant ././ when combining root_dir and out_dir
        out_dir = os.path.join(os.getcwd(), args.out_dir)

    if len(args.dirs) == 0:
        # In case the list is empty
        print("No directories provided {}".format(ErrorCode.EMPTY_LIST.name))
        status = 1
        return status

    # If the output directory doesn't exist, make it.
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    # Build the list of files
    files = []
    for dir in args.dirs:
        for file in os.listdir(dir):
            if file.endswith(".html") or file.endswith(".md"):
                files.append(os.path.join(dir, file))

    # Stores messages logs
    logs = []

    # Start parsing for the rules
    for file in files:
        try:
            f = open(file, "r")
            content = f.read()
            if file.endswith(".md"):
                content = mistune.html(content)
            parser = BeautifulSoup(content, 'html.parser')
            f.close()
            # Check for title
            logs.append(check_for_title(parser, f.name))
            # Check for long paragraphs
            logs.append(check_for_large_paragraph(parser, f.name))
            # Check for lists with one item
            logs.append(check_single_item_list(parser, f.name))
            # Check for long sections
            logs.append(check_big_section(parser, f.name))
        except (FileNotFoundError, FileExistsError, OSError, IOError) as err:
            print("ERROR {} in {}".format(err, f.name))
            status = 1
            return status

    # Parse logs into individual pass, warn, failures
    log_pass = []
    log_fails = []
    log_warning = []
    for log in logs:
        for k, v in log.items():
            if k == "0":
                log_pass.append(v)
            if k == "1":
                log_fails.append(v)
            if k == "2":
                log_warning.append(v)

    # Write logs in sort of easy to read format
    try:
        f = open(os.path.join(out_dir, args.log_file), "w")
        f.write("############## FAILURES #####################\n")
        for line in log_fails:
            if type(line) is list:
                for i in line:
                    f.write(i + "\n")
            else:
                f.write(line + "\n")
        f.write("\n############## WARNINGS #####################\n")
        for line in log_warning:
            if type(line) is list:
                for i in line:
                    f.write(i + "\n")
            else:
                f.write(line + "\n")
        if args.show_passes:
            f.write("\n############## PASSES #####################\n")
            for line in log_pass:
                if type(line) is list:
                    for i in line:
                        f.write(i + "\n")
                else:
                    f.write(line + "\n")
        print("Wrote logs to {}".format(os.path.join(out_dir, args.log_file)))
    except (FileNotFoundError, FileExistsError, OSError, IOError) as err:
        print("ERROR opening {}: {}".format(os.path.join(out_dir, args.log_file), err))
        status = 1
        return status

    # If any fails in formatting/style, fail do not continue.
    if len(log_fails) >= 1:
        status = 1
        return status

    return status


if __name__ == "__main__":
    args = parser.parse_args()
    main_status = main()

    sys.exit(main_status)
