import os
import json
import pymongo
from lib import file_list_generator, mttl_db_helpers


'''
Use this script to verify if the question name/folder in the json file matches the actual name/folder of the file.
It also checks if its MTTL Rel-Link info matches what's in the json file.
'''


def check_rel_link_paths():
    # Get Mongo DB connection
    host = os.getenv('MONGO_HOST', "localhost")
    port = int(os.getenv('MONGO_PORT', "27017"))
    client = pymongo.MongoClient(host, port)
    db = client["mttl"]
    paths = file_list_generator.dir_walk(".", ["knowledge", "performance"], ".question.json")
    for path in paths:
        is_q_data_good = True
        q_dir = os.path.dirname(os.path.relpath(path, os.getcwd())).replace("\\", "/")
        q_name = os.path.splitext(os.path.splitext(os.path.basename(path))[0])[0]  # Remove .question.json
        with open(path, "r") as path_fp:
            question = json.load(path_fp)
        if q_name != question["question_name"]:
            print(f"The question's name in {path} doesn't match '{question['question_name']}' stored in the json file.")
            is_q_data_good = False
        if q_dir != question["question_path"]:
            print(f"The question's relative path in {path} doesn't match '{question['question_path']}' "
                  f"stored in the json file.")
            is_q_data_good = False
        if is_q_data_good:
            # We know the path matches the information in the question; now check the info in the MTTL
            if question["rel-link_id"] == "":
                print(f"'{path}' needs to have its rel-link ID added.")
                continue
            rel_link = mttl_db_helpers.get_question(question["rel-link_id"], db)
            if rel_link is None or rel_link == "":
                print(f"Unable to locate rel-link info for '{path}'.")
            elif rel_link["question_name"] != question["question_name"]:
                print(f"MTTL rel-link ID '{rel_link['_id']}' needs the 'question_name' key's value updated to "
                      f"'{question['question_name']}'")

    print("\nFinished Processing.")


if __name__ == "__main__":
    check_rel_link_paths()
