#!/usr/bin/python3

import json
import os
import sys
import argparse
import pymongo
from colorama import Fore
from lib import text_to_image
from lib.file_list_generator import changed_paths, dir_walk
from lib.errors import ErrorCode
from lib.csv_helpers import csv_escape_special_chars, stringify_list_for_cell, add_csv_content
from lib.default_strings import param_question_dir_desc, opt_param_changed_desc, error_str, warning_str, newline_map, \
    group_key, group_questions_key, group_name_key, question_key, choices_key, snippet_key, answer_key, topic_key, \
    explanation_key, attempts_key, failures_key, passes_key, keyword_default, keyword_desc, rellink_id_key, \
    snippet_lang_key, question_type_key, id_key, question_prof_key, etc_key, workroles_key, rev_num_key, \
    created_on_key, updated_on_key
from lib.util import write_str_totext, write_dict_tojson
from lib import mttl_db_helpers

# Choice headers are dynamically built according to the requested amount of choices up to the supported number
col_hd_pre_choices = [
    "Question Type:",
    "Question Name:",
    "Question:"
]

col_hd_post_choices = [
    "Answer:",
    "Points:",
    "Subject:",
    "Training Ref:",
    "Image Name:",
    "Snippet:",
    "Explanation:",
    "Group Name:",
    "Group Question UIDs:",
    "Proficiency Code:",
    "Est. Time to Complete:",
    "KSATs:",
    "Work Role:",
    "UID:",
    "Version:",
    "Date Created:",
    "Date Revised:",
    "Attempts:",
    "Passes:",
    "Failures:",
    "Reviewer Section:",
    "Training Comments/Suggestions:"
]

delimiter = ","
# These are the supported number of choices
answer_map = {
    0: "A",
    1: "B",
    2: "C",
    3: "D",
    4: "E",
    5: "F",
    6: "G",
    7: "H"
}
question_choice = "CHOICE"
knowledge_points = 1

db_name_default = "mttl"
db_name_desc = "This is the name of the data base and is used for DB connection purposes. " \
               f"The default is {db_name_default}"
description = "This generates CSV files for each work role or specialization (dev topics c, networking, python, and " \
              "assembly) and images for all snippets needed in the Google Script Knowledge exam."
log_file_default = "GenGScriptFilesReport.json"
log_file_desc = f"The name of the report file; the default is {log_file_default}."
mongo_host_desc = f"This is the host name where the Mongo Database is running. " \
                  f"The default is '{mttl_db_helpers.HOST_NAME}'"
mongo_port_desc = f"This is the port where the Mongo Database is running. The default is '{mttl_db_helpers.PORT_NUM}'"
nameof_mql_csv_default = "Master_Question_List.csv"
nameof_mql_csv_desc = "The name of the Master Question List (MQL) CSV file. Note, it should have a .csv extension. " \
                      f"The default is '{nameof_mql_csv_default}'"
max_choices_desc = "This is the maximum number of choices to allow per multiple choice question. The default is 5."
out_dir_default = "gen_gscript_csv_files/"
out_dir_desc = "The directory to store the resulting Generate Google Script Files report, relative to root_dir. " \
               f"The default is {out_dir_default}."
root_dir_desc = "The path to a directory with the desired questions."

msg_er_color = f"{Fore.RED + os.path.basename(__file__)} {error_str + Fore.RESET}:"
msg_er = f"{os.path.basename(__file__)} {error_str}:"
msg_wn_color = f"{Fore.YELLOW + os.path.basename(__file__)} {warning_str + Fore.RESET}:"
msg_wn = f"{os.path.basename(__file__)} {warning_str}:"
msg_gd_color = f"{Fore.GREEN + os.path.basename(__file__) + Fore.RESET}:"
msg_gd = f"{os.path.basename(__file__)}:"


def gen_google_script_csv(questions: list, out_dir: str, mql_csv: str, max_choices: int, file_type: str,
                          csv_col_headers: list, db_obj: pymongo.database.Database, ans_map: dict = None,
                          delim: str = delimiter, quest_type: str = question_choice,
                          points: int = knowledge_points) -> dict:
    '''
    Purpose: This function takes a list of question *.question.json files and builds a CSV in a format needed for the
    Google Script Form Generator to create Knowledge exams.
    :param questions: A list of question *.question.json files.
    :param out_dir: The output directory for necessary images and resulting CSV.
    :param mql_csv: The name of the output CSV file.
    :param max_choices: The max choices allowed in the multiple choice questions.
    :param file_type: The expected type of file with the question content; i.e. .question.json.
    :param csv_col_headers: The headers to use in the output CSV file.
    :param db_obj: The PyMongo Database object
    :param ans_map: An index number to Multiple Choice option letter mapping.
    :param delim: The delimiter used to separate cells in the CSV file.
    :param quest_type: The type of question; this currently only supports CHOICE.
    :param points: The number of points given if the question is answered correctly
    :return: this_status: Empty = success; otherwise, keys indicating the error with their messages
    '''
    global answer_map
    this_status = {}
    # Stores the final CSV formatted string
    master_csv = ""
    if ans_map is None:
        ans_map = answer_map

    if len(questions) == 0:
        # In case the list is empty
        msg = "{0} Received empty file list."
        print(msg.format(msg_er_color))
        this_status[ErrorCode.EMPTY_LIST.value] = [msg.format(msg_er)]
        return this_status

    # In case the list isn't empty, start each csv file with the column headers
    master_csv = f"{','.join(csv_col_headers)}\n"

    for json_file in questions:
        # Process each question
        current_row = ""
        if not json_file.endswith(file_type):
            # Since we're only looking for json files, skip anything that doesn't end in .json
            continue

        # Process each JSON file by adding it to the CSV string
        try:
            with open(json_file, "r") as json_fp:
                json_data = json.load(json_fp)

        except json.JSONDecodeError as e:
            msg = "{0} Unable to load {1} on line #{2} position #{3}: {4}"
            print(msg.format(
                msg_er_color,
                Fore.CYAN + e.doc + Fore.RESET,
                Fore.CYAN + e.lineno + Fore.RESET,
                Fore.CYAN + e.pos + Fore.RESET, e.msg
            ))
            if ErrorCode.JSON_DECODE_ERROR.value not in this_status:
                this_status[ErrorCode.JSON_DECODE_ERROR.value] = []
            this_status[ErrorCode.JSON_DECODE_ERROR.value] = [msg.format(msg_er, e.doc, e.lineno, e.pos)]

        else:
            if question_type_key in json_data and json_data[question_type_key] == "knowledge":
                # Question Type Column; it currently only supports CHOICE
                current_row += quest_type

                # Question Name Column
                q_name = os.path.basename(json_file)
                grp_name = ""  # Stores the name of the question group when applicable
                if group_key in json_data:
                    # In case this is a group question, get the group's folder name
                    grp_name = json_data[group_key][group_name_key]
                current_row += add_csv_content(delim, f"\"{os.path.join(grp_name, q_name)}\"")

                # Question Column
                question = csv_escape_special_chars(json_data[question_key])
                current_row += add_csv_content(delim, f"\"{question}\"")

                # A, B, C, D, E Columns
                num_choices = 0  # Stores total number of options in this question
                for choice in json_data[choices_key]:
                    if choice is not None and "" != choice:
                        try:
                            int(choice)
                            current_row += add_csv_content(delim, f"=\"{csv_escape_special_chars(choice, False)}\"")
                        except ValueError:
                            current_row += add_csv_content(delim, f"\"{csv_escape_special_chars(choice, False)}\"")
                    else:
                        current_row += delim
                    num_choices += 1

                while num_choices < max_choices:
                    # In case all the options are not used, add enough empty cells to fill-in all A - E options
                    current_row += delim
                    num_choices += 1

                # Answer: Column
                current_row += add_csv_content(delim, f"{ans_map[json_data[answer_key]]}")

                # Points Column
                current_row += add_csv_content(delim, f"{points}")

                # Subject Column
                current_row += add_csv_content(delim, f"\"{csv_escape_special_chars(json_data[topic_key])}\"")

                # Training Ref. Column
                current_row += delim

                # Determine if a snippet exists
                # Image Name Column (i.e. The snippet's file name)
                # Snippet Column
                if snippet_key not in json_data or (json_data[snippet_key] is None or json_data[snippet_key] == ""):
                    # In case there's no snippet for this question
                    current_row += delim + delim  # Account for both Image Name and Snippet columns

                else:
                    # In case there's a snippet for this question
                    # Image Name Column
                    # Generate an image file for Google Forms to display and store the image name used
                    snip_dir = os.path.join(out_dir, "code_snippets")
                    if snippet_lang_key in json_data and json_data[snippet_lang_key] == "mermaid":
                        # run mermaid to image if the snippet language is specified as mermaid
                        image_name = text_to_image.mermaid_to_image(json_file,
                                                                    json_data[snippet_key],
                                                                    json_data[topic_key],
                                                                    json_data[id_key],
                                                                    snip_dir)
                    else:
                        image_name = text_to_image.snippet_image_generator(json_data, json_file, snip_dir)

                    if type(image_name) is int:
                        # In case an error occurred during image generation
                        if image_name not in this_status:
                            this_status[image_name] = []
                        this_status[image_name].append(ErrorCode.UNABLE_TO_GEN_IMAGE.name)
                        return this_status

                    current_row += add_csv_content(delim, f"{image_name}")
                    # Escape any special characters in the snippet
                    # Replace all newlines that separate each line of code with \r\n so the data remains in the
                    # current cell, instead of a new row, when opened in spreadsheet software.
                    snip = stringify_list_for_cell(json_data[snippet_key].split("\n"), False)
                    current_row += add_csv_content(delim, f"\"{snip}\"")

                # Explanation Column
                current_row += delim
                count = 0
                explain = ""  # Stores the final explanation string

                for explanation in json_data[explanation_key]:
                    if explanation != "":
                        # Ensure any special characters are escaped
                        explanation = csv_escape_special_chars(explanation)
                        # Only process if there's an actual explanation
                        if count == 0:
                            explain += explanation

                        else:
                            explain += add_csv_content(newline_map, f"{explanation}")

                        count += 1

                current_row += f"\"{explain}\""

                # Determine if this is a group question
                if group_key in json_data:
                    # In case this is a group question
                    # Group Name Column
                    current_row += add_csv_content(delim, f"{json_data[group_key][group_name_key]}")

                    # Group Question IDs Column
                    current_row += add_csv_content(
                        delim,
                        f"\"{newline_map.join(json_data[group_key][group_questions_key])}\""
                    )

                else:
                    # In case this isn't a group question, add blank data for group name & question UID columns
                    current_row += delim + delim

                # Get information from the question's rel-link data
                rellink_id = json_data[rellink_id_key]
                if rellink_id is None or rellink_id == "":
                    print(f"{Fore.YELLOW}The MTTL MongoDB rel-link ObjectID for '{Fore.CYAN + json_file + Fore.YELLOW}'"
                          f" cannot be blank.{Fore.RESET}")
                    continue
                rellink_data = mttl_db_helpers.get_question(rellink_id, db_obj)
                ksats = mttl_db_helpers.get_question_ksats(rellink_id, db_obj)
                if len(rellink_data) == 0:
                    print(f"'{Fore.RED + json_file + Fore.RESET}' doesn't have an MTTL mapping.")
                    continue

                # Proficiency Code Column
                if question_prof_key not in rellink_data:
                    pcode = ""
                else:
                    pcode = rellink_data[question_prof_key]
                current_row += add_csv_content(delim, f"{pcode}")

                # Est. Time to Complete Column
                if etc_key not in rellink_data:
                    etc_value = 0
                else:
                    etc_value = rellink_data[etc_key]
                current_row += add_csv_content(delim, f"{etc_value}")

                # KSATs Column
                current_row += add_csv_content(delim, f"\"{stringify_list_for_cell(ksats, False)}\"")

                # Workrole Column
                if workroles_key not in rellink_data:
                    workroles = [os.path.basename(os.getcwd())]
                else:
                    workroles = rellink_data[workroles_key]
                current_row += add_csv_content(delim, f"{workroles}")

                # UID Column
                current_row += add_csv_content(delim, f"{json_data[id_key]}")

                # Version Column
                if rev_num_key not in rellink_data:
                    rev_num = 0
                else:
                    rev_num = rellink_data[rev_num_key]
                current_row += add_csv_content(delim, f"\"{rev_num}\"")

                # Date Created Column
                if created_on_key not in rellink_data:
                    created = ""
                else:
                    created = rellink_data[created_on_key]
                current_row += add_csv_content(delim, f"\"{csv_escape_special_chars(created)}\"")

                # Date Revised Column
                if updated_on_key not in rellink_data:
                    updated = ""
                else:
                    updated = rellink_data[updated_on_key]
                current_row += add_csv_content(delim, f"\"{csv_escape_special_chars(updated)}\"")

                # Attempts Column
                current_row += add_csv_content(delim, f"{json_data[attempts_key]}")

                # Passes Column
                current_row += add_csv_content(delim, f"{json_data[passes_key]}")

                # Failures Column
                current_row += add_csv_content(delim, f"{json_data[failures_key]}")

                # Reviewer Section Column
                current_row += delim

                # Training Comments/Suggestions Column
                current_row += delim

                current_row += "\n"  # The end of the row

                # Add row to master csv file
                master_csv += current_row

    write_status = write_str_totext(master_csv, os.path.join(out_dir, mql_csv), "w")
    if ErrorCode.SUCCESS.value in write_status and type(write_status[ErrorCode.SUCCESS.value]) is not list:
        write_status[ErrorCode.SUCCESS.value] = [write_status[ErrorCode.SUCCESS.value]]

    this_status.update(write_status)

    return this_status


def get_col_headers(max_choices: int, ans_map: dict = None, pre_choices_header: list = None,
                    post_choices_header: list = None) -> list:
    """
    Purpose: This adds the requested amount of choice headers between the left and right column headers.
    :param max_choices: An integer representing the maximum amount of options per multiple choice question.
    :param ans_map: A dictionary mapping the index number to its expected lettered choice.
    :param pre_choices_header: A list representing the headers appearing to the left of the multiple choice options
    :param post_choices_header: A list representing the headers appearing to the right of the multiple choice options
    :return: list: The new combined column header list
    """
    # Builds the necessary CSV column headers
    if ans_map is None:
        ans_map = answer_map
    if pre_choices_header is None:
        pre_choices_header = [] + col_hd_pre_choices
    if post_choices_header is None:
        post_choices_header = [] + col_hd_post_choices
    for ind in range(max_choices):
        # Add the requested amount of Choice headers
        if ind in ans_map:
            # Prevent trying to access a non-existent key the answer_map
            pre_choices_header.append(ans_map[ind])
    return pre_choices_header + post_choices_header


def main(args: argparse.Namespace) -> dict:
    '''
    Purpose: This function validates requested input and sets necessary variables/data in order to be ready to generate
    the CSV and any supporting images.
    :return: this_status: a dictionary with keys representing error codes according to class Error or the exception,
             when present.
    '''
    this_status = {}
    out_dir = args.out_dir

    if args.question_dirs is None:
        args.question_dirs = ["."]

    if not args.nameof_mql_csv.lower().endswith(".csv"):
        # In case the requested MQL csv file is missing the .csv extension
        args.nameof_mql_csv += ".csv"

    # Get Mongo DB connection
    host = os.getenv('MONGO_HOST', args.Host)
    port = int(os.getenv('MONGO_PORT', args.Port))
    client = pymongo.MongoClient(host, port)
    db = client[args.db_name]

    try:
        if not os.path.exists(out_dir):
            # In case the output folder doesn't exist, create it
            os.makedirs(out_dir)
            msg = "{0} Created '{1}' for output files."
            print(msg.format(msg_gd_color, Fore.CYAN + out_dir + Fore.RESET))
            this_status[ErrorCode.SUCCESS.value] = [msg.format(msg_gd, out_dir)]

        if args.changed:
            # In case only changed files are requested
            paths = changed_paths(repo_base_dir=args.root_dir, keyword=args.filter)
        else:
            # In case all files are requested
            paths = dir_walk(root_dir=args.root_dir, json_dirs=args.question_dirs, keyword=args.filter)

        this_status.update(gen_google_script_csv(paths, out_dir, args.nameof_mql_csv, args.max_choices, args.filter,
                                                 col_headers, db))

    except ValueError as err:
        print(f"{msg_er_color} {ErrorCode.INVALID_INPUT.name}: {err}")
        if ErrorCode.JSON_DECODE_ERROR.value not in this_status:
            this_status[ErrorCode.JSON_DECODE_ERROR.value] = []
        this_status[ErrorCode.JSON_DECODE_ERROR.value].append(f"{msg_er} {ErrorCode.INVALID_INPUT.name}: {err}")
        return this_status

    except (FileNotFoundError, FileExistsError, OSError, IOError) as err:
        # In case the file cannot be opened, display a reason and stop processing
        msg = f"({err.errno}){err}"
        print(f"{msg_er_color} {msg}")
        if err.errno not in this_status:
            this_status[err.errno] = []
        this_status[err.errno].append(f"{msg_er} {msg}")
        return this_status

    return this_status


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("root_dir", type=str, help=root_dir_desc)
    parser.add_argument("-c", "--changed", action="store_true", help=opt_param_changed_desc)
    parser.add_argument("-d", "--db_name", type=str, default=db_name_default, help=db_name_desc)
    parser.add_argument("-f", "--filter", type=str, default=keyword_default, help=keyword_desc)
    parser.add_argument("-H", "--Host", type=str, default=mttl_db_helpers.HOST_NAME, help=mongo_host_desc)
    parser.add_argument("-l", "--log_file", type=str, default=log_file_default, help=log_file_desc)
    parser.add_argument("-m", "--max_choices", type=int, choices=range(2, 9), default=5, help=max_choices_desc)
    parser.add_argument("-n", "--nameof_mql_csv", type=str, default=nameof_mql_csv_default, help=nameof_mql_csv_desc)
    parser.add_argument("-o", "--out_dir", type=str, default=out_dir_default, help=out_dir_desc)
    parser.add_argument("-P", "--Port", type=str, default=mttl_db_helpers.PORT_NUM, help=mongo_port_desc)
    parser.add_argument("-q", "--question_dirs", nargs='+', type=str, default=None, help=param_question_dir_desc)
    params = parser.parse_args()
    # Builds the necessary CSV column headers
    col_headers = get_col_headers(params.max_choices, answer_map)
    # Now that the column headers are built, we're ready to build the CSV and supporting images
    status = main(params)
    if (len(status) == 1 and ErrorCode.SUCCESS.value in status) or len(status) == 0:
        msg = "The Master Question List and supporting images for the Google Script Form Generator was generated " \
              "successfully."
        msg_loc = "Output files were saved in '{0}'."
        print("{0} {1}\n{2}".format(
            msg_gd_color,
            msg,
            msg_loc.format(Fore.CYAN + params.out_dir + Fore.RESET)
        ))
        if len(status) == 0:
            status[ErrorCode.SUCCESS.value] = []
        status[ErrorCode.SUCCESS.value].append("{0} {1}".format(msg_gd, msg))
        status[ErrorCode.SUCCESS.value].append(msg_loc.format(params.out_dir))
    write_dict_tojson(status, os.path.join(params.out_dir, params.log_file), mode="w", indent=4)
    if not (len(status) == 1 and ErrorCode.SUCCESS.value in status):
        # In case status has errors, prevent returning zero
        if ErrorCode.SUCCESS.value in status:
            # For cases where ErrorCode.SUCCESS.value isn't present
            del status[ErrorCode.SUCCESS.value]
    sys.exit(list(status.keys())[0])
