import os
import json
import pymongo
from colorama import Fore
from lib import markdown_helpers
from lib import mttl_db_helpers
from lib.default_strings import readme, answer_key, question_key, choices_key, explanation_key, snippet_key, \
    topic_key, snippet_lang_key, rellink_id_key

num_md_cols = 4  # The expected number of columns in the markdown table
md_col_labels = "| A | B | C | D |\n"
md_col_format = "|:---|:---|:---|:---|\n"  # Serves as a horizontal line separator and cell text justification
answer_map_to_let = {
    0: "A",
    1: "B",
    2: "C",
    3: "D",
    4: "E",
    5: "F",
    6: "G",
    7: "H"
}  # To easily convert index numbers to multiple choice options
answer_header = "## Answer\n"
answer_link = "Click [here]({link}) to see the answers.\n"  # Allows viewing the answers when desired
back_link = "[**<- Back To {role}**]({link})\n"  # Allows returning to the master README Questions list when desired
answer_options_msg = "## Answer\n- {option}: {answer}\n"  # A quick view of the correct multiple choice answers
answer_essay_msg = "##Answer\n\t{answer}\nFeedback:\n\n\t{feedback}\n"  # Feedback of the correct essay answer
explain_msg = "Feedback:\n\n{feedback}\n"
bold_italic = "***{strong}***"
bold = "**{bold}**"
italic = "*{italic}*"
topic_map = {
    "assembly": "nasm",
    "python": "python",
    "networking": "python",
    "c": "c",
    "algorithms": ""
}


def build_snippet(data: dict, language: str, snip_key: str = snippet_key) -> str:
    '''
    Purpose: This function takes in a dictionary representation of a question's JSON file and builds a markdown version
    of its snippet, if applicable. Note: key should be the dictionary key that has the snippet text.
    :param data: The dictionary object representing a question's JSON file
    :param language: The programming language the code snippet represents
    :param snip_key: A dictionary key pointing to the text containing the code snippet
    :return: A markdown formatted string representing the code snippet or None if there's no snippet
    '''
    snippet = ""

    if snip_key in data and data[snip_key] != "" and data[snip_key] is not None:
        # In case there's a snippet for processing
        snippet = "```{subject}\n".format(subject=language.lower())
        snippet += data[snip_key]  # These are the actual lines of code in the snippet
        snippet += "\n```\n\n"  # Indicates the code block is finished and adds a blank line afterwards

    return snippet


def build_markdown_table(data: dict, labels: str, vert_line_fmt: str, num_cols: int, sub_key: str = choices_key,
                         ans_index: int = 0, is_ans: bool = False) -> str or None:
    '''
    Purpose: This function creates a markdown table from the input dictionary formatted data using the desired list in
    data, indicated by the input key. Note, this function expects key to have a list as its value, i.e. of multiple
    choice options or explanation statements of each option. Upon success, a string representation of the markdown
    table is returned.
    :param data: The dictionary object representing a question's JSON file
    :param labels: The desired column labels for the table
    :param vert_line_fmt: The vertical lines for the table and the formatting of each columns cell justification
    :param num_cols: The number of columns in the table
    :param sub_key: The desired list to write to the table; for example, multiple choice options or explanation
                    statements
    :param ans_index: Specifies the index to the correct multiple choice answer
    :param is_ans: Determines if the correct answer should be highlighted with bold & italic text
    :return: A string representing a markdown compatible table of multiple choice options
    '''
    if is_ans:
        # In case this is an answer table, highlight the column label
        labels = labels.replace(answer_map_to_let[ans_index], bold_italic.format(strong=answer_map_to_let[ans_index]))

    md_table = labels + vert_line_fmt

    if len(data[sub_key]) != 0:
        # In case there are values to write to the table
        md_table += "|"
        count = 0  # To track the current index

        for option in data[sub_key]:
            # Write the current cell value for this row
            if count < num_cols:
                # In case there are more columns in the table
                # Escape necessary characters for proper markdown rendering.
                option = markdown_helpers.escape_chars_for_md(option)
                if is_ans and count == ans_index:
                    # In case this is the answer table, highlight the answer with bold & italic
                    option = bold_italic.format(strong=option)

                # For correct markdown rendering, convert new line \n to html <br> when between words
                md_table += " {0} |".format(option.replace("\n", "<br>"))

            count += 1

        md_table += "\n\n"  # A blank line after the table prevents additional text being added as a table row

    return md_table


def markdown_generator(json_file: str, out_answer_url: list, db_obj: pymongo.database.Database,
                       gen_snippet: bool = True, is_answer: bool = True, ans_key: str = answer_key,
                       choice_key: str = choices_key, explan_key: str = explanation_key, quest_key: str = question_key,
                       snip_key: str = snippet_key, top_key: str = topic_key) -> str or int:
    '''
    Purpose: This function takes in: 1) a string representing the path to a file, expected to be a question JSON file;
    2) the link to the directory that contains the questions's answer, which is in its README.md file; 3) True to
    indicate the snippet should be generated; and 4) True to indicate this is the answer README.md or False to indicate
    this is not the answer README.md. When is_answer is False, the table will be appended to the repo's README.md file.
    Upon successful operation, the generated markdown files are saved in the requested directory.
    :param json_file: This is the path to a json file containing the question's properties
    :param out_answer_url: This is the HTTP URL directory that contains the answer file
    :param db_obj: The PyMongo Database object
    :param gen_snippet: Determines if a snippet should be generated, useful for group questions, default is True
    :param is_answer: This determines if the output README.md file should contain the answer, default is True
    :param ans_key: The name of the answer key within the knowledge dictionary in template.json. Default = "answer"
    :param choice_key: The name of the choices key within the knowledge dictionary in template.json. Default = "choices"
    :param explan_key: The name of the explanation key within the knowledge dictionary in template.json.
                       Default = "explanation"
    :param quest_key: The name of the question key within the knowledge dictionary in template.json.
                      Default = "question"
    :param snip_key: The name of the snippet key within the knowledge dictionary in template.json. Default = "snippet"
    :param top_key: The name of the topic key in template.json. Default = "topic"
    :return: 1 = invalid Input file; 2 = Unexpected JSON data; 3 = Request Not Yet Implemented
             md_content = Success, the markdown string representing the question
    '''
    md_content = ""

    if not os.path.exists(json_file):
        # In case the input file and output directory are invalid
        return md_content

    if len(out_answer_url) == 0:
        print("{0}: At least one output return URL is required.".format(Fore.RED + "ERROR" + Fore.RESET))
        return md_content

    for url in out_answer_url:
        if "" == url:
            print("{0}: The output return URL cannot be blank.".format(Fore.RED + "ERROR" + Fore.RESET))
            return md_content

    # Open the json to extract code snippet using the snippet key
    with open(json_file, "r") as read_file:
        data = json.load(read_file)

        if top_key in data and quest_key in data and choice_key in data and ans_key in data and explan_key in data:
            # This is a knowledge question
            # Process the README.md file
            rellink_id = data[rellink_id_key]
            if rellink_id is None or rellink_id == "":
                print(f"{Fore.YELLOW}The MTTL MongoDB rel-link ObjectID for '{Fore.CYAN + json_file + Fore.YELLOW}'"
                      f" cannot be blank.{Fore.RESET}")
                return md_content
            if snip_key not in data:
                # In case there's no snippet
                snippet = None
            elif (data[snip_key] is not None and data[snip_key] != "") and gen_snippet:
                # In case there's a snippet
                try:
                    subject = data[snippet_lang_key]
                except KeyError:
                    # if the snippet language is not specified infer it from the topic
                    subject = data[top_key].split(" ")[0].strip()  # Assumes the language is first
                    try:
                        subject = topic_map[subject.lower()]
                    except KeyError:
                        subject = ""

                # Otherwise, it's assumed the language is a markdown compatible name such as c or python
                snippet = build_snippet(data, subject)
                md_content += snippet

            # Write the question so there's a blank line after it
            md_content += "## {0}\n\n".format(markdown_helpers.escape_chars_for_md(data[quest_key]))

            # To store all KSAs in a string
            ksats = ", ".join(mttl_db_helpers.get_question_ksats(rellink_id, db_obj))

            md_content += "KSATs: {0}\n\n".format(ksats)
            # Get the number of choices available to the question
            num_md_cols = len(data[choice_key])
            # Set the format for each column in the choices table
            md_col_format = "{0}\n".format(markdown_helpers.set_md_table_cell_justify(num_cols=num_md_cols))
            count = 0  # Keeps track of each option worked in a loop
            option = ord('A')  # Options start with A
            md_col_labels = []  # Stores each option letter
            while count < num_md_cols:
                # Build a list of option letters for each option in the question
                md_col_labels.append(chr(option))
                option += 1
                count += 1
            # Convert the list into a markdown column label string
            md_col_labels = "| {0} |\n".format(" | ".join(md_col_labels))

            if not is_answer:
                # In case the answer shouldn't be displayed
                if num_md_cols > 0:
                    md_content += build_markdown_table(data, md_col_labels, md_col_format, num_md_cols, choice_key)

                # Add each answer URL.
                for url in out_answer_url:
                    if not url.endswith('/') and not url.endswith("\\"):
                        # In case the output directory is missing the trailing slash or backslash
                        url += '/'  # This works regardless of slash or backslash in path

                    md_content += answer_link.format(link=url + readme)

            else:
                # In case the answer should be displayed
                md_content += answer_header

                if num_md_cols > 0:
                    md_content += build_markdown_table(data, md_col_labels, md_col_format, num_md_cols, choice_key,
                                                       data[ans_key], True)

                    # Add a blank line between the summary and the explanation
                    md_content += "\nFeedback:\n\n"
                    reasons = 0

                    for feedback in data[explan_key]:
                        # Count the number of actual reasons
                        if feedback != "":
                            # In case the reason is not an empty string
                            reasons += 1

                    if reasons >= 3:
                        # In case there are explanations for each choice
                        choice = ord("A")

                        for reason in data[explan_key]:
                            if reason != "":
                                # In case this reason isn't an empty string
                                md_content += f"- {chr(choice)}) {markdown_helpers.escape_chars_for_md(reason)}\n"
                                choice += 1

                    else:
                        # In case there isn't one explanation for every choice
                        for reason in data[explan_key]:
                            if reason != "":
                                # In case this reason isn't an empty string
                                md_content += f"- {markdown_helpers.escape_chars_for_md(reason)}\n"

                for url in out_answer_url:
                    workrole = os.path.basename(url).split(".")[0]
                    md_content += "\n{0}\n".format(back_link.format(role=workrole.replace("_", " "), link=url))

        else:
            # The input json file does not contain the expected information
            return md_content

    return md_content
