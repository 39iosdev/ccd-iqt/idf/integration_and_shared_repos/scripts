import pymongo
import json
from colorama import Fore
from lib.exam_generator import questioncategory, evalquestion
from lib.default_strings import topic_key, provisioned_key, attempts_key, passes_key, etc_key, rellink_id_key
from lib import mttl_db_helpers


class EvalBuilder:
    def __init__(self):
        self.question_list = []
        self.category_list = []
        self.ksat_set = set()
        self.exams_generated = 0
        self.exams_to_generate = 0
        self.time_allowed = 0
        self.delta = 0  # The difference between the amount of questions generated and requested, as a percentage

    def create_exam(self, exam_type: str, prev_exams_list: list = None):
        ex_list = []
        time_used = 0
        if prev_exams_list is None:
            prev_exams_list = []
        for cat in self.category_list:
            cat_list, time_used = self.get_category_questions(cat[0], cat[1], exam_type, self.time_allowed, time_used)
            ex_list += cat_list

        for prev_exam in prev_exams_list:
            # Prevent creation of exact duplicate exams
            if sorted([x.question_id for x in ex_list]) == sorted(prev_exam):
                ex_list = []
                time_used = 0
                for cat in self.category_list:
                    cat_list, time_used = self.get_category_questions(cat[0], cat[1], exam_type, self.time_allowed,
                                                                      time_used, rand_list=True)
                    ex_list += cat_list

        return [x.question_id for x in ex_list]

    def register_category(self, category_name, amount):
        self.category_list.append((category_name, amount))

    def get_category_questions(self, category_name, amount, exam_type: str, time_allowed: int, time_used: int,
                               rand_list: bool = False):
        # check for category
        category = questioncategory.QuestionCategory(category_name, amount)
        # get top amount of questions and return them
        for question in self.question_list:
            if question.topic == category_name and exam_type.lower() in question.question_id.lower():
                category.question_list.append(question)
        if rand_list:
            exam, time_used = category.create_rand_quest_set(amount, time_allowed, time_used)
        else:
            exam, time_used = category.create_question_set(amount, time_allowed, time_used)
        if amount - len(exam) > self.delta * amount:
            # If the difference is greater than the allowed delta
            msg = f"Delta between requested size '{amount}' and generated size '{len(exam)}' ({amount - len(exam)}) " \
                  f"> '{self.delta * amount}'"
            raise ValueError(msg)
        return exam, time_used

    def sort_cat_list_by_value(self):
        self.category_list = sorted(self.category_list, key=lambda category: category[1])

    def clear_categories(self):
        self.category_list = []

    def feedback_update(self, exam_q_list):
        self.exams_generated += 1
        for q_id in exam_q_list:
            for q in self.question_list:
                if q_id == q.question_id:
                    q.times_provisioned += 1
                    # update attempts
                    # update failures

        # reset weights to normal
        for q in self.question_list:
            q.get_question_weight(self.exams_generated)

    def set_question_list(self, dirs, db_obj: pymongo.database.Database):
        q_list = []
        for f_path in dirs:
            try:
                with open(f_path, "r") as j_file:
                    q_dat = json.load(j_file)
                rellink_id = q_dat[rellink_id_key]
                if rellink_id is None or rellink_id == "":
                    print(f"{Fore.YELLOW}The MTTL MongoDB rel-link ObjectID for '{Fore.CYAN + f_path + Fore.YELLOW}'"
                          f" cannot be blank.{Fore.RESET}")
                    continue
                q_rellink_data = mttl_db_helpers.get_question(rellink_id, db_obj)
                new_q = evalquestion.EvalQuestion(f_path, q_dat[topic_key], q_dat[provisioned_key], q_dat[attempts_key],
                                                  q_dat[passes_key], q_rellink_data[etc_key])
                new_q.ksat_list = mttl_db_helpers.get_question_ksats(rellink_id, db_obj)
                new_q.get_question_weight(self.exams_generated)
                q_list.append(new_q)
            except FileNotFoundError:
                print(f"Could not read file {f_path}")
            except KeyError as err:
                print(f"Key Not Found: {err}")
            except TypeError as err:
                print(f"'{f_path}': {err}")
        self.question_list = q_list

    def get_question(self, q_id: str) -> evalquestion.EvalQuestion:
        for question in self.question_list:
            if q_id == question.question_id:
                return question
