class EvalQuestion:
    def __init__(self, question_id=None, topic: str = "", times_provisioned: int = 0, times_asked: int = 0,
                 correct_answers: int = 0, complete_time: int = 0):
        self.question_id = question_id
        self.times_provisioned = times_provisioned
        self.times_asked = times_asked
        self.correct_answers = correct_answers
        self.ksat_list = []
        self.weight = 1.0
        self.topic = topic
        self.complete_time = complete_time
        self.q_weight_scalar = 1.0

    def get_accuracy(self):
        '''
        :return: A percentage representing the pass rate of this question
        '''
        return max(self.correct_answers, 1) / max(self.times_asked, 1)

    # returns the reciprocal of the ratio between the number of tests and the number of times
    # the question has been used in a test
    # fewer uses_test should return higher weights
    def get_question_weight(self, exams_generated: int):
        # simplification of 1 / (uses_test / exams_generated)
        # 1 / (uses_test / exams_generated) = exams_generated / uses_test
        # exams_generated is the number of exams created since the beginning
        # uses_test is the number of times the question was provisioned on an exam; this is different from times
        # attempted
        self.weight = self.q_weight_scalar * (max(exams_generated, 1) / (max(self.times_provisioned, 1)))
        return self.weight
