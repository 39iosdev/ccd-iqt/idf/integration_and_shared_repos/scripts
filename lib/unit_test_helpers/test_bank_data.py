'''
These questions are for testing purposes only and is necessary to build an environment suitable for testing.
'''
knowledge = {
    "knowledge/Algorithms/algorithms-big-O/algorithms-big-O.question.json":
        {
            "_id": "BD_ALG_0001",
            "rel-link_id": "5ee9250b761cf87371cf315e",
            "question_name": "algorithms-big-O",
            "question_path": "knowledge/Algorithms/algorithms-big-O",
            "question_type": "knowledge",
            "topic": "Algorithms",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "In computer science, what term is used for the notation to classify algorithms according "
                        "to how their running time or space requirements grow as the input size grows?",
            "choices": [
                "Capacity measure",
                "Big O notation",
                "Complexity rating",
                "Weighted measure"
            ],
            "answer": 1,
            "explanation": [
                "Big-O is the common term used to classify a sorting or searching algorithm according to the size "
                "of the data structure being used."
            ]
        },
    "knowledge/Algorithms/algorithms-breadth-first/breadth-first.question.json":
        {
            "_id": "BD_ALG_0008",
            "rel-link_id": "5ee9250b761cf87371cf315f",
            "question_name": "breadth-first",
            "question_path": "knowledge/Algorithms/algorithms-breadth-first",
            "question_type": "knowledge",
            "topic": "Algorithms",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "In the above graph, using a breadth-first traversal, which node would be searched next "
                        "given the current search was on node B?",
            "snippet": "graph TD\n  A-->B\n  A-->C\n  B-->D\n  B-->E\n  C-->F\n  C-->G\n",
            "snippet_lang": "mermaid",
            "choices": [
                "C",
                "D",
                "E",
                "A"
            ],
            "answer": 0,
            "explanation": [
                "A breadth-first traversal will visit each node in a tree at the same level before moving down in "
                "the tree to the next level."
            ]
        },
    "knowledge/Algorithms/algorithms-depth-first/depth-first.question.json":
        {
            "_id": "BD_ALG_0009",
            "rel-link_id": "5ee9250b761cf87371cf3163",
            "question_name": "depth-first",
            "question_path": "knowledge/Algorithms/algorithms-depth-first",
            "question_type": "knowledge",
            "topic": "Algorithms",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "Referring to the depicted graph, using a depth-first traversal, which node would be "
                        "searched next given the current search was on node B?",
            "snippet": "graph TD\n  A-->B\n  A-->C\n  B-->D\n  B-->E\n  C-->F\n  C-->G\n",
            "snippet_lang": "mermaid",
            "choices": [
                "C",
                "D",
                "E",
                "D or E depending on implementation"
            ],
            "answer": 3,
            "explanation": [
                "A depth-first traversal will visit each node in a branch of a tree before moving to the next "
                "branch. If the branch splits, it depends on the specific algorithm implementation on which branch "
                "to go down."
            ]
        },
    "knowledge/Algorithms/algorithms-merge-sort/merge-sort.question.json":
        {
            "_id": "BD_ALG_0005",
            "rel-link_id": "5ee9250b761cf87371cf3160",
            "question_name": "merge-sort",
            "question_path": "knowledge/Algorithms/algorithms-merge-sort",
            "question_type": "knowledge",
            "topic": "Algorithms",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "Pertaining to Time Complexity, what is the average case Big(O) for the merge sort "
                        "algorithm for an array containing n elements?",
            "choices": [
                "O(1)",
                "O(n)",
                "O(n*log(n))",
                "O(n*n)"
            ],
            "answer": 2,
            "explanation": [
                "The average case sorting for merge sort is O(n*log(n))"
            ]
        },
    "knowledge/Algorithms/algorithms-heap-sort/heap-sort.question.json":
        {
            "_id": "BD_ALG_0003",
            "rel-link_id": "5ee9250b761cf87371cf3161",
            "question_name": "heap-sort",
            "question_path": "knowledge/Algorithms/algorithms-heap-sort",
            "question_type": "knowledge",
            "topic": "Algorithms",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "Pertaining to Time Complexity, what is the average case Big(O) for the heap sort algorithm for"
                        " an array containing n elements?",
            "choices": [
                "O(1)",
                "O(n)",
                "O(n*log(n))",
                "O(n*n)"
            ],
            "answer": 2,
            "explanation": [
                "The average case sorting for heap sort is O(n log n)"
            ]
        },
    "knowledge/Algorithms/algorithms-insertion-sort/insertion-sort.question.json":
        {
            "_id": "BD_ALG_0004",
            "rel-link_id": "5ee9250b761cf87371cf3164",
            "question_name": "insertion-sort",
            "question_path": "knowledge/Algorithms/algorithms-insertion-sort",
            "question_type": "knowledge",
            "topic": "Algorithms",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "Pertaining to Time Complexity, what is the average case Big(O) for the insertion sort "
                        "algorithm for an array containing n elements?",
            "choices": [
                "O(1)",
                "O(n)",
                "O(n*log(n))",
                "O(n*n)"
            ],
            "answer": 3,
            "explanation": [
                "The average case sorting for insertion sort is O(n*n)"
            ]
        },
    "knowledge/Algorithms/algorithms-selection-sort/selection-sort.question.json":
        {
            "_id": "BD_ALG_0007",
            "rel-link_id": "5ee9250b761cf87371cf3162",
            "question_name": "selection-sort",
            "question_path": "knowledge/Algorithms/algorithms-selection-sort",
            "question_type": "knowledge",
            "topic": "Algorithms",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "Pertaining to Time Complexity, what is the average case Big(O) for the selection sort "
                        "algorithm for an array containing n elements?",
            "choices": [
                "O(1)",
                "O(n)",
                "O(n*log(n))",
                "O(n*n)"
            ],
            "answer": 3,
            "explanation": [
                "The average case sorting for selection sort is O(n*n)"
            ]
        },
    "knowledge/Assembly/assembly-arithmetic-instructions_2/assembly-arithmetic-instructions_2.question.json":
        {
            "_id": "BD_ASM_0003",
            "rel-link_id": "5ee9250b761cf87371cf3181",
            "question_name": "assembly-arithmetic-instructions_2",
            "question_path": "knowledge/Assembly/assembly-arithmetic-instructions_2",
            "question_type": "knowledge",
            "topic": "Assembly",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "Which register in Assembly is the default register used for a counter?",
            "choices": [
                "rax",
                "rcx",
                "rdx",
                "rdi"
            ],
            "answer": 1,
            "explanation": [
                "Incorrect: rax is where values are returned from functions",
                "Correct: rcx is the default counter register",
                "Incorrect: rdx can be freely used but is not the default counter",
                "Incorrect: rdi is used in 64-bit Linux as function argument #1 and in 64-bit Windows it's "
                "preserved"
            ]
        },
    "knowledge/Assembly/assembly-basics-and-memory_4/assembly-basics-and-memory_4.question.json":
        {
            "_id": "BD_ASM_0011",
            "rel-link_id": "5ee9250b761cf87371cf316a",
            "question_name": "assembly-basics-and-memory_4",
            "question_path": "knowledge/Assembly/assembly-basics-and-memory_4",
            "question_type": "knowledge",
            "topic": "Assembly",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "Which of the following is a general purpose register?",
            "choices": [
                "ebp",
                "rri",
                "esp",
                "rdx"
            ],
            "answer": 3,
            "explanation": [
                "Incorrect: ebp is not a general purpose register, it is a base pointer register",
                "Incorrect: rri is not a valid assembly register name",
                "Incorrect: esp is not a general purpose register, it is a stack pointer register",
                "Correct: rdx is the standard general purpose register in x86_64 assembly"
            ]
        },
    "knowledge/Assembly/assembly-negative-bitwise_2/assembly-negative-bitwise_2.question.json":
        {
            "_id": "BD_ASM_0029",
            "rel-link_id": "5ee9250b761cf87371cf3178",
            "question_name": "assembly-negative-bitwise_2",
            "question_path": "knowledge/Assembly/assembly-negative-bitwise_2",
            "question_type": "knowledge",
            "topic": "Assembly",
            "disabled": False,
            "provisioned": 1,
            "attempts": 2,
            "passes": 2,
            "failures": 0,
            "question": "In the above Assembly code, what's contained in the rax register when finished?",
            "snippet": "1 | mov rax, 2\n2 | shr rax, 2\n3 | shl rax, 2\n4 | ",
            "snippet_lang": "nasm",
            "choices": [
                "2",
                "1",
                "0",
                "4"
            ],
            "answer": 2,
            "explanation": [
                "mov rax, 2 puts ...00000010 into rax, shr (shift right) 2, shifts bits 2 to right resulting in "
                "...00000000 in rax, shl (shift left) 1 shifts all bits one to left but has no affect because "
                "register is all zeros"
            ]
        },
    "knowledge/Assembly/assembly-negative-numbers-and-bitwise_1/assembly-negative-numbers-and-bitwise_1.question.json":
        {
            "_id": "BD_ASM_0031",
            "rel-link_id": "5ee9250b761cf87371cf3185",
            "question_name": "assembly-negative-numbers-and-bitwise_1",
            "question_path": "knowledge/Assembly/assembly-negative-numbers-and-bitwise_1",
            "question_type": "knowledge",
            "topic": "Assembly",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "In Assembly, given the above what will rax contain in binary?",
            "snippet": "1 | mov rax, 1\n2 | mov rcx, 5\n3 | and rax, rcx\n4 | ",
            "snippet_lang": "nasm",
            "choices": [
                "00000001",
                "00000100",
                "00000110",
                "00000101"
            ],
            "answer": 0,
            "explanation": [
                "Correct: mov rax, 1 puts ...00000001 in rax, mov rcx, 5 puts ...00000101 in rcx, and-ing rax with "
                "rcx results in 00000001"
            ]
        },
    "knowledge/Assembly/assembly-arithmetic-instructions_8/assembly-arithmetic-instructions_8.question.json":
        {
            "_id": "BD_ASM_0044",
            "rel-link_id": "5ee9250b761cf87371cf316f",
            "question_name": "assembly-arithmetic-instructions_8",
            "question_path": "knowledge/Assembly/assembly-arithmetic-instructions_8",
            "question_type": "knowledge",
            "topic": "Assembly",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "Which is true about the x86/x86-64 DIV instruction?",
            "choices": [
                "The dividend register is the first operand and the divisor register is the second operand.",
                "The dividend is assumed to be in the AX register.",
                "The first operand is the register to store the quotient.",
                "The FDIV flag is used to set the division mode between integer and floating point division."
            ],
            "answer": 1,
            "explanation": [
                "div assumes the dividend to be in the AX register and the sole operand to be the divisor. The quotient"
                " is placed in the AX register with the remainder in the DX register."
            ]
        },
    "knowledge/Assembly/assembly-creating-asm_1/assembly-creating-asm_1.question.json":
        {
            "_id": "BD_ASM_0021",
            "rel-link_id": "5ee9250b761cf87371cf3177",
            "question_name": "assembly-creating-asm_1",
            "question_path": "knowledge/Assembly/assembly-creating-asm_1",
            "question_type": "knowledge",
            "topic": "Assembly",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "The text section of an Assembly program must begin with what declaration?",
            "choices": [
                "_begin",
                "start",
                "section",
                "global _start"
            ],
            "answer": 3,
            "explanation": [
                "Correct: The text must begin with the declaration global _start. This tells the kernel where the "
                "program execution begins"
            ]
        },
    "knowledge/Assembly/assembly-instructions-movzx/assembly-instructions-movzx.question.json":
        {
            "_id": "BD_ASM_0045",
            "rel-link_id": "5ee9250b761cf87371cf3174",
            "question_name": "assembly-instructions-movzx",
            "question_path": "knowledge/Assembly/assembly-instructions-movzx",
            "question_type": "knowledge",
            "topic": "Assembly",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "What best describes the x86/x86-64 movzx instruction?",
            "choices": [
                "Copy the contents from the second operand into the first operand.",
                "Move the contents from the second operand into the first operand and set the second operand to zero.",
                "Copy the contents from the second operand into the first operand and fill the low bits with zeros",
                "Copy the contents from the second operand into the first operand and fill the high bits with zeros"
            ],
            "answer": 3,
            "explanation": [
                "movzx will copy the contents from the source to the destination and extends to a full word or dword"
            ]
        },
    "knowledge/C_Programming/c-arithmetic-operators_1/c-arithmetic-operators_1.question.json":
        {
            "_id": "BD_C_0002",
            "rel-link_id": "5ee9250b761cf87371cf31d1",
            "question_name": "c-arithmetic-operators_1",
            "question_path": "knowledge/C_Programming/c-arithmetic-operators_1",
            "question_type": "knowledge",
            "topic": "C Programming",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "What is the output of the above C code snippet?",
            "snippet": "1 | int a = 10/3;\n2 | printf(\"%d\",a);\n3 | ",
            "snippet_lang": "c",
            "choices": [
                "3.33",
                "3.0",
                "3",
                "0"
            ],
            "answer": 2,
            "explanation": [
                "dividing an integer by another integer will result in an integer. Any floating point results will "
                "be lost."
            ]
        },
    "knowledge/C_Programming/c-group_1/c-functions_2/c-functions_2.question.json":
        {
            "_id": "BD_C_0025",
            "rel-link_id": "5ee9250b761cf87371cf31fa",
            "question_name": "c-functions_2",
            "question_path": "knowledge/C_Programming/c-group_1/c-functions_2",
            "question_type": "knowledge",
            "topic": "C Programming",
            "disabled": False,
            "provisioned": 1,
            "attempts": 2,
            "passes": 2,
            "failures": 0,
            "question": "Which line of code contains a function prototype?",
            "snippet": "1  | #include <stdio.h>\n2  | #include <stdlib.h>\n3  | \n4  | #define y 10\n5  | \n"
                       "6  | float getQuotient(int, int);\n7  | \n8  | int main()\n9  | {\n"
                       "10 |     int x = rand();\n11 | \n12 |     float z = getQuotient(x,y);\n13 |     \n"
                       "14 |     float* p = (float *) malloc(y * sizeof(float));\n15 |     \n16 |     p[0] = z;\n"
                       "17 |     \n18 |     printf(\"Result is %f\", *p);\n19 |     \n20 |     return 0;\n21 | }\n"
                       "22 | \n23 | float getQuotient(int numerator, int denominator)\n24 | {\n"
                       "25 |     return (float)numerator/denominator;\n26 | }\n27 | ",
            "snippet_lang": "c",
            "choices": [
                "10",
                "6",
                "12",
                "19"
            ],
            "answer": 1,
            "explanation": [
                "Incorrect: Line 10 is calling (invoking) an existing library function rand()",
                "Correct: a function prototype is a single line typically at the top of the program that provides "
                "the signature for a function to be defined later",
                "Incorrect: Line 12 is calling (invoking) an the function getQuotient",
                "Incorrect: there is no code on this line"
            ],
            "group": {
                "group_name": "c-group_1",
                "questions": [
                    "BD_C_0024",
                    "BD_C_0027",
                    "BD_C_0022",
                    "BD_C_0035",
                    "BD_C_0029",
                    "BD_C_0031",
                    "BD_C_0026",
                    "BD_C_0023",
                    "BD_C_0025",
                    "BD_C_0034",
                    "BD_C_0032",
                    "BD_C_0030",
                    "BD_C_0033",
                    "BD_C_0028"
                ]
            }
        },
    "knowledge/C_Programming/c-group_1/c-pointers_1/c-pointers_1.question.json":
        {
            "_id": "BD_C_0032",
            "rel-link_id": "5ee9250b761cf87371cf31f1",
            "question_name": "c-pointers_1",
            "question_path": "knowledge/C_Programming/c-group_1/c-pointers_1",
            "question_type": "knowledge",
            "topic": "C Programming",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "Which of the following variables contain a memory address?",
            "snippet": "1  | #include <stdio.h>\n2  | #include <stdlib.h>\n3  | \n4  | #define y 10\n5  | \n"
                       "6  | float getQuotient(int, int);\n7  | \n8  | int main()\n9  | {\n"
                       "10 |     int x = rand();\n11 | \n12 |     float z = getQuotient(x,y);\n13 |     \n"
                       "14 |     float* p = (float *) malloc(y * sizeof(float));\n15 |     \n16 |     p[0] = z;\n"
                       "17 |     \n18 |     printf(\"Result is %f\", *p);\n19 |     \n20 |     return 0;\n21 | }\n"
                       "22 | \n23 | float getQuotient(int numerator, int denominator)\n24 | {\n"
                       "25 |     return (float)numerator/denominator;\n26 | }\n27 | ",
            "snippet_lang": "c",
            "choices": [
                "p",
                "z",
                "y",
                "All the above"
            ],
            "answer": 0,
            "explanation": [
                "Correct: p is a pointer, and pointers can contain a memory address",
                "Incorrect: z is not a pointer and therefore cannot contain a memory address",
                "Incorrect: y is not a pointer and therefore cannot contain a memory address"
            ],
            "group": {
                "group_name": "c-group_1",
                "questions": [
                    "BD_C_0024",
                    "BD_C_0027",
                    "BD_C_0022",
                    "BD_C_0035",
                    "BD_C_0029",
                    "BD_C_0031",
                    "BD_C_0026",
                    "BD_C_0023",
                    "BD_C_0025",
                    "BD_C_0034",
                    "BD_C_0032",
                    "BD_C_0030",
                    "BD_C_0033",
                    "BD_C_0028"
                ]
            }
        },
    "knowledge/C_Programming/c-main-function/c-main-function.question.json":
        {
            "_id": "BD_C_0041",
            "rel-link_id": "5ee9250b761cf87371cf31c2",
            "question_name": "c-main-function",
            "question_path": "knowledge/C_Programming/c-main-function",
            "question_type": "knowledge",
            "topic": "C Programming",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "Which of the following is true about the 'main' function in C progamming?",
            "choices": [
                "There can be mulitple 'main' functions in a program.",
                "The 'main' function does not have a return type.",
                "All execution begins and ends in the 'main' function.",
                "'main' is an optional function to have in a program."
            ],
            "answer": 2,
            "explanation": [
                "False: A C program can only have one main function.",
                "False: The main function needs a return type even if it is void.",
                "True: The main function is where all execution begins and ends in a C program",
                "False: All C programs must have a main function."
            ]
        },
    "knowledge/C_Programming/c-bit-shift-left/c-bit-shift-left.question.json":
        {
            "_id": "BD_C_0007",
            "rel-link_id": "5ee9250b761cf87371cf31bd",
            "question_name": "c-bit-shift-left",
            "question_path": "knowledge/C_Programming/c-bit-shift-left",
            "question_type": "knowledge",
            "topic": "C Programming",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "What is the value of x after the C program snippet above executes?",
            "snippet": "1 | int x = 2; \n2 | x <<= 1;",
            "snippet_lang": "c",
            "choices": [
                "2",
                "1",
                "4",
                "0"
            ],
            "answer": 2,
            "explanation": [
                "Left shift operator shifts bits one to the left.  Given the start bits were 00000010 for a value of 2,"
                " when shifting the bits one to the left the result is 00000100 resulting in 4"
            ]
        },
    "knowledge/C_Programming/c-data-types_1/c-data-types_1.question.json":
        {
            "_id": "BD_C_0011",
            "rel-link_id": "5ee9250b761cf87371cf31de",
            "question_name": "c-data-types_1",
            "question_path": "knowledge/C_Programming/c-data-types_1",
            "question_type": "knowledge",
            "topic": "C Programming",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "Which of the following is a vaild ASCII char declaration in C?",
            "choices": [
                "char x = '\\n'",
                "char x = \"A\";",
                "char x = -3;",
                "None of the above"
            ],
            "answer": 0,
            "explanation": [
                "Correct: \\n is a special character included in a series of escape sequences. This character "
                "represents a newline",
                "Incorrect: \"A\" is in double-quotes which is a string or char array literal",
                "Incorrect: -3 is not a valid integer value for an ASCII char"
            ]
        },
    "knowledge/C_Programming/c-stack-memory/c_stack_memory.question.json":
        {
            "_id": "BD_C_0054",
            "rel-link_id": "5ee9250b761cf87371cf31bf",
            "question_name": "c_stack_memory",
            "question_path": "knowledge/C_Programming/c-stack-memory",
            "question_type": "knowledge",
            "topic": "C Programming",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "Where is the memory for x being allocated in the above C snippet?",
            "snippet": "1 | int x = 2; \n2 | ",
            "snippet_lang": "c",
            "choices": [
                "registers",
                "heap",
                "disk",
                "stack"
            ],
            "answer": 3,
            "explanation": [
                "All variables are allocated memory on the stack"
            ]
        },
    "knowledge/Data_Structures/data-structure-pitfalls/pitfalls.question.json":
        {
            "_id": "BD_DSTRUC_0001",
            "rel-link_id": "5ee9250b761cf87371cf31b6",
            "question_name": "pitfalls",
            "question_path": "knowledge/Data_Structures/data-structure-pitfalls",
            "question_type": "knowledge",
            "topic": "Data Structures",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "Which of the following is a disadvantage of using a linked-list over an array in C for the"
                        " same amount of data?",
            "choices": [
                "linked-lists use more memory",
                "linked-lists cannot be randomly accessed",
                "linked-lists nodes do not use contiguous memory",
                "all the above"
            ],
            "answer": 3,
            "explanation": [
                "All are disadvantages of using linked lists over arrays."
            ]
        },
    "knowledge/Data_Structures/data-structures-circularly-linked-lists/circularly-linked-list.question.json":
        {
            "_id": "BD_DSTRUC_0002",
            "rel-link_id": "5ee9250b761cf87371cf31b7",
            "question_name": "circularly-linked-list",
            "question_path": "knowledge/Data_Structures/data-structures-circularly-linked-lists",
            "question_type": "knowledge",
            "topic": "Data Structures",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "What does the last node of a multi-node circularly linked list point to?",
            "choices": [
                "The first node",
                "NULL",
                "Itself",
                "The previous node"
            ],
            "answer": 0,
            "explanation": [
                "Circularly linked nodes use nodes similar to regular linked lists except the last node in a "
                "circularly linked list points to the first node - no node points to NULL"
            ]
        },
    "knowledge/Data_Structures/data-structures-doubly-linked-lists/doubly-linked-list.question.json":
        {
            "_id": "BD_DSTRUC_0003",
            "rel-link_id": "5ee9250b761cf87371cf31b4",
            "question_name": "doubly-linked-list",
            "question_path": "knowledge/Data_Structures/data-structures-doubly-linked-lists",
            "question_type": "knowledge",
            "topic": "Data Structures",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "The above C struct definition is ideal for what type of Data Structure?",
            "snippet": " 1  |  struct Node {\n 2  |    int data;\n 3  |    struct Node *next;\n "
                       "4  |    struct Node *prev;\n 5  |  };\n",
            "snippet_lang": "mermaid",
            "choices": [
                "Linked List",
                "Doubly Linked List",
                "Binary Tree",
                "Graph"
            ],
            "answer": 1,
            "explanation": [
                "Incorrect: Linked list nodes will only have a single pointer to the next node.",
                "Correct: Doubly linked lists will have a pointer that points to the previous node or NULL, and to "
                "the next node or NULL",
                "Incorrect: Although Binary Tree nodes contain two pointers, these typically are labled left and "
                "right respectively",
                "Incorrect: Each node in a graph can point to more than two other graph nodes. Therefore there must"
                " be a way to track an unknown number of nodes such as with an array."
            ]
        },
    "knowledge/Data_Structures/data-structures-weighted-graphs/weighted-graphs.question.json":
        {
            "_id": "BD_DSTRUC_0004",
            "rel-link_id": "5ee9250b761cf87371cf31ba",
            "question_name": "weighted-graphs",
            "question_path": "knowledge/Data_Structures/data-structures-weighted-graphs",
            "question_type": "knowledge",
            "topic": "Data Structures",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "In a Weighted Graph, the weight is applied to each of the graph's _______________",
            "choices": [
                "nodes",
                "vertices",
                "levels",
                "edges"
            ],
            "answer": 3,
            "explanation": [
                "A weighted graph refers to a simple graph that has weighted edges. These weighted edges can be "
                "used to compute the shortest path."
            ]
        },
    "knowledge/Data_Structures/data-structure-queue/queue.question.json":
        {
            "_id": "BD_DSTRUC_0005",
            "rel-link_id": "5ee9250b761cf87371cf31b5",
            "question_name": "queue",
            "question_path": "knowledge/Data_Structures/data-structure-queue",
            "question_type": "knowledge",
            "topic": "Data Structures",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "Which of the following is a proper behavior for a queue?",
            "choices": [
                "Items can be added to the front of the queue if it's implemented with a linked-list.",
                "Items are removed from the front of the queue.",
                "A queue is a Last In, First Out data structure.",
                "Both B & C are correct."
            ],
            "answer": 1,
            "explanation": [
                "Only the first item in the queue (front) can be removed via a dequeue function for a queue.  A queue "
                "is first in first out data structure."
            ]
        },
    "knowledge/Data_Structures/data-structures-stack/stack.question.json":
        {
            "_id": "BD_DSTRUC_0006",
            "rel-link_id": "5ee9250b761cf87371cf31b8",
            "question_name": "stack",
            "question_path": "knowledge/Data_Structures/data-structures-stack",
            "question_type": "knowledge",
            "topic": "Data Structures",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "Which of the following is a proper behavior for a stack?",
            "choices": [
                "Items can be removed anywhere in the stack if it's implemented with a linked-list.",
                "Only the last item added can be removed from the stack.",
                "A stack is a last in last out data structure.",
                "Both B & C are correct."
            ],
            "answer": 1,
            "explanation": [
                "Only the last item added can be removed via a pop function for a stack.  A stack is last in first out "
                "data structure."
            ]
        },
    "knowledge/Data_Structures/data-structures-tree/tree.question.json":
        {
            "_id": "BD_DSTRUC_0007",
            "rel-link_id": "5ee9250b761cf87371cf31b9",
            "question_name": "tree",
            "question_path": "knowledge/Data_Structures/data-structures-tree",
            "question_type": "knowledge",
            "topic": "Data Structures",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "The above class is ideal for what type of data structure?",
            "snippet": "1 | class Node:\n 2 |     def __init__(self, data):\n 3 |         self.data = data\n "
                       "4 |         self.children = []\n",
            "snippet_lang": "mermaid",
            "choices": [
                "linked-list.",
                "doubly-linked list.",
                "general tree",
                "binary tree"
            ],
            "answer": 2,
            "explanation": [
                "Incorrect: A linked list has a node typically consisting of data and a next ref to the next node or to"
                " None.",
                "Incorrect: A doubly-linked list has a node typically consisting of data and a next reference the next"
                " node or to None and a prev reference to the previous node or None.",
                "Correct: A general tree is a data structure where every child has only one parent, but one parent can "
                "have many children.",
                "Incorrect: although this could be used for a binary tree, binary tree are limited parents have two "
                "children, therefore the node would consist of a left reference the left node/child or to None and a "
                "right reference to the right node/child or None."
            ]
        },
    "knowledge/Networking/networking-dhcp_1/networking-dhcp_1.question.json":
        {
            "_id": "BD_NET_0005",
            "rel-link_id": "5ee9250b761cf87371cf31b3",
            "question_name": "networking-dhcp_1",
            "question_path": "knowledge/Networking/networking-dhcp_1",
            "question_type": "knowledge",
            "topic": "Networking",
            "disabled": False,
            "provisioned": 1,
            "attempts": 2,
            "passes": 2,
            "failures": 0,
            "question": "Upon bootup, a particular network device was configured with 0.0.0.0 so it asked for and "
                        "received an IP from the ___.",
            "choices": [
                "Router",
                "DNS Server",
                "ARP Server",
                "DHCP server"
            ],
            "answer": 3,
            "explanation": [
                "Incorrect: A router is a device that forwards data packets along networks; it doesn't assign IP "
                "addresses.",
                "Incorrect: A DNS Server resolves domain names to IP addresses.",
                "Incorrect: ARP Server will map a dynamic Internet Protocol address (IP address) to a permanent "
                "physical machine address in a local area network (LAN)/].",
                "Correct: A DHCP Server is a network server that automatically provides and assigns IP addresses, "
                "default gateways and other network parameters to client devices. It relies on the standard "
                "protocol known as Dynamic Host Configuration Protocol or DHCP to respond to broadcast queries by "
                "clients."
            ]
        },
    "knowledge/Networking/networking-ipv4-addresses_1/networking-ipv4-addresses_1.question.json":
        {
            "_id": "BD_NET_0009",
            "rel-link_id": "5ee9250b761cf87371cf319f",
            "question_name": "networking-ipv4-addresses_1",
            "question_path": "knowledge/Networking/networking-ipv4-addresses_1",
            "question_type": "knowledge",
            "topic": "Networking",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "An IPV4 octet's number ranges from:",
            "choices": [
                "1 to 254",
                "0 to 196",
                "10 to 255",
                "0 to 255"
            ],
            "answer": 3,
            "explanation": [
                "Incorrect: Does not include 0 and 255.",
                "Incorrect: Does not include 197 to 255.",
                "Incorrect: Does not include 0 to 9.",
                "Correct: An IPv4 address is made up of octets, an octet being an eight-bit value. So you can only "
                "have 256, so 0 to 255"
            ]
        },
    "knowledge/Networking/networking-networks-and-ports_1/networking-networks-and-ports_1.question.json":
        {
            "_id": "BD_NET_0015",
            "rel-link_id": "5ee9250b761cf87371cf31ad",
            "question_name": "networking-networks-and-ports_1",
            "question_path": "knowledge/Networking/networking-networks-and-ports_1",
            "question_type": "knowledge",
            "topic": "Networking",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "Jimmy sent a package to Dean and noticed the source port was 51234 and the destination "
                        "port was 666 on one of the TCP packets.  The difference between these two port types is .",
            "choices": [
                "port 51234 is assigned by IANA whereas 666 is never assigned.",
                "port 51234 is a registered port whereas 666 is an ephemeral port.",
                "other than the numbers, there is no difference.",
                "port 51234 is an ephemeral port whereas 666 is a well-known port."
            ],
            "answer": 3,
            "explanation": [
                "IAW RFC 6335, ports 0 - 1023 are considered system, or well-known, ports; 1024 - 49151 are "
                "considered user, or registered ports; and 49152 - 65535 are dynamic, private, or ephemeral ports. "
                "The dynamic ports are never assigned."
            ]
        },
    "knowledge/Networking/networking-rfc/rfc.question.json":
        {
            "_id": "BD_NET_0038",
            "rel-link_id": "5ee9250b761cf87371cf31a1",
            "question_name": "rfc",
            "question_path": "knowledge/Networking/networking-rfc",
            "question_type": "knowledge",
            "topic": "Networking",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "What type of document is authored by engineers and computer scientists in the form of a "
                        "memorandum describing methods, behaviors, research, or innovations applicable to the "
                        "working of the Internet and Internet-connected systems?",
            "choices": [
                "IEEE - Instructions of Electrical and Electronics Engineers",
                "RFC - Request for Comments",
                "W3C - World Wide Web Comments",
                "NGM - Networking Guidance Memorandum"
            ],
            "answer": 1,
            "explanation": [
                "An RFC is authored by engineers and computer scientists in the form of a memorandum describing "
                "methods, behaviors, research, or innovations applicable to the working of the Internet and "
                "Internet-connected systems. It is submitted either for peer review or to convey new concepts, "
                "information, or occasionally engineering humor."
            ]
        },
    "knowledge/Networking/networking-CIDR/networking-CIDR.question.json":
        {
            "_id": "BD_NET_0040",
            "rel-link_id": "5f088587ef8faf28b78d3b2d",
            "question_name": "networking-CIDR",
            "question_path": "knowledge/Networking/networking-CIDR",
            "question_type": "knowledge",
            "topic": "Networking",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "Which of the following describes Classless Inter-Domain Routing (CIDR) notation:",
            "choices": [
                "The process where a network device assigns a public address to one or more computers inside a private "
                "network",
                "An IP addressing scheme that improves the allocation of IP addresses. It replaces the old system based"
                " on classes A, B, and C",
                "A method to dynamically assign an IP address and other network configuration parameters to each device"
                " on a network so they can communicate with other IP networks",
                "A communication protocol used for discovering the link layer address, such as a MAC address, "
                "associated with a given internet layer address"
            ],
            "answer": 1,
            "explanation": [
                "Incorrect: This describes NAT.",
                "Correct",
                "Incorrect: This describes DHCP.",
                "Incorrect: This describes ARP"
            ]
        },
    "knowledge/Networking/networking-NAT/networking-NAT.question.json":
        {
            "_id": "BD_NET_0041",
            "rel-link_id": "5f088587ef8faf28b78d3b2e",
            "question_name": "networking-NAT",
            "question_path": "knowledge/Networking/networking-NAT",
            "question_type": "knowledge",
            "topic": "Networking",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "Which of the following describes Network Address Translation (NAT):",
            "choices": [
                "The process where a network device assigns a public address to one or more computers inside a private "
                "network",
                "An IP addressing scheme that improves the allocation of IP addresses. It replaces the old system based"
                " on classes A, B, and C",
                "A method to dynamically assign an IP address and other network configuration parameters to each device"
                " on a network so they can communicate with other IP networks",
                "A communication protocol used for discovering the link layer address, such as a MAC address, "
                "associated with a given internet layer address"
            ],
            "answer": 0,
            "explanation": [
                "Correct.",
                "Incorrect: This is a description of CIDR notation.",
                "Incorrect: This describes DHCP.",
                "Incorrect: This describes ARP"
            ]
        },
    "knowledge/Networking/networking-udp/networking-udp.question.json":
        {
            "_id": "BD_NET_0032",
            "rel-link_id": "5ee9250b761cf87371cf319e",
            "question_name": "networking-udp",
            "question_path": "knowledge/Networking/networking-udp",
            "question_type": "knowledge",
            "topic": "Networking",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "Which of the following is correct about the UDP protocol?",
            "choices": [
                "Keeps track of lost packets",
                "Adds sequence numbers",
                "Checks packet arrival order",
                "Faster than TCP"
            ],
            "answer": 3,
            "explanation": [
                "UDP is faster than TCP because it doesn't add sequence numbers, doesn't try to keep track of lost "
                "packets, and doesn't check for arrival order like TCP"
            ]
        },
    "knowledge/Python/python-class-attributes/class-attributes.question.json":
        {
            "_id": "BD_PY_0100",
            "rel-link_id": "5ee9250b761cf87371cf321d",
            "question_name": "class-attributes",
            "question_path": "knowledge/Python/python-class-attributes",
            "question_type": "knowledge",
            "topic": "Python",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "Given the above Python class, self.age is an instance ___________",
            "snippet": "class Person:\n  def __init__(self, name, age):\n    self.name = name\n    self.age = age",
            "snippet_lang": "python",
            "choices": [
                "function",
                "attribute",
                "object",
                "instance"
            ],
            "answer": 1,
            "explanation": [
                "a variable declared with a prepended self. indicates a Instance attribute"
            ]
        },
    "knowledge/Python/python-factory-design/python-factory-design.question.json":
        {
            "_id": "BD_PY_0124",
            "rel-link_id": "5ee9250b761cf87371cf3213",
            "question_name": "python-factory-design",
            "question_path": "knowledge/Python/python-factory-design",
            "question_type": "knowledge",
            "topic": "Python",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "What design pattern solves the problem of creating product objects without specifying "
                        "their concrete classes?",
            "choices": [
                "Concrete",
                "Factory",
                "Repeater",
                "Product"
            ],
            "answer": 1,
            "explanation": [
                "Factory design pattern solves the problem of creating product objects without specifying their "
                "concrete classes"
            ]
        },
    "knowledge/Python/python-group_1/dict_1/dict_1.question.json":
        {
            "_id": "BD_PY_0023",
            "rel-link_id": "5ee9250b761cf87371cf322c",
            "question_name": "dict_1",
            "question_path": "knowledge/Python/python-group_1/dict_1",
            "question_type": "knowledge",
            "topic": "Python",
            "disabled": False,
            "provisioned": 1,
            "attempts": 2,
            "passes": 2,
            "failures": 0,
            "question": "What is occurring on line 16?",
            "snippet": "1  | def getAverage(GoT):\n2  |    tot = 0\n3  |    count = 0\n"
                       "4  |    for rank in GoT.values():\n5  |       tot += rank\n6  |       count += 1\n7  | \n"
                       "8  |    return int(tot/count)\n9  | \n"
                       "10 | names = ['Cersei','Arya','Jon','Denerys','Tyrion','Jon','Sam','Arya'] \n11 |    \n"
                       "12 | peeps = {'Jon':16,'Denerys':7,'Tyrion':15,'Cersei':2,'Arya':20}\n13 | \n"
                       "14 | print(peeps[names[0]])\n15 | \n16 | peeps['Tyrion'] = 18\n17 | \n"
                       "18 | avg = getAverage(peeps)\n19 | \n20 | castNames = {'Arya'}\n21 | \n"
                       "22 | for name in names:\n23 |    castNames.add(name)\n24 | \n"
                       "25 | print(names[len(names)])\n26 | ",
            "snippet_lang": "python",
            "choices": [
                "The value associated with 'Tyrion' is changed to 18",
                "An additional key/value pair of 'Tyrion' and 18 is added",
                "The value associated with 'Tyrion' is increased by 18",
                "None of the above"
            ],
            "answer": 0,
            "explanation": [
                "Since the dictionary 'peeps' already has a key of 'Tyrion', the value is changed to 18 because of "
                "the assignment operator. Duplicate keys aren't allowed in a dictionary object."
            ],
            "group": {
                "group_name": "python-group_1",
                "questions": [
                    "BD_PY_0016",
                    "BD_PY_0019",
                    "BD_PY_0017",
                    "BD_PY_0023",
                    "BD_PY_0020",
                    "BD_PY_0022",
                    "BD_PY_0021",
                    "BD_PY_0018",
                    "BD_PY_0015"
                ]
            }
        },
    "knowledge/Python/python-group_2/object/object.question.json":
        {
            "_id": "BD_PY_0106",
            "rel-link_id": "5ee9250b761cf87371cf3246",
            "question_name": "object",
            "question_path": "knowledge/Python/python-group_2/object",
            "question_type": "knowledge",
            "topic": "Python",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "What is 'peep' in line 18?",
            "snippet": "1 |   class Person:\n2 |     def __init__(self, name):\n3 |         self.name = name\n4 |\n"
                       "5 |     def print(self):\n6 |         print(self.name)\n7 |\n8 |   class Airman(Person):\n"
                       "9 |     def __init__(self, name, rank):\n10 |         super().__init__(name)\n"
                       "11 |         self.rank = rank\n12 |\n13 |     def print(self):\n"
                       "14 |         print(self.rank, self.name)  \n15 |\n"
                       "16 |   people = {Person('Joe Schmoe'), Airman('Joe', 'SSgt')}\n17 |\n"
                       "18 |   for peep in people:\n19 |     peep.print()",
            "snippet_lang": "python",
            "choices": [
                "A String",
                "An Object",
                "A Class",
                "A Set"
            ],
            "answer": 1,
            "explanation": [
                "peep represents each item in the people set defined earlier which is a set of objects"
            ],
            "group": {
                "group_name": "python-group_2",
                "questions": [
                    "BD_PY_0103",
                    "BD_PY_0104",
                    "BD_PY_0105",
                    "BD_PY_0106",
                    "BD_PY_0107",
                    "BD_PY_0108",
                    "BD_PY_0109"
                ]
            }
        },
    "knowledge/Python/python-group_1/lists_7/lists_7.question.json":
        {
            "_id": "BD_PY_0020",
            "rel-link_id": "5ee9250b761cf87371cf3227",
            "question_name": "lists_7",
            "question_path": "knowledge/Python/python-group_1/lists_7",
            "question_type": "knowledge",
            "topic": "Python",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "What type of container is names?",
            "snippet": "1  | def getAverage(GoT):\n2  |    tot = 0\n3  |    count = 0\n"
                       "4  |    for rank in GoT.values():\n5  |       tot += rank\n6  |       count += 1\n7  | \n"
                       "8  |    return int(tot/count)\n9  | \n"
                       "10 | names = ['Cersei','Arya','Jon','Denerys','Tyrion','Jon','Sam','Arya'] \n11 |    \n"
                       "12 | peeps = {'Jon':16,'Denerys':7,'Tyrion':15,'Cersei':2,'Arya':20}\n13 | \n"
                       "14 | print(peeps[names[0]])\n15 | \n16 | peeps['Tyrion'] = 18\n17 | \n"
                       "18 | avg = getAverage(peeps)\n19 | \n20 | castNames = {'Arya'}\n21 | \n"
                       "22 | for name in names:\n23 |    castNames.add(name)\n24 | \n25 | print(names[len(names)])\n"
                       "26 | ",
            "snippet_lang": "python",
            "choices": [
                "List",
                "Array",
                "String",
                "Set"
            ],
            "answer": 0,
            "explanation": [
                "Correct: A list is declared with square brackets.",
                "Incorrect: Python does not have array containers.",
                "Incorrect: A string is declared by surrounding text with either single or double quotes.",
                "Incorrect: A set is declared using curly brackets and comma separated values, no key-value pairs "
                "present."
            ],
            "group": {
                "group_name": "python-group_1",
                "questions": [
                    "BD_PY_0016",
                    "BD_PY_0019",
                    "BD_PY_0017",
                    "BD_PY_0023",
                    "BD_PY_0020",
                    "BD_PY_0022",
                    "BD_PY_0021",
                    "BD_PY_0018",
                    "BD_PY_0015"
                ]
            }
        },
    "knowledge/Python/python-group_2/attribute/attribute.question.json":
        {
            "_id": "BD_PY_0103",
            "rel-link_id": "5ee9250b761cf87371cf3249",
            "question_name": "attribute",
            "question_path": "knowledge/Python/python-group_2/attribute",
            "question_type": "knowledge",
            "topic": "Python",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "What is self.name in the Person class?",
            "snippet": "1 |   class Person:\n2 |     def __init__(self, name):\n3 |         self.name = name\n4 |\n"
                       "5 |     def print(self):\n6 |         print(self.name)\n7 |\n8 |   class Airman(Person):\n"
                       "9 |     def __init__(self, name, rank):\n10 |         super().__init__(name)\n"
                       "11 |         self.rank = rank\n12 |\n13 |     def print(self):\n"
                       "14 |         print(self.rank, self.name)  \n15 |\n"
                       "16 |   people = {Person('Joe Schmoe'), Airman('Joe', 'SSgt')}\n17 |\n"
                       "18 |   for peep in people:\n19 |     peep.print()",
            "snippet_lang": "python",
            "choices": [
                "Attribute",
                "Constructor",
                "Class",
                "Function"
            ],
            "answer": 0,
            "explanation": [
                "Correct: An attribute in a class is defined with the self. identifier",
                "Incorrect: An constructor is as an __init__ function",
                "Incorrect: A class is defined with the keyword class",
                "Incorrect: A function is defined with the keyword def"
            ],
            "group": {
                "group_name": "python-group_2",
                "questions": [
                    "BD_PY_0103",
                    "BD_PY_0104",
                    "BD_PY_0105",
                    "BD_PY_0106",
                    "BD_PY_0107",
                    "BD_PY_0108",
                    "BD_PY_0109"
                ]
            }
        },
    "knowledge/Python/python-oop_1/python-oop_1.question.json":
        {
            "_id": "BD_PY_0040",
            "rel-link_id": "5ee9250b761cf87371cf3221",
            "question_name": "python-oop_1",
            "question_path": "knowledge/Python/python-oop_1",
            "question_type": "knowledge",
            "topic": "Python",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "Which of the following is NOT a keyword used in Python exception handling?",
            "choices": [
                "finally",
                "break",
                "try",
                "except"
            ],
            "answer": 1,
            "explanation": [
                "Try, except, and finally are all exception handling keywords; break is not an exception handling "
                "keyword but does allow breaking out of loops."
            ]
        }
}
performance = {
    "performance/Assembly/character_replace/character_replace.question.json":
        {
            "_id": "BD_ASM_0037",
            "rel-link_id": "5ee9250b761cf87371cf3266",
            "question_name": "character_replace",
            "question_path": "performance/Assembly/character_replace",
            "question_type": "performance_dev",
            "topic": "Assembly",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/Assembly/character_replace/character_replace.asm"
            },
            "support_files": [
                "performance/Assembly/character_replace/ASM.vcxproj",
                "performance/Assembly/character_replace/ASM.vcxproj.filters",
                "performance/Assembly/character_replace/ASM.vcxproj.user",
                "performance/Assembly/character_replace/main.c"
            ],
            "tests": [
                {
                    "filepath": "performance/Assembly/character_replace/tests.c"
                },
                {
                    "filepath": "performance/Assembly/character_replace/test_defs.c",
                    "headerpath": "performance/Assembly/character_replace/test_defs.h"
                }
            ],
            "solution": "performance/Assembly/character_replace/Solution/character_replace.asm",
            "dependencies": {
                "libraries": [],
                "req_software": [
                    "nasm.exe",
                    "MS Visual Studio 2017"
                ]
            }
        },
    "performance/Assembly/mem_move/mem_move.question.json":
        {
            "_id": "BD_ASM_0039",
            "rel-link_id": "5ee9250b761cf87371cf3265",
            "question_name": "mem_move",
            "question_path": "performance/Assembly/mem_move",
            "question_type": "performance_dev",
            "topic": "Assembly",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/Assembly/mem_move/testfile.S"
            },
            "support_files": [
                "performance/Assembly/mem_move/ASM.vcxproj",
                "performance/Assembly/mem_move/ASM.vcxproj.filters",
                "performance/Assembly/mem_move/main.c"
            ],
            "tests": [
                {
                    "filepath": "performance/Assembly/mem_move/tests.c"
                },
                {
                    "filepath": "performance/Assembly/mem_move/test_defs.c",
                    "headerpath": "performance/Assembly/mem_move/test_defs.h"
                }
            ],
            "solution": "performance/Assembly/mem_move/Solution/testfile.S",
            "dependencies": {
                "libraries": [],
                "req_software": [
                    "nasm.exe",
                    "MS Visual Studio 2017"
                ]
            }
        },
    "performance/Assembly/power_of_two/power_of_two.question.json":
        {
            "_id": "BD_ASM_0040",
            "rel-link_id": "5ee9250b761cf87371cf3263",
            "question_name": "power_of_two",
            "question_path": "performance/Assembly/power_of_two",
            "question_type": "performance_dev",
            "topic": "Assembly",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/Assembly/power_of_two/testfile.S"
            },
            "support_files": [
                "performance/Assembly/power_of_two/ASM.vcxproj",
                "performance/Assembly/power_of_two/ASM.vcxproj.filters",
                "performance/Assembly/power_of_two/main.c"
            ],
            "tests": [
                {
                    "filepath": "performance/Assembly/power_of_two/tests.c"
                },
                {
                    "filepath": "performance/Assembly/power_of_two/test_defs.c",
                    "headerpath": "performance/Assembly/power_of_two/test_defs.h"
                }
            ],
            "solution": "performance/Assembly/power_of_two/Solution/testfile.S",
            "dependencies": {
                "libraries": [],
                "req_software": [
                    "nasm.exe",
                    "MS Visual Studio 2017"
                ]
            }
        },
    "performance/Assembly/reverse_string/reverse_string.question.json":
        {
            "_id": "BD_ASM_0041",
            "rel-link_id": "5ee9250b761cf87371cf3264",
            "question_name": "reverse_string",
            "question_path": "performance/Assembly/reverse_string",
            "question_type": "performance_dev",
            "topic": "Assembly",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/Assembly/reverse_string/testfile.S"
            },
            "support_files": [
                "performance/Assembly/reverse_string/ASM.vcxproj",
                "performance/Assembly/reverse_string/ASM.vcxproj.filters",
                "performance/Assembly/reverse_string/main.c"
            ],
            "tests": [
                {
                    "filepath": "performance/Assembly/reverse_string/tests.c"
                },
                {
                    "filepath": "performance/Assembly/reverse_string/test_defs.c",
                    "headerpath": "performance/Assembly/reverse_string/test_defs.h"
                }
            ],
            "solution": "performance/Assembly/reverse_string/Solution/testfile.S",
            "dependencies": {
                "libraries": [],
                "req_software": [
                    "nasm.exe",
                    "MS Visual Studio 2017"
                ]
            }
        },
    "performance/Assembly/add_values/add_values.question.json":
        {
            "_id": "BD_ASM_0036",
            "rel-link_id": "5ee9250b761cf87371cf3262",
            "question_name": "add_values",
            "question_path": "performance/Assembly/add_values",
            "question_type": "performance_dev",
            "topic": "Assembly",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/Assembly/add_values/testfile.S"
            },
            "support_files": [
                "performance/Assembly/add_values/ASM.vcxproj",
                "performance/Assembly/add_values/ASM.vcxproj.filters",
                "performance/Assembly/add_values/main.c"
            ],
            "tests": [
                {
                    "filepath": "performance/Assembly/add_values/tests.c"
                },
                {
                    "filepath": "performance/Assembly/add_values/test_defs.c",
                    "headerpath": "performance/Assembly/add_values/test_defs.h"
                }
            ],
            "solution": "performance/Assembly/add_values/Solution/testfile.S",
            "dependencies": {
                "libraries": [],
                "req_software": [
                    "nasm.exe",
                    "MS Visual Studio 2017"
                ]
            }
        },
    "performance/Assembly/count_characters/count_characters.question.json":
        {
            "_id": "BD_ASM_0038",
            "rel-link_id": "5ee9250b761cf87371cf3267",
            "question_name": "count_characters",
            "question_path": "performance/Assembly/count_characters",
            "question_type": "performance_dev",
            "topic": "Assembly",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/Assembly/count_characters/testfile.S"
            },
            "support_files": [
                "performance/Assembly/count_characters/ASM.vcxproj",
                "performance/Assembly/count_characters/ASM.vcxproj.filters",
                "performance/Assembly/count_characters/main.c"
            ],
            "tests": [
                {
                    "filepath": "performance/Assembly/count_characters/tests.c"
                },
                {
                    "filepath": "performance/Assembly/count_characters/test_defs.c",
                    "headerpath": "performance/Assembly/count_characters/test_defs.h"
                }
            ],
            "solution": "performance/Assembly/count_characters/Solution/testfile.S",
            "dependencies": {
                "libraries": [],
                "req_software": [
                    "nasm.exe",
                    "MS Visual Studio 2017"
                ]
            }
        },
    "performance/C_Programming/binary_search_tree/binary_search_tree.question.json":
        {
            "_id": "BD_C_0067",
            "rel-link_id": "5ee9250c761cf87371cf3281",
            "question_name": "c_binary_search_tree",
            "question_path": "performance/C_Programming/c_binary_search_tree",
            "question_type": "performance_dev",
            "topic": "C Programming",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/C_Programming/c_binary_search_tree/TestCode.c",
                "headerpath": "performance/C_Programming/c_binary_search_tree/TestCode.h"
            },
            "support_files": [],
            "tests": [
                {
                    "filepath": "performance/C_Programming/c_binary_search_tree/testcases.cpp"
                }
            ],
            "solution": "performance/C_Programming/c_binary_search_tree/Solution/TestCode.c",
            "dependencies": {
                "libraries": [
                    "gmock-1.7.0"
                ],
                "req_software": [
                    "MS Visual Studio 2017"
                ]
            }
        },
    "performance/C_Programming/circularly_linked_lists/circularly.question.json":
        {
            "_id": "BD_C_0109",
            "rel-link_id": "5ee9250c761cf87371cf3280",
            "question_name": "c_circularly_linked_lists",
            "question_path": "performance/C_Programming/c_circularly_linked_lists",
            "question_type": "performance_dev",
            "topic": "C Programming",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/C_Programming_Converted/c_circularly_linked_list/TestCode.c",
                "headerpath": "performance/C_Programming_Converted/c_circularly_linked_list/TestCode.h"
            },
            "support_files": [],
            "tests": [
                {
                    "filepath": "performance/C_Programming_Converted/c_circularly_linked_list/testcases.cpp"
                }
            ],
            "solution": "performance/C_Programming_Converted/c_circularly_linked_list/Solution/TestCode.c",
            "dependencies": {
                "libraries": [
                    "gmock-1.7.0"
                ],
                "req_software": [
                    "MS Visual Studio 2017"
                ]
            }
        },
    "performance/C_Programming/grades/grades.question.json":
        {
            "_id": "BD_C_0082",
            "rel-link_id": "5ee9250b761cf87371cf3272",
            "question_name": "grades",
            "question_path": "performance/C_Programming/grades",
            "question_type": "performance_dev",
            "topic": "C Programming",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/C_Programming/grades/TestCode.c",
                "headerpath": "performance/C_Programming/grades/TestCode.h"
            },
            "support_files": [],
            "tests": [
                {
                    "filepath": "performance/C_Programming/grades/testcases.cpp"
                }
            ],
            "solution": "performance/C_Programming/grades/Solution/TestCode.c",
            "dependencies": {
                "libraries": [
                    "gmock-1.7.0"
                ],
                "req_software": [
                    "MS Visual Studio 2017"
                ]
            }
        },
    "performance/C_Programming/string_copying/string_copying.question.json":
        {
            "_id": "BD_C_0100",
            "rel-link_id": "5ee9250b761cf87371cf3276",
            "question_name": "string_copying",
            "question_path": "performance/C_Programming/string_copying",
            "question_type": "performance_dev",
            "topic": "C Programming",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/C_Programming/string_copying/TestCode.c",
                "headerpath": "performance/C_Programming/string_copying/TestCode.h"
            },
            "support_files": [],
            "tests": [
                {
                    "filepath": "performance/C_Programming/string_copying/testcases.cpp"
                }
            ],
            "solution": "performance/C_Programming/string_copying/Solution/TestCode.c",
            "dependencies": {
                "libraries": [
                    "gmock-1.7.0"
                ],
                "req_software": [
                    "MS Visual Studio 2017"
                ]
            }
        },
    "performance/C_Programming/bubble_sort/bubble_sort.question.json":
        {
            "_id": "BD_C_0070",
            "rel-link_id": "5ee9250b761cf87371cf326d",
            "question_name": "bubble_sort",
            "question_path": "performance/C_Programming/bubble_sort",
            "question_type": "performance_dev",
            "topic": "C Programming",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/C_Programming/bubble_sort/TestCode.c",
                "headerpath": "performance/C_Programming/bubble_sort/TestCode.h"
            },
            "support_files": [],
            "tests": [
                {
                    "filepath": "performance/C_Programming/bubble_sort/testcases.cpp"
                }
            ],
            "solution": "performance/C_Programming/bubble_sort/Solution/TestCode.c",
            "dependencies": {
                "libraries": [
                    "gmock-1.7.0"
                ],
                "req_software": [
                    "MS Visual Studio 2017"
                ]
            }
        },
    "performance/C_Programming/files/files.question.json":
        {
            "_id": "BD_C_0078",
            "rel-link_id": "5ee9250c761cf87371cf3299",
            "question_name": "files",
            "question_path": "performance/C_Programming/files",
            "question_type": "performance_dev",
            "topic": "C Programming",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/C_Programming/files/TestCode.c",
                "headerpath": "performance/C_Programming/files/TestCode.h"
            },
            "support_files": [
                "performance/C_Programming/files/california.txt",
                "performance/C_Programming/files/newyork.txt",
                "performance/C_Programming/files/texas.txt",
                "performance/C_Programming/files/florida.txt"
            ],
            "tests": [
                {
                    "filepath": "performance/C_Programming/files/testcases.cpp"
                }
            ],
            "solution": "performance/C_Programming/files/Solution/TestCode.c",
            "dependencies": {
                "libraries": [
                    "gmock-1.7.0"
                ],
                "req_software": [
                    "MS Visual Studio 2017"
                ]
            }
        },
    "performance/C_Programming/reverse_bits/reverse_bits.question.json":
        {
            "_id": "BD_C_0096",
            "rel-link_id": "5ee9250c761cf87371cf328f",
            "question_name": "reverse_bits",
            "question_path": "performance/C_Programming/reverse_bits",
            "question_type": "performance_dev",
            "topic": "C Programming",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/C_Programming/reverse_bits/TestCode.c",
                "headerpath": "performance/C_Programming/reverse_bits/TestCode.h"
            },
            "support_files": [],
            "tests": [
                {
                    "filepath": "performance/C_Programming/reverse_bits/testcases.cpp"
                }
            ],
            "solution": "performance/C_Programming/reverse_bits/Solution/TestCode.c",
            "dependencies": {
                "libraries": [
                    "gmock-1.7.0"
                ],
                "req_software": [
                    "MS Visual Studio 2017"
                ]
            }
        },
    "performance/Networking/Crypto_Sockets/Crypto_Sockets.question.json":
        {
            "_id": "BD_NET_0037",
            "rel-link_id": "5ee9250b761cf87371cf326a",
            "question_name": "Crypto_Sockets",
            "question_path": "performance/Networking/Crypto_Sockets",
            "question_type": "performance_dev",
            "topic": "Networking",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/Networking/Crypto_Sockets/client.py"
            },
            "support_files": [
                "performance/Networking/Crypto_Sockets/server.py",
                "performance/Networking/Crypto_Sockets/NetworkComMap.txt"
            ],
            "tests": [],
            "solution": "performance/Networking/Crypto_Sockets/Solution/Client.py",
            "dependencies": {
                "libraries": [],
                "req_software": [
                    "Python 3.7"
                ]
            }
        },
    "performance/Networking/KeyServ_REST/KeyServ_REST.question.json":
        {
            "_id": "BD_NET_0034",
            "rel-link_id": "5ee9250b761cf87371cf3269",
            "question_name": "KeyServ_REST",
            "question_path": "performance/Networking/KeyServ_REST",
            "question_type": "performance_dev",
            "topic": "Networking",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/Networking/KeyServ_REST/client.py"
            },
            "support_files": [
                "performance/Networking/KeyServ_REST/helper_stuff.py",
                "performance/Networking/KeyServ_REST/server.py",
                "performance/Networking/KeyServ_REST/websrv_config.json"
            ],
            "tests": [
                {
                    "filepath": "performance/Networking/KeyServ_REST/runtest.py"
                }
            ],
            "solution": "performance/Networking/KeyServ_REST/Solution/client.py",
            "dependencies": {
                "libraries": [
                    "flask",
                    "django"
                ],
                "req_software": [
                    "Python 3.7"
                ]
            }
        },
    "performance/Networking/Socket_Key_Match/Socket_Key_Match.question.json":
        {
            "_id": "BD_NET_0035",
            "rel-link_id": "5ee9250b761cf87371cf326b",
            "question_name": "Socket_Key_Match",
            "question_path": "performance/Networking/Socket_Key_Match",
            "question_type": "performance_dev",
            "topic": "Networking",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/Networking/Socket_Key_Match/Client.py"
            },
            "support_files": [
                "performance/Networking/Socket_Key_Match/server.py"
            ],
            "tests": [],
            "solution": "performance/Networking/Socket_Key_Match/Solution/Client.py",
            "dependencies": {
                "libraries": [],
                "req_software": [
                    "Python 3.7"
                ]
            }
        },
    "performance/Networking/TCP_Modes/TCP_Modes.question.json":
        {
            "_id": "BD_NET_0036",
            "rel-link_id": "5ee9250b761cf87371cf3268",
            "question_name": "TCP_Modes",
            "question_path": "performance/Networking/TCP_Modes",
            "question_type": "performance_dev",
            "topic": "Networking",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/Networking/TCP_Modes/client.py"
            },
            "support_files": [
                "performance/Networking/TCP_Modes/server.py"
            ],
            "tests": [],
            "solution": "performance/Networking/TCP_Modes/Solution/client.py",
            "dependencies": {
                "libraries": [],
                "req_software": [
                    "Python 3.7"
                ]
            }
        },
    "performance/Networking/Send_Data/Send_Data.question.json":
        {
            "_id": "BD_NET_0042",
            "rel-link_id": "5f08c70d645a2e409006d908",
            "question_name": "Send_Data",
            "question_path": "performance/Networking/Send_Data",
            "question_type": "performance_dev",
            "topic": "Networking",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/Networking/send_data/client.py"
            },
            "support_files": [
                "performance/Networking/send_data/comm_helper.py",
                "performance/Networking/send_data/server.py",
                "performance/Networking/send_data/student_helper.py"
            ],
            "tests": [],
            "solution": "performance/Networking/send_data/Solution/client.py",
            "dependencies": {
                "libraries": [],
                "req_software": [
                    "Python 3.7"
                ]
            }
        },
    "performance/Python/binary_to_decimal/binary_to_decimal.question.json":
        {
            "_id": "BD_PY_0058",
            "rel-link_id": "5ee9250c761cf87371cf32c5",
            "question_name": "binary_to_decimal",
            "question_path": "performance/Python/binary_to_decimal",
            "question_type": "performance_dev",
            "topic": "Python",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/Python/binary_to_decimal/testfile.py"
            },
            "support_files": [],
            "tests": [
                {
                    "filepath": "performance/Python/binary_to_decimal/runtests.py"
                }
            ],
            "solution": "performance/Python/binary_to_decimal/Solution/testfile.py",
            "dependencies": {
                "libraries": [],
                "req_software": [
                    "Python 3.7"
                ]
            }
        },
    "performance/Python/change_for_snacks/change_for_snacks.question.json":
        {
            "_id": "BD_PY_0061",
            "rel-link_id": "5ee9250c761cf87371cf329f",
            "question_name": "change_for_snacks",
            "question_path": "performance/Python/change_for_snacks",
            "question_type": "performance_dev",
            "topic": "Python",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/Python/change_for_snacks/testfile.py"
            },
            "support_files": [],
            "tests": [
                {
                    "filepath": "performance/Python/change_for_snacks/runtests.py"
                }
            ],
            "solution": "performance/Python/change_for_snacks/Solution/testfile.py",
            "dependencies": {
                "libraries": [],
                "req_software": [
                    "Python 3.7"
                ]
            }
        },
    "performance/Python/exponential_euler/exponential_euler.question.json":
        {
            "_id": "BD_PY_0075",
            "rel-link_id": "5ee9250c761cf87371cf32c1",
            "question_name": "exponential_euler",
            "question_path": "performance/Python/exponential_euler",
            "question_type": "performance_dev",
            "topic": "Python",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/Python/exponential_euler/testfile.py"
            },
            "support_files": [],
            "tests": [
                {
                    "filepath": "performance/Python/exponential_euler/runtests.py"
                }
            ],
            "solution": "performance/Python/exponential_euler/Solution/testfile.py",
            "dependencies": {
                "libraries": [],
                "req_software": [
                    "Python 3.7"
                ]
            }
        },
    "performance/Python/reverse_string_and_characters/reverse_string_and_characters.question.json":
        {
            "_id": "BD_PY_0093",
            "rel-link_id": "5ee9250c761cf87371cf32a8",
            "question_name": "reverse_string_and_characters",
            "question_path": "performance/Python/reverse_string_and_characters",
            "question_type": "performance_dev",
            "topic": "Python",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/Python/reverse_string_and_characters/testfile.py"
            },
            "support_files": [
                "performance/Python/reverse_string_and_characters/helper_stuff.py"
            ],
            "tests": [
                {
                    "filepath": "performance/Python/reverse_string_and_characters/runtests.py"
                },
                {
                    "filepath": "performance/Python/reverse_string_and_characters/test_results.txt"
                }
            ],
            "solution": "performance/Python/reverse_string_and_characters/Solution/testfile.py",
            "dependencies": {
                "libraries": [],
                "req_software": [
                    "Python 3.7"
                ]
            }
        },
    "performance/Python/IP_port_match/IP_port_match.question.json":
        {
            "_id": "BD_PY_0079",
            "rel-link_id": "5ee9250c761cf87371cf32c7",
            "question_name": "IP_port_match",
            "question_path": "performance/Python/IP_port_match",
            "question_type": "performance_dev",
            "topic": "Python",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/Python/IP_port_match/testfile.py"
            },
            "support_files": [],
            "tests": [
                {
                    "filepath": "performance/Python/IP_port_match/runtests.py"
                }
            ],
            "solution": "performance/Python/IP_port_match/Solution/testfile.py",
            "dependencies": {
                "libraries": [],
                "req_software": [
                    "Python 3.7"
                ]
            }
        },
    "performance/Python/perfect_num/perfect_num.question.json":
        {
            "_id": "BD_PY_0086",
            "rel-link_id": "5ee9250c761cf87371cf329e",
            "question_name": "perfect_num",
            "question_path": "performance/Python/perfect_num",
            "question_type": "performance_dev",
            "topic": "Python",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/Python/perfect_num/testfile.py"
            },
            "support_files": [],
            "tests": [
                {
                    "filepath": "performance/Python/perfect_num/runtests.py"
                }
            ],
            "solution": "performance/Python/perfect_num/Solution/testfile.py",
            "dependencies": {
                "libraries": [],
                "req_software": [
                    "Python 3.7"
                ]
            }
        },
    "performance/Python/python_queue/python_queue.question.json":
        {
            "_id": "BD_PY_0089",
            "rel-link_id": "5ee9250c761cf87371cf32cb",
            "question_name": "python_queue",
            "question_path": "performance/Python/python_queue",
            "question_type": "performance_dev",
            "topic": "Python",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/Python/python_queue/testfile.py"
            },
            "support_files": [],
            "tests": [
                {
                    "filepath": "performance/Python/python_queue/runtests.py"
                }
            ],
            "solution": "performance/Python/python_queue/Solution/testfile.py",
            "dependencies": {
                "libraries": [],
                "req_software": [
                    "Python 3.7"
                ]
            }
        },
    "performance/Reverse_Engineering/NOSCHEMABasic_MagicNum/NOSCHEMABasic_MagicNum.question.json":
        {
            "_id": "BD_RE_0001",
            "rel-link_id": "5ee9250c761cf87371cf32cd",
            "question_name": "NOSCHEMABasic_MagicNum",
            "question_path": "performance/Reverse_Engineering/NOSCHEMABasic_MagicNum",
            "question_type": "performance_dev",
            "topic": "Reverse Engineering",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/Reverse_Engineering/NOSCHEMABasic_MagicNum/Instructions.txt"
            },
            "support_files": [
                "performance/Reverse_Engineering/NOSCHEMABasic_MagicNum/BD_RE_2_magicnum.exe"
            ],
            "tests": [],
            "solution": "None Provided",
            "dependencies": {
                "libraries": [],
                "req_software": [
                    "IDA Pro",
                    "WinDBG"
                ]
            }
        },
    "performance/Reverse_Engineering/NOSCHEMABasic_MysteryArg/NOSCHEMABasic_MysteryArg.question.json":
        {
            "_id": "BD_RE_0002",
            "rel-link_id": "5ee9250c761cf87371cf32cc",
            "question_name": "NOSCHEMABasic_MysteryArg",
            "question_path": "performance/Reverse_Engineering/NOSCHEMABasic_MysteryArg",
            "question_type": "performance_dev",
            "topic": "Reverse Engineering",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/Reverse_Engineering/NOSCHEMABasic_MysteryArg/Instructions.txt"
            },
            "support_files": [
                "performance/Reverse_Engineering/NOSCHEMABasic_MysteryArg/BD_RE_1_mysteryargs.exe"
            ],
            "tests": [],
            "solution": "None Provided",
            "dependencies": {
                "libraries": [],
                "req_software": [
                    "IDA Pro",
                    "WinDBG"
                ]
            }
        }
}
