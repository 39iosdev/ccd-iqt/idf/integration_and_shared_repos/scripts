import json
import os
import shutil


def create_files(q_dict: dict, test_folder: str):
    if not os.path.exists(test_folder):
        os.makedirs(test_folder)
    for path, q_data in q_dict.items():
        if not os.path.exists(os.path.join(test_folder, os.path.dirname(path))):
            os.makedirs(os.path.join(test_folder, os.path.dirname(path)))
        with open(os.path.join(test_folder, path), "w") as q_fp:
            json.dump(q_data, q_fp, indent=4)


def get_all_files(root_dir: str) -> list:
    result = []
    for root, folders, files in os.walk(root_dir):
        for file in files:
            # Purposely leave out the top ost directory for later file comparison
            temp = root.replace("\\", "/").split("/")[1:]  # replace() makes it compatible across Windows & Linux
            result.append(os.path.join("/".join(temp), file))
    return result


def get_json_files(root_repo_url: str, compressed_file_repo_url: str, target_dir: str, test_dir: str,
                   comp_file_ext: str = ".tar.gz") -> str:
    repo_name = os.path.basename(root_repo_url).rstrip(".git")
    if not os.path.exists(os.path.join(test_dir, repo_name)):
        os.makedirs(os.path.join(test_dir, repo_name))
    if os.path.dirname(target_dir) != "" and not os.path.exists(
            os.path.join(test_dir, repo_name, os.path.dirname(target_dir))):
        # Make any parents necessary for the move
        os.makedirs(os.path.join(test_dir, repo_name, os.path.dirname(target_dir)))
    os.system(f"curl -o {test_dir}/{repo_name}/{os.path.basename(target_dir)}{comp_file_ext} "
              f"{compressed_file_repo_url}")
    os.system(f"tar -xf {test_dir}/{repo_name}/{os.path.basename(target_dir)}{comp_file_ext} "
              f"-C {test_dir}/{repo_name}")
    folders = next(os.walk(os.path.join(test_dir, repo_name)))[1]
    # Move folders out of the compressed folder name created during compression
    # and then remove the compressed folder
    for folder in folders:
        if repo_name in folder:
            # Only move files if the folder name is the one used to contain all compressed files
            os.rename(f"{test_dir}/{repo_name}/{folder}/{target_dir}/", f"{test_dir}/{repo_name}/{target_dir}/")
            shutil.rmtree(f"{test_dir}/{repo_name}/{folder}/")  # Remove folder created to contain compressed files
    os.remove(f"{test_dir}/{repo_name}/{os.path.basename(target_dir)}{comp_file_ext}")  # Remove the compressed file
    return os.path.join(test_dir, repo_name)


def clean_up(folder: str):
    if os.path.exists(folder):
        shutil.rmtree(folder)


def remove_questions_by_keyword(root_dir: str, file_keyword: str) -> list:
    current_files = get_all_files(root_dir)
    result = []
    for path in current_files:
        if file_keyword.lower() in path.lower():
            os.remove(os.path.join(root_dir, path))
            result.append(os.path.join(root_dir, path))
    return result


def remove_folder(folder: str):
    shutil.rmtree(folder)
