import os
import json
from colorama import Fore
from .default_strings import id_key, knowledge_key, performance_key
from .file_list_generator import dir_walk

wkrl_id_map = {
    "basic-dev": "CCD",
    "scrum-master": "SM",
    "senior-dev-windows": "SCCD-W",
    "senior-dev-linux": "SCCD-L",
    "see": "SEE",
    "embedded": "EMB",
    "network": "NET",
    "mobile": "MOB",
    "basic-po": "PO",
    "senior-po": "SPO",
    "test-automation-engineer": "TAE",
    "everyone": "EVR",
    "ccd": "CCD",
    "sccd-l": "SCCD-L",
    "sccd-w": "SCCD-W",
    "mccd": "MCCD",
    "po": "PO",
    "spo": "SPO",
    "sm": "SM",
    "tae": "TAE",
    "vr": "VR",
    "jsre": "JSRE"
}

topic_id_map = {
    "algorithms": "ALG",
    "assembly": "ASM",
    "c programming": "C",
    "data structures": "DSTRUC",
    "git": "GIT",
    "programming fundamentals": "PF",
    "networking": "NET",
    "python": "PY",
    "reverse engineering": "RE",
    "product owner": "BPO",
    "see": "SEE",
    "linux kernel development": "KD",
    "windows drivers": "DRV",
    "windows vuln research/exploitation": "REVR",
    "windows user mode": "UM",
    "software engineering fundamentals": "SEF",
    "agile": "AGL",
    "scrum": "SCR"
}


def get_next_id_nums(file_filter: str):
    '''
    Purpose: This builds a map of all question IDs in use and calculates the next available number for each.
    :param file_filter: The file extension to use when collecting applicable questions.
    :return: next_id_nums - A dictionary representing the map of the next available IDs.
    '''
    next_id_nums = {}
    questions = dir_walk(".", ["."], keyword=file_filter)

    try:
        for path in questions:
            question = json.load(open(path, "r"))
            if "question_type" not in question:
                # This is an older file that uses a one-way hash for the ID or it's not a question file so skip
                continue
            if id_key in question and question[id_key] is not None and "" != question[id_key]:
                _id = question[id_key]
                designator = "_".join(_id.split("_")[:-1])
                id_num = int(_id.split("_")[-1])

                if designator not in next_id_nums:
                    next_id_nums[designator] = id_num
                elif id_num > next_id_nums[designator]:
                    next_id_nums[designator] = id_num

    except json.JSONDecodeError as err:
        raise ValueError("{0} {1}".format(Fore.RED + os.path.basename(__file__) + Fore.RESET, err))

    except (FileNotFoundError, FileExistsError, OSError, IOError) as err:
        # In case the file cannot be opened, display a reason and stop processing
        raise IOError("{0} ({1}){2}".format(Fore.RED + os.path.basename(__file__) + Fore.RESET, err.errno, err))

    except (Exception, BaseException) as err:
        raise Exception("{0} {1}".format(Fore.RED + os.path.basename(__file__) + Fore.RESET, err))

    else:
        for designator in next_id_nums.keys():
            next_id_nums[designator] += 1

    return next_id_nums


def get_qid_code(role_name: str, topic: str, next_id_nums: dict) -> (str, dict):
    '''
    Purpose: This assigns the next available question ID according to the work-role and topic.
    :param role_name: The name of the question's work-role.
    :param topic: The name of the question's topic.
    :param next_id_nums: A dictionary representing a map of the next available question IDs
    :return: new_id - A string representing the new question ID
             next_id_nums - A dictionary representing the updated next available question IDs
    '''
    # assign new eval UID
    role = wkrl_id_map[role_name.lower()]  # Get the role code
    topic = topic_id_map[topic.lower()]  # Get the topic code

    if "dev" in role_name.lower() or "ccd" in role_name.lower():
        new_id = "{role}_{topic}".format(role=role, topic=topic)  # Build new question ID
    else:
        new_id = "{role}".format(role=role, topic=topic)

    if new_id in next_id_nums:
        id = next_id_nums[new_id]  # Get the question ID
        next_id_nums[new_id] += 1  # Update the next available number
    else:
        id = 1  # Establish a new question ID
        next_id_nums[new_id] = id + 1  # Update the next available number

    new_id = "{0}_{1:0>4}".format(new_id, id)

    return new_id, next_id_nums
