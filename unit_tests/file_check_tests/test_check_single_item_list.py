import unittest
from bs4 import BeautifulSoup
from validate_slides import check_single_item_list


class TestCheckSingleItemList(unittest.TestCase):
    def test_check_single_item_list(self):
        expected_pass_dict = {'0': 'PASS All Lists Valid: slides/test_pass_list.html'}
        expected_fail_dict = {
            '1':
                ['ERROR List has a single item: please consider modifying list at line 50 in slides/test_fail_list.html',
                 'ERROR List has a single item: please consider modifying list at line 60 in slides/test_fail_list.html'
                ]
        }

        file_name = "slides/test_pass_list.html"
        f = open(file_name, "r")
        content = f.read()
        parser = BeautifulSoup(content, 'html.parser')
        f.close()
        self.assertEqual(expected_pass_dict, check_single_item_list(parser, file_name))

        file_name = "slides/test_fail_list.html"
        f = open(file_name, "r")
        content = f.read()
        parser = BeautifulSoup(content, 'html.parser')
        f.close()
        self.assertEqual(expected_fail_dict, check_single_item_list(parser, file_name))


if __name__ == "__main__":
    unittest.main()
