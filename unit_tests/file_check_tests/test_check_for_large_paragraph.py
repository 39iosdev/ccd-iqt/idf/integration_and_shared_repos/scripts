import unittest
from bs4 import BeautifulSoup
from validate_slides import check_for_large_paragraph


class TestCheckForLargeParagraph(unittest.TestCase):
    def test_check_for_large_paragraph(self):
        expected_pass_dict = {'0': 'PASS Valid Paragraph: slides/test_pass_paragraph.html'}
        expected_fail_dict = {
            '2':
                [
                    'WARNING Paragraph length too long: please consider reducing the length of paragraph at line 71 in '
                    'slides/test_fail_paragraph.html',
                    'WARNING Paragraph length too long: please consider reducing the length of paragraph at line 407 in'
                    ' slides/test_fail_paragraph.html',
                    'WARNING Paragraph length too long: please consider reducing the length of paragraph at line 523 in'
                    ' slides/test_fail_paragraph.html'
                ]
        }

        file_name = "slides/test_pass_paragraph.html"
        f = open(file_name, "r")
        content = f.read()
        parser = BeautifulSoup(content, 'html.parser')
        f.close()
        self.assertEqual(expected_pass_dict, check_for_large_paragraph(parser, file_name))

        file_name = "slides/test_fail_paragraph.html"
        f = open(file_name, "r")
        content = f.read()
        parser = BeautifulSoup(content, 'html.parser')
        f.close()
        self.assertEqual(expected_fail_dict, check_for_large_paragraph(parser, file_name))


if __name__ == "__main__":
    unittest.main()
