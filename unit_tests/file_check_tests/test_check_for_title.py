import unittest
from bs4 import BeautifulSoup
from validate_slides import check_for_title


class TestCheckForTitle(unittest.TestCase):
    def test_check_for_title(self):
        expected_pass_dict = {'0': 'PASS Title Valid: slides/test_pass_title.html'}
        expected_fail_dict = {'1': [
            'ERROR Invalid title: No title in section at line 32 in slides/test_fail_title.html',
            'ERROR Invalid title: No title in section at line 34 in slides/test_fail_title.html'
            ]
        }

        file_name = "slides/test_pass_title.html"
        f = open(file_name, "r")
        content = f.read()
        parser = BeautifulSoup(content, 'html.parser')
        f.close()

        self.assertEqual(expected_pass_dict, check_for_title(parser, file_name))

        file_name = "slides/test_fail_title.html"
        f = open(file_name, "r")
        content = f.read()
        parser = BeautifulSoup(content, 'html.parser')
        f.close()

        self.assertEqual(expected_fail_dict, check_for_title(parser, file_name))


if __name__ == "__main__":
    unittest.main()

