import unittest
from bs4 import BeautifulSoup
from validate_slides import check_big_section


class TestCheckBigSection(unittest.TestCase):
    def test_check_big_section(self):
        expected_pass_dict = {"0": "PASS All Section length limit: slides/test_pass_sections.html"}
        expected_fail_dict = {
            '2':
                [
                    'WARNING Section length too big: please consider reducing the size at line 37 in slides/test_fail_'
                    'sections.html'
                ]
        }

        file_name = "slides/test_pass_sections.html"
        f = open(file_name, "r")
        content = f.read()
        parser = BeautifulSoup(content, 'html.parser')
        f.close()
        self.assertEqual(expected_pass_dict, check_big_section(parser, file_name))

        file_name = "slides/test_fail_sections.html"
        f = open(file_name, "r")
        content = f.read()
        parser = BeautifulSoup(content, 'html.parser')
        f.close()
        self.assertEqual(expected_fail_dict, check_big_section(parser, file_name))


if __name__ == "__main__":
    unittest.main()
