import sys
import os
import json
import shutil
import unittest
sys.path.append("scripts")
from lib import util

alg_lsn = {
    "uid": "BD_PY_ALGORITHMS",
    "TRN_EVL_ID": "IDF",
    "NETWORK": "COMMERCIAL",
    "KSATs": [
        [
            "K0753",
            "B"
        ],
        [
            "K0133",
            "B"
        ],
        [
            "K0134",
            "B"
        ],
        [
            "K0135",
            "B"
        ],
        [
            "K0136",
            "B"
        ],
        [
            "K0137",
            "B"
        ],
        [
            "K0132",
            "B"
        ],
        [
            "K0024",
            "B"
        ],
        [
            "A0087",
            "2"
        ],
        [
            "A0093",
            "2"
        ]
    ],
    "subject": "algorithms",
    "lesson": "algorithms",
    "lecture_time": 60,
    "perf_demo_time": 120,
    "references": [],
    "lesson_objectives": [
        {
            "description": "Employ Algorithms.",
            "proficiency_level": "C",
            "KSATs": [],
            "MSBs": [
                {
                    "description": "Describe the purpose of algorithms.",
                    "proficiency_level": "B",
                    "KSATs": [
                        "K0132"
                    ]
                },
                {
                    "description": "Describe Asymptotic notaiton (Big-O).",
                    "proficiency_level": "B",
                    "KSATs": [
                        "K0024"
                    ]
                },
                {
                    "description": "Differentiate between sort algorithms.",
                    "proficiency_level": "B",
                    "KSATs": [
                        "K0133",
                        "K0134",
                        "K0135",
                        "K0136",
                        "K0137"
                    ]
                },
                {
                    "description": "Given a dataset, select the most efficient sorting algorithm.",
                    "proficiency_level": "C",
                    "KSATs": [
                        "K0133",
                        "K0134",
                        "K0135",
                        "K0136",
                        "K0137"
                    ]
                }
            ]
        }
    ],
    "performance_objectives": [
        {
            "description": "Implement the most efficient sorting algorithm for the dataset",
            "KSATs": [
                "A0093"
            ]
        },
        {
            "description": "Implement a sorting routine.",
            "KSATs": [
                "A0087"
            ]
        }
    ],
    "workroles": [
        "basic-dev"
    ]
}


class DownloadFileTests(unittest.TestCase):
    def setUp(self) -> None:
        self.file_url = "https://gitlab.com/90cos/mttl/-/raw/master/Roadmap.json"
        self.targz_url = "https://gitlab.com/90cos/mttl/-/archive/master/mttl-master.tar.gz?path=requirements"
        self.targz_dnload_msg = "Expected tar.gz File Download missing"
        self.file_dnload_msg = "Expected File Download missing"

    def tearDown(self) -> None:
        if os.path.exists("Roadmap.json"):
            os.remove("Roadmap.json")
        if os.path.exists("mttl-master.tar.gz"):
            os.remove("mttl-master.tar.gz")

    def test_file_dnload(self):
        util.download_file(self.file_url, "Roadmap.json")
        downloaded = False
        if os.path.exists("Roadmap.json"):
            downloaded = True
        self.assertEqual(True, downloaded, self.file_dnload_msg)

    def test_targz_dnload(self):
        util.download_file(self.targz_url, "mttl-master.tar.gz")
        downloaded = os.path.exists("mttl-master.tar.gz")
        self.assertTrue(downloaded, self.targz_dnload_msg)


class ExtractTarGzTests(unittest.TestCase):
    targz_url = "https://gitlab.com/90cos/mttl/-/archive/master/mttl-master.tar.gz?path=requirements"
    targz_file = "mttl-master.tar.gz"
    extracted_dir = "mttl-master-requirements"
    test_dir = "Test_files"
    @classmethod
    def setUpClass(cls) -> None:
        os.mkdir(cls.test_dir)
        util.download_file(cls.targz_url, os.path.join(cls.test_dir, cls.targz_file))

    @classmethod
    def tearDownClass(cls) -> None:
        if os.path.exists(cls.test_dir):
            shutil.rmtree(cls.test_dir)

    def setUp(self) -> None:
        self.msg = "Expected extracted tar.gz files missing"
        self.test_ext_dir = "Test_Extracted"

    def tearDown(self) -> None:
        if os.path.exists(os.path.join(self.test_dir, self.extracted_dir)):
            shutil.rmtree(os.path.join(self.test_dir, self.extracted_dir))
        if os.path.exists(self.test_ext_dir):
            shutil.rmtree(self.test_ext_dir)
        if os.path.exists(self.extracted_dir):
            shutil.rmtree(self.extracted_dir)

    def test_extract_targz(self):
        util.extract_tar_gz(os.path.join(self.test_dir, self.targz_file), self.test_dir)
        self.assertTrue(os.path.exists(os.path.join(self.test_dir, self.extracted_dir)), self.msg)

    def test_extract_targz_null_chdir(self):
        util.extract_tar_gz(os.path.join(self.test_dir, self.targz_file), "")
        self.assertTrue(self.extracted_dir, self.msg)

    def test_extract_targz_diff_chdir(self):
        os.mkdir(self.test_ext_dir)
        util.extract_tar_gz(os.path.join(self.test_dir, self.targz_file), self.test_ext_dir)
        self.assertTrue(os.path.exists(os.path.join(self.test_ext_dir, self.extracted_dir)), self.msg)

    def test_extract_null_path(self):
        self.assertRaises(ValueError, util.extract_tar_gz, "", self.test_dir)

    def test_extract_zip(self):
        self.assertRaises(ValueError, util.extract_tar_gz, os.path.join(self.test_dir, "mttl-mast.zip"), self.test_dir)

    def test_extract_no_ext(self):
        self.assertRaises(ValueError, util.extract_tar_gz, os.path.join(self.test_dir, "mttl-master"), self.test_dir)


class GetDirsFromTargzUrlTests(unittest.TestCase):
    def setUp(self) -> None:
        self.folder_msg = "Mismatch in returned compressed archive folder"
        self.path_msg = "Mismatch in returned repository path"

    def test_get_dirs_from_targz_url1(self):
        targz_url = "https://gitlab.com/90cos/mttl/-/archive/master/mttl-master.tar.gz?path=requirements"
        exp_extract_dir = "mttl-master-requirements"
        exp_repo_path = "requirements"
        extract_dir, repo_path = util.get_dirs_from_targz_url(targz_url)
        self.assertEqual(exp_extract_dir, extract_dir, self.folder_msg)
        self.assertEqual(exp_repo_path, repo_path, self.path_msg)

    def test_get_dirs_from_targz_url2(self):
        targz_url = "https://gitlab.com/90cos/public/training/lessons/python/-/archive/master/python-master.tar.gz?path=mdbook/src"
        exp_extract_dir = "python-master-mdbook-src"
        exp_repo_path = "mdbook/src"
        extract_dir, repo_path = util.get_dirs_from_targz_url(targz_url)
        self.assertEqual(exp_extract_dir, extract_dir, self.folder_msg)
        self.assertEqual(exp_repo_path, repo_path, self.path_msg)

    def test_get_dirs_from_targz_no_questmark(self):
        no_param_url = "https://gitlab.com/90cos/mttl/-/archive/master/mttl-master.tar.gz"
        self.assertRaises(ValueError, util.get_dirs_from_targz_url, no_param_url)

    def test_get_dirs_from_targz_no_params(self):
        no_param_url = "https://gitlab.com/90cos/mttl/-/archive/master/mttl-master.tar.gz?"
        self.assertRaises(ValueError, util.get_dirs_from_targz_url, no_param_url)


class DnloadExtractTests(unittest.TestCase):
    targz_url = "https://gitlab.com/90cos/mttl/-/archive/master/mttl-master.tar.gz?path=requirements"
    targz_file = "mttl-master.tar.gz"
    extracted_dir = "mttl-master-requirements"
    test_dir = "Test_files"
    @classmethod
    def setUpClass(cls) -> None:
        os.mkdir(cls.test_dir)

    @classmethod
    def tearDownClass(cls) -> None:
        if os.path.exists(cls.test_dir):
            shutil.rmtree(cls.test_dir)

    def tearDown(self) -> None:
        if os.path.exists(os.path.join(self.test_dir, "requirements")):
            shutil.rmtree(os.path.join(self.test_dir, "requirements"))

    def test_dnload_extract(self):
        util.dnload_extract(self.targz_url, self.test_dir)
        extracted = os.path.exists(os.path.join(self.test_dir, "requirements"))
        self.assertTrue(extracted, "Expected extracted and moved tar.gz files missing")

    def test_dnload_extract_cleanup_targz(self):
        util.dnload_extract(self.targz_url, self.test_dir)
        targz = os.path.exists(self.targz_file)
        self.assertFalse(targz, "Expected tar.gz file not removed")

    def test_dnload_extract_cleanup_mt_dir(self):
        util.dnload_extract(self.targz_url, self.test_dir)
        extract_dir = os.path.exists(self.extracted_dir)
        self.assertFalse(extract_dir, "Expected folder not removed")


class GetDataTests(unittest.TestCase):
    def setUp(self) -> None:
        self.test_path = "Test_files"
        os.mkdir(self.test_path)
        self.json_path = f"{self.test_path}/alg_lsn.rel-link.json"
        self.log = []
        with open(self.json_path, "w") as json_fp:
            json.dump(alg_lsn, json_fp, indent=4)

    def tearDown(self) -> None:
        if os.path.exists(self.test_path):
            shutil.rmtree(self.test_path)

    def test_get_data(self):
        self.assertDictEqual(alg_lsn, util.get_data(self.json_path, self.log), "Mismatch in returned dictionary")
        self.assertListEqual([], self.log, "Mismatch in log entry")

    def test_get_data_no_file(self):
        exp_msg = "util.py ERROR: Unable to open 'no_file': "
        self.assertDictEqual({}, util.get_data("no_file", self.log), "Mismatch in returned dictionary")
        self.assertIn(exp_msg, "\n".join(self.log), "Mismatch in log entry")

    def test_get_data_bad_format(self):
        exp_msg = f"util.py ERROR: Unable to open '{os.path.join(self.test_path, 'bad.rel-link.json')}': "
        data = {4, "three", "food"}
        with open(os.path.join(self.test_path, "bad.rel-link.json"), "w") as fp:
            fp.write(str(data))
        self.assertDictEqual({}, util.get_data(os.path.join(self.test_path, "bad.rel-link.json"), self.log),
                             "Mismatch in returned dictionary")
        self.assertIn(exp_msg, "\n".join(self.log), "Mismatch in log entry")


class LstripNonAlphaTests(unittest.TestCase):
    def setUp(self) -> None:
        self.msg = "Mismatch in processed string"

    def test_leading_digits(self):
        test_strs = [
            "023_Data_Types",
            "023-Data_Types",
            "023Data_Types",
            "23_Data_Types",
            "23-Data_Types",
            "23Data_Types",
            "283746_Data_Typ283746es",
            "283746-Da283746ta_Types",
            "283746Data_283746Types",
            "23_Da2ta_Typ3es",
            "23-Data23_Type32s",
            "23Da3ta_23Typ2es"
        ]
        exp_test_strs = [
            "Data_Types",
            "Data_Types",
            "Data_Types",
            "Data_Types",
            "Data_Types",
            "Data_Types",
            "Data_Typ283746es",
            "Da283746ta_Types",
            "Data_283746Types",
            "Da2ta_Typ3es",
            "Data23_Type32s",
            "Da3ta_23Typ2es"
        ]
        for index, test_str in enumerate(test_strs):
            self.assertEqual(exp_test_strs[index], util.lstrip_nonalpha(test_str))

    def test_leading_non_alnum(self):
        test_strs = [
            "_Data_Types",
            "-Data_Types",
            " Data_Types",
            "?Data_Types",
            "\nData_Types",
            ":\t*%Data_Types",
            "#@\rData_Typ#@es",
            " Da ta Types ",
            "[Data_[Types",
            "}{_Da}ta_Typ{es",
            "_-Data_-Type-_s",
            ",.Da.ta,.Typ,es",
            "\"Data\"-'Types'"
        ]
        exp_test_strs = [
            "Data_Types",
            "Data_Types",
            "Data_Types",
            "Data_Types",
            "Data_Types",
            "Data_Types",
            "Data_Typ#@es",
            "Da ta Types ",
            "Data_[Types",
            "Da}ta_Typ{es",
            "Data_-Type-_s",
            "Da.ta,.Typ,es",
            "Data\"-'Types'"
        ]
        for index, test_str in enumerate(test_strs):
            self.assertEqual(exp_test_strs[index], util.lstrip_nonalpha(test_str))

    def test_leading_alpha(self):
        test_strs = [
            "Data_Types",
            "Data-Types",
            "Data^Types",
            "Data'Types",
            "Data\"Types\"",
            "Data_Types",
            "Data_Typ#@es",
            "Da ta Types ",
            "Data_[Types",
            "Da}ta_Typ{es",
            "Data_-Type-_s",
            "Da.ta,.Typ,es",
            "Data_Typ283746es",
            "Da283746ta_Types",
            "Data_283746Types",
            "Da2ta_Typ3es",
            "Data23_Type32s",
            "Da3ta_23Typ2es"
        ]
        exp_test_strs = [
            "Data_Types",
            "Data-Types",
            "Data^Types",
            "Data'Types",
            "Data\"Types\"",
            "Data_Types",
            "Data_Typ#@es",
            "Da ta Types ",
            "Data_[Types",
            "Da}ta_Typ{es",
            "Data_-Type-_s",
            "Da.ta,.Typ,es",
            "Data_Typ283746es",
            "Da283746ta_Types",
            "Data_283746Types",
            "Da2ta_Typ3es",
            "Data23_Type32s",
            "Da3ta_23Typ2es"
        ]
        for index, test_str in enumerate(test_strs):
            self.assertEqual(exp_test_strs[index], util.lstrip_nonalpha(test_str))

    def test_null_string(self):
        self.assertIsNotNone(util.lstrip_nonalpha(""))
        self.assertEqual("", util.lstrip_nonalpha(""))


if __name__ == "__main__":
    unittest.main()
