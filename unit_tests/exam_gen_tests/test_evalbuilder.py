#!/usr/bin/python3

import sys
import os
import math
import unittest
import unittest.mock as mock
import io
import json
import copy
from colorama import Fore

sys.path.append("scripts")
from lib.exam_generator import evalbuilder, evalquestion
from lib.file_list_generator import dir_walk
from lib.default_strings import attempts_key
from lib.unit_test_helpers import fake_mttl_mongo_db, test_bank_data
from lib.unit_test_helpers.unit_test_helpers import create_files, clean_up, remove_questions_by_keyword

topic1 = "Topic1"
topic2 = "Topic2"
topic3 = "Topic3"
topic4 = "Topic4"
q_id = "{exam_type}/{topic}/dir{num}/question{num}.question.json"
topic1_size = 5
topic2_size = 10
topic3_size = 15
uses1 = [3, 6, 0, 12, 23]
uses2 = [3, 6, 0, 12, 23, 1, 54, 5, 13, 0]
uses3 = [0, 0, 1, 1, 2, 1, 3, 0, 0, 1, 2, 5, 2, 1, 1]
times_asked1 = [6, 12, 0, 24, 46]
times_asked2 = [6, 12, 0, 24, 46, 2, 108, 10, 26, 0]
times_asked3 = [0, 0, 2, 2, 4, 2, 6, 0, 0, 2, 4, 10, 4, 2, 2]
correct_ans1 = [4, 12, 0, 12, 30]
correct_ans2 = [4, 12, 0, 12, 30, 0, 73, 10, 22, 0]
correct_ans3 = [0, 0, 2, 1, 2, 0, 5, 0, 0, 2, 1, 9, 4, 0, 1]
pf_comp_time1 = [96, 60, 75, 85, 100]  # an amount under 480 min max
pf_comp_time2 = [69, 68, 69, 69, 69, 68, 68, 68, 68, 68]  # 480 min max
pf_comp_time3 = [30, 30, 45, 65, 60, 60, 38, 90, 66, 92, 85, 59, 75, 41, 77]  # An amount over 480 min max
pf_comp_time4 = [96, 96, 96, 96, 96, 96, 96, 96, 96, 96]  # an amount over 480 min max to test delta
kn_comp_time1 = [11, 15, 11, 12, 10]  # An amount under 90 min max
kn_comp_time2 = [13, 12, 15, 14, 12, 12, 12, 12, 12, 12]  # 90 min max
kn_comp_time3 = [8, 10, 12, 9, 11, 12, 12, 11, 8, 8, 9, 10, 11, 12, 10]  # An amount over 90 min max
weight1 = [4, 5, 2, 8, 3]
weight2 = [3, 8, 6.5, 5, 2.1, 8.6, 4, 3.8, 2, 4.3]
weight3 = [11.4, 3, 8.6, 2.1, 8, 7.0, 1, 6.5, 5, 4.3, 4, 13, 3.8, 2, 1.9]
ksats1 = [
    ["A0012", "A0013", "A0002", "K0123", "K0122", "K0124", "S0003", "S0010", "S0006"],
    ["A0012", "A0013", "K0123", "K0124", "S0003", "S0010"],
    ["A0112", "A0113", "A0212", "K0123", "K0163", "K0124", "S0203", "S0010", "S0012"],
    ["A0022", "A0023", "K0173", "K0723", "K0124", "S0015", "S0019", "S0119"],
    ["A0312", "K0323", "S0033"]
]
ksats2 = [
    ["A0012", "A0013", "A0002", "K0123", "K0122", "K0124", "S0003", "S0010", "S0006"],
    ["A0012", "A0013", "K0123", "K0124", "S0003", "S0010"],
    ["A0112", "A0113", "A0212", "K0123", "K0163", "K0124", "S0203", "S0010", "S0012"],
    ["A0022", "A0023", "K0173", "K0723", "K0124", "S0015", "S0019", "S0119"],
    ["A0312", "K0323", "S0033"],
    ["A0555", "A0554"],
    ["K0323", "K0354", "K0212"],
    ["T0011"],
    ["S0004", "K0123", "K234", "A0424"],
    ["K0321"]
]
ksats3 = [
    ["A0012", "A0013", "A0002", "K0123", "K0122", "K0124", "S0003", "S0010", "S0006"],
    ["A0012", "A0013", "K0123", "K0124", "S0003", "S0010"],
    ["A0112", "A0113", "A0212", "K0123", "K0163", "K0124", "S0203", "S0010", "S0012"],
    ["A0022", "A0023", "K0173", "K0723", "K0124", "S0015", "S0019", "S0119"],
    ["A0312", "K0323", "S0033"],
    ["A0555", "A0554"],
    ["K0323", "K0354", "K0212"],
    ["T0011"],
    ["S0004", "K0123", "K234", "A0424"],
    ["K0321"],
    ["A0023", "A0024", "A0034", "A0035", "A0042", "A0044", "A0066", "A0777"],
    ["A0023", "A0024", "A0035", "A0042", "A0066", "S0011", "S0012", "S0020", "S0032", "S0015"],
    ["S0111"],
    ["A0013"],
    ["A0432", "S0543", "K0034", "K0036", "T0012"]
]
topics = {
    "Networking": 5,
    "C Programming": 5,
    "Assembly": 5,
    "Python": 5
}
unique_ksats = []
exp_pf_delta = .35  # The difference between the amount of questions generated and requested, as a percentage
exp_kn_delta = .2


def add_ksats(ksat_list: list) -> int:
    # Enables using this functionality without setting up the questioncategory object
    global unique_ksats
    count = 0
    for ksat in ksat_list:
        if ksat not in unique_ksats:
            unique_ksats.append(ksat)
            count += 1
    return count


def calc_expected_weights(multi_ksats_list: list, weights: list, exam_type: str, topic: str) -> dict:
    # Enables using this functionality without setting up the questioncategory object
    expected_questions = {}
    weights.sort(reverse=True)
    if len(multi_ksats_list) == len(weights):
        for i, ksat_list in enumerate(multi_ksats_list):
            ksat_weight = add_ksats(ksat_list)
            # slow the rate of growth of bonus weight via ksat
            ksat_w_mod = math.log2(ksat_weight + 1)  # add 1 to prevent log(1) = 0
            expected_questions[q_id.format(exam_type=exam_type, topic=topic, num=i)] = weights[i] * ksat_w_mod
    return expected_questions


def rsort_dict_by_value(in_dict: dict) -> dict:
    rsorted_result = {}
    for key in sorted(in_dict, key=in_dict.get, reverse=True):
        rsorted_result[key] = in_dict[key]
    return rsorted_result


def sort_dict_by_value(in_dict: dict) -> dict:
    sorted_result = {}
    for key in sorted(in_dict, key=in_dict.get):
        sorted_result[key] = in_dict[key]
    return sorted_result


def get_exp_q_list(q_map: dict, time_allowed: int, comp_times: list, set_size: int) -> (list, int):
    exp_q_list = []
    time_used = 0
    if len(q_map) == len(comp_times):
        for q_id in q_map.keys():
            q_index = int(os.path.basename(q_id).rstrip(".question.json").lstrip("question"))
            if time_used + comp_times[q_index] <= time_allowed and len(exp_q_list) < set_size:
                exp_q_list.append(q_id)
                time_used += comp_times[q_index]
    return exp_q_list, time_used


kn_q_map1_exp = rsort_dict_by_value(calc_expected_weights(ksats1, weight1, "knowledge", topic1))
unique_ksats.clear()
kn_q_map2_exp = rsort_dict_by_value(calc_expected_weights(ksats2, weight2, "knowledge", topic2))
unique_ksats.clear()
kn_q_map3_exp = rsort_dict_by_value(calc_expected_weights(ksats3, weight3, "knowledge", topic3))
unique_ksats.clear()
pf_q_map1_exp = rsort_dict_by_value(calc_expected_weights(ksats1, weight1, "performance", topic1))
unique_ksats.clear()
pf_q_map2_exp = rsort_dict_by_value(calc_expected_weights(ksats2, weight2, "performance", topic2))
unique_ksats.clear()
pf_q_map3_exp = rsort_dict_by_value(calc_expected_weights(ksats3, weight3, "performance", topic3))
unique_ksats.clear()
pf_q_map4_exp = rsort_dict_by_value(calc_expected_weights(ksats2, weight2, "performance", topic4))
del unique_ksats


def get_question_list(topic_sz: int, exam_type: str, q_id: str, topic: str, uses_lst: list, times_asked_lst: list,
                      correct_ans_lst: list, comp_time_lst: list, weight_lst: list, ksats: list) -> list:
    q_list = []
    try:
        for i in range(topic_sz):
            q = evalquestion.EvalQuestion(question_id=q_id.format(exam_type=exam_type, topic=topic, num=i),
                                          topic=topic,
                                          times_provisioned=uses_lst[i],
                                          times_asked=times_asked_lst[i],
                                          correct_answers=correct_ans_lst[i],
                                          complete_time=comp_time_lst[i])
            q.weight = weight_lst[i]
            q.ksat_list = ksats[i]
            q_list.append(q)
    except IndexError:
        raise IndexError("Ensure all input lists to 'get_question_list()' have the same size as input 'topic_sz'")
    except KeyError:
        raise KeyError("Ensure the 'q_id' input param uses the same format as the global 'q_id' variable")
    return q_list


def sum_list(in_list: list) -> int:
    result = 0
    for num in in_list:
        result += num
    return result


class ConstructorDefaultValueTests(unittest.TestCase):
    def setUp(self) -> None:
        self.eb = evalbuilder.EvalBuilder()

    def test_eval_builder_obj(self):
        self.assertIsInstance(self.eb, evalbuilder.EvalBuilder)

    def test_def_q_list(self):
        self.assertListEqual([], self.eb.question_list, "Default 'question_list' value should be an empty list")

    def test_def_cat_list(self):
        self.assertListEqual([], self.eb.category_list, "Default 'category_list' value should be an empty list")

    def test_def_ksat_set(self):
        self.assertSetEqual(set(), self.eb.ksat_set, "Default 'ksat_set' value should be an empty set")

    def test_def_exams_gend(self):
        self.assertEqual(0, self.eb.exams_generated, "Default 'exams_generated' value should be 0")

    def test_def_exams_to_gen(self):
        self.assertEqual(0, self.eb.exams_to_generate, "Default 'exams_to_generate' value should be 0")

    def test_def_time_allowed(self):
        self.assertEqual(0, self.eb.time_allowed, "Default 'time_allowed' value should be 0")

    def test_def_delta(self):
        self.assertEqual(0, self.eb.delta)


class CreateCategoryTests(unittest.TestCase):
    def setUp(self) -> None:
        self.eb = evalbuilder.EvalBuilder()
        self.eb.question_list += get_question_list(topic1_size, "knowledge", q_id, topic1, uses1, times_asked1,
                                                   correct_ans1, kn_comp_time1, weight1, ksats1)
        self.eb.question_list += get_question_list(topic2_size, "knowledge", q_id, topic2, uses2, times_asked2,
                                                   correct_ans2, kn_comp_time2, weight2, ksats2)
        self.eb.question_list += get_question_list(topic3_size, "knowledge", q_id, topic3, uses3, times_asked3,
                                                   correct_ans3, kn_comp_time3, weight3, ksats3)
        self.eb.question_list += get_question_list(topic1_size, "performance", q_id, topic1, uses1, times_asked1,
                                                   correct_ans1, pf_comp_time1, weight1, ksats1)
        self.eb.question_list += get_question_list(topic2_size, "performance", q_id, topic2, uses2, times_asked2,
                                                   correct_ans2, pf_comp_time2, weight2, ksats2)
        self.eb.question_list += get_question_list(topic3_size, "performance", q_id, topic3, uses3, times_asked3,
                                                   correct_ans3, pf_comp_time3, weight3, ksats3)
        # Topic 4 uses Topic 2 variables when they don't need to be different
        self.eb.question_list += get_question_list(topic2_size, "performance", q_id, topic4, uses2, times_asked2,
                                                   correct_ans2, pf_comp_time4, weight2, ksats2)
        self.kn_time_allow = 90
        self.pf_time_allow = 480
        self.time_used = 0
        self.set_size1 = 3
        self.set_size1_gt_delta = 4
        self.set_size2 = 7
        self.set_size3_gt_delta = 14
        self.set_size3_lt_delta = 10
        self.set_sz4_gt_delta = 8
        self.set_sz4_lt_delta = 6

    def test_create_cat_under_time(self):
        # Knowledge category using less than time allowed
        self.eb.delta = exp_kn_delta
        exp_kn_q_list, exp_kn_time = get_exp_q_list(kn_q_map1_exp, self.kn_time_allow, kn_comp_time1, self.set_size1)
        exam, time = self.eb.get_category_questions(topic1, self.set_size1, "knowledge", self.kn_time_allow,
                                                    self.time_used)
        self.assertEqual(exp_kn_time, time, "Mismatch in 'time used' calculation")
        self.assertListEqual(exp_kn_q_list, [x.question_id for x in exam], "Mismatch in question list generation")

        # Performance category using less than time allowed
        self.eb.delta = exp_pf_delta
        exp_pf_q_list, exp_pf_time = get_exp_q_list(pf_q_map1_exp, self.pf_time_allow, pf_comp_time1, self.set_size1)
        exam, time = self.eb.get_category_questions(topic1, self.set_size1, "performance", self.pf_time_allow,
                                                    self.time_used)
        self.assertEqual(exp_pf_time, time, "Mismatch in 'time used' calculation")
        self.assertListEqual(exp_pf_q_list, [x.question_id for x in exam], "Mismatch in question list generation")

    def test_create_cat_exact_time(self):
        # Knowledge category using exact time allowed
        self.eb.delta = exp_kn_delta
        exp_kn_q_list, exp_kn_time = get_exp_q_list(kn_q_map2_exp, self.kn_time_allow, kn_comp_time2, self.set_size2)
        exam, time = self.eb.get_category_questions(topic2, self.set_size2, "knowledge", self.kn_time_allow,
                                                    self.time_used)
        self.assertEqual(exp_kn_time, time, "Mismatch in 'time used' calculation")
        self.assertListEqual(exp_kn_q_list, [x.question_id for x in exam], "Mismatch in question list generation")

        # Performance category using exact time allowed
        self.eb.delta = exp_pf_delta
        exp_pf_q_list, exp_pf_time = get_exp_q_list(pf_q_map2_exp, self.pf_time_allow, pf_comp_time2, self.set_size2)
        exam, time = self.eb.get_category_questions(topic2, self.set_size2, "performance", self.pf_time_allow,
                                                    self.time_used)
        self.assertEqual(exp_pf_time, time, "Mismatch in 'time used' calculation")
        self.assertListEqual(exp_pf_q_list, [x.question_id for x in exam], "Mismatch in question list generation")

    def test_create_cat_gt_time_lt_delta(self):
        # Knowledge category using more than time allowed and under delta of questions generated to requested amount
        self.eb.delta = exp_kn_delta
        exp_kn_q_list, exp_kn_time = get_exp_q_list(kn_q_map3_exp, self.kn_time_allow, kn_comp_time3,
                                                    self.set_size3_lt_delta)
        exam, time = self.eb.get_category_questions(topic3, self.set_size3_lt_delta, "knowledge", self.kn_time_allow,
                                                    self.time_used)
        self.assertEqual(exp_kn_time, time, "Mismatch in 'time used' calculation")
        self.assertListEqual(exp_kn_q_list, [x.question_id for x in exam], "Mismatch in question list generation")
        # Check if delta between questions generated and those requested is within limits
        self.assertAlmostEqual(self.set_size3_lt_delta, len(exam), delta=exp_pf_delta * self.set_size3_lt_delta,
                               msg="Delta between questions generated and requested not checked")

        # Performance category using more than time allowed and under delta of questions generated to requested amount
        self.eb.delta = exp_pf_delta
        exp_pf_q_list, exp_pf_time = get_exp_q_list(pf_q_map3_exp, self.pf_time_allow, pf_comp_time3,
                                                    self.set_size3_lt_delta)
        exam, time = self.eb.get_category_questions(topic3, self.set_size3_lt_delta, "performance", self.pf_time_allow,
                                                    self.time_used)
        self.assertEqual(exp_pf_time, time, "Mismatch in 'time used' calculation")
        self.assertListEqual(exp_pf_q_list, [x.question_id for x in exam], "Mismatch in question list generation")
        # Check if delta between questions generated and those requested is within limits
        self.assertAlmostEqual(self.set_size3_lt_delta, len(exam), delta=exp_pf_delta * self.set_size3_lt_delta,
                               msg="Delta between questions generated and requested not checked")

        exp_pf_q_list, exp_pf_time = get_exp_q_list(pf_q_map4_exp, self.pf_time_allow, pf_comp_time4,
                                                    self.set_sz4_lt_delta)
        exam, time = self.eb.get_category_questions(topic4, self.set_sz4_lt_delta, "performance", self.pf_time_allow,
                                                    self.time_used)
        self.assertEqual(exp_pf_time, time, "Mismatch in 'time used' calculation")
        self.assertListEqual(exp_pf_q_list, [x.question_id for x in exam], "Mismatch in question list generation")
        # Check if delta between questions generated and those requested is within limits
        self.assertAlmostEqual(self.set_sz4_lt_delta, len(exam), delta=exp_pf_delta * self.set_sz4_lt_delta,
                               msg="Delta between questions generated and requested not checked")

    def test_create_cat_gt_time_gt_delta(self):
        # Knowledge category using more than time allowed and over delta of questions generated to requested amount
        self.eb.delta = exp_kn_delta
        self.assertRaises(ValueError, self.eb.get_category_questions, topic3, self.set_size3_gt_delta, "knowledge",
                          self.kn_time_allow, self.time_used)

        # Performance category using more than time allowed and over delta of questions generated to requested amount
        self.eb.delta = exp_pf_delta
        self.assertRaises(ValueError, self.eb.get_category_questions, topic3, self.set_size3_gt_delta, "performance",
                          self.pf_time_allow, self.time_used)
        self.assertRaises(ValueError, self.eb.get_category_questions, topic4, self.set_sz4_gt_delta, "performance",
                          self.pf_time_allow, self.time_used)


class RegisterCategoryTests(unittest.TestCase):
    def setUp(self) -> None:
        self.eb = evalbuilder.EvalBuilder()

    def test_register_category(self):
        for category, amount in topics.items():
            self.eb.register_category(category, amount)
            self.assertIn((category, amount), self.eb.category_list, "Mismatch after registering category")


class SortCatByValueTests(unittest.TestCase):
    def setUp(self) -> None:
        self.eb = evalbuilder.EvalBuilder()
        self.exp_sorted_by_value = []
        self.exp_unsorted_list = []
        for category, amount in topics.items():
            self.eb.register_category(category, amount)
            self.exp_unsorted_list.append((category, amount))
        for category, amount in sort_dict_by_value(topics).items():
            self.exp_sorted_by_value.append((category, amount))

    def test_unsorted_cat_by_value(self):
        self.assertListEqual(self.exp_unsorted_list, self.eb.category_list)

    def test_sorted_cat_by_value(self):
        self.eb.sort_cat_list_by_value()
        self.assertListEqual(self.exp_sorted_by_value, self.eb.category_list)


class ClearCategoriesTests(unittest.TestCase):
    def setUp(self) -> None:
        self.eb = evalbuilder.EvalBuilder()
        for category, amount in topics.items():
            self.eb.register_category(category, amount)

    def test_clear_categories(self):
        self.eb.clear_categories()
        self.assertListEqual([], self.eb.category_list)


class FeedbackUpdateTests(unittest.TestCase):
    def setUp(self) -> None:
        self.exam_list = []
        self.exp_question_list = []
        self.eb = evalbuilder.EvalBuilder()
        self.eb.question_list += get_question_list(topic3_size, "knowledge", q_id, topic3, uses3, times_asked3,
                                                   correct_ans3, kn_comp_time3, weight3, ksats3)
        self.exp_question_list += get_question_list(topic3_size, "knowledge", q_id, topic3, uses3, times_asked3,
                                                    correct_ans3, kn_comp_time3, weight3, ksats3)
        self.eb.exams_generated = 3
        for i, question in enumerate(self.eb.question_list):
            if i < 10:
                self.exam_list.append(question.question_id)
        for question in self.exp_question_list:
            if question.question_id in self.exam_list:
                question.times_provisioned += 1
            question.get_question_weight(self.eb.exams_generated + 1)

    def test_feedback_update_q_times_provisioned(self):
        self.eb.feedback_update(self.exam_list)
        self.assertListEqual([q.times_provisioned for q in self.exp_question_list],
                             [q.times_provisioned for q in self.eb.question_list],
                             "Mismatch in uses after feedback update")

    def test_feedback_update_q_weight(self):
        self.eb.feedback_update(self.exam_list)
        self.assertListEqual([q.weight for q in self.exp_question_list], [q.weight for q in self.eb.question_list],
                             "Mismatch in weight after feedback update")


class GetQuestionTests(unittest.TestCase):
    def setUp(self) -> None:
        self.eb = evalbuilder.EvalBuilder()
        self.eb.question_list += get_question_list(topic3_size, "knowledge", q_id, topic3, uses3, times_asked3,
                                                   correct_ans3, kn_comp_time3, weight3, ksats3)
        self.q_expected = evalquestion.EvalQuestion(question_id=q_id.format(exam_type="knowledge", topic=topic3, num=4),
                                                    topic=topic3,
                                                    times_provisioned=uses3[4],
                                                    times_asked=times_asked3[4],
                                                    correct_answers=correct_ans3[4],
                                                    complete_time=kn_comp_time3[4])
        self.q_expected.weight = weight3[4]
        self.q_expected.ksat_list = ksats3[4]

    def test_get_question(self):
        self.assertIsInstance(self.eb.get_question(self.q_expected.question_id), evalquestion.EvalQuestion)
        self.assertEqual(self.q_expected.question_id, self.eb.get_question(self.q_expected.question_id).question_id)
        self.assertEqual(self.q_expected.topic, self.eb.get_question(self.q_expected.question_id).topic)
        self.assertEqual(self.q_expected.times_provisioned,
                         self.eb.get_question(self.q_expected.question_id).times_provisioned)
        self.assertEqual(self.q_expected.times_asked, self.eb.get_question(self.q_expected.question_id).times_asked)
        self.assertEqual(self.q_expected.correct_answers,
                         self.eb.get_question(self.q_expected.question_id).correct_answers)
        self.assertEqual(self.q_expected.complete_time, self.eb.get_question(self.q_expected.question_id).complete_time)
        self.assertEqual(self.q_expected.weight, self.eb.get_question(self.q_expected.question_id).weight)
        self.assertListEqual(self.q_expected.ksat_list, self.eb.get_question(self.q_expected.question_id).ksat_list)
        self.assertEqual(self.q_expected.q_weight_scalar,
                         self.eb.get_question(self.q_expected.question_id).q_weight_scalar)


class JsonLoadTests(unittest.TestCase):
    test_dir = "test_evalbuilder_files"
    @classmethod
    def setUpClass(cls) -> None:
        cls.knowledge = test_bank_data.knowledge
        cls.performance = test_bank_data.performance
        cls.mongodb = fake_mttl_mongo_db.FakeMttlDb(cls.test_dir)
        create_files(cls.knowledge, cls.test_dir)
        create_files(cls.performance, cls.test_dir)

    @classmethod
    def tearDownClass(cls) -> None:
        clean_up(cls.test_dir)

    def setUp(self) -> None:
        self.eb = evalbuilder.EvalBuilder()
        self.eb.exams_generated = 12
        self.q_list_expected = dir_walk(self.test_dir, ["knowledge", "performance"], ".question.json")
        self.kn_time_allow = 90
        self.pf_time_allow = 480

    def test_create_exam(self):
        expected_num_questions = 0
        self.eb.set_question_list(self.q_list_expected, self.mongodb.db)
        for category, amount in topics.items():
            self.eb.register_category(category, amount)
            expected_num_questions += amount
        self.eb.time_allowed = self.kn_time_allow
        exam = self.eb.create_exam("knowledge")
        self.assertAlmostEqual(expected_num_questions, len(exam), delta=exp_kn_delta,
                               msg="Mismatch in number of requested questions")
        for q in exam:
            self.assertIn(q, [x.question_id for x in self.eb.question_list],
                          "Mismatch between added question and question list")

    def test_set_q_list_invalid_dict_key(self):
        expected_output = f"Key Not Found: '{attempts_key}'"

        # Setup one json file with a missing key for testing and restore data before returning
        with open(self.q_list_expected[0], "r") as q_file:
            q_data = json.load(q_file)  # Data to modify
        orig_q_data = copy.deepcopy(q_data)  # Preserve original data
        if attempts_key in q_data:
            # The missing key to test
            del q_data[attempts_key]
        with open(self.q_list_expected[0], "w") as q_file:
            # Since the self.eb.set_question_list() method loads from a file
            json.dump(q_data, q_file, indent=4)

        # This is where the test happens
        with mock.patch('sys.stdout', new=io.StringIO()) as fake_out:
            self.eb.set_question_list(self.q_list_expected, self.mongodb.db)
            self.assertEqual(expected_output, fake_out.getvalue().strip(), "Mismatch in message when KeyError caught.")

        # Restore modified data for other tests
        with open(self.q_list_expected[0], "w") as q_file:
            json.dump(orig_q_data, q_file, indent=4)

    def test_set_q_list_invalid_files(self):
        invalid_file = f"{self.test_dir}/knowledge/invalid.question.json"
        expected_output = f"Could not read file {invalid_file}"
        with mock.patch('sys.stdout', new=io.StringIO()) as fake_out:
            self.eb.set_question_list([invalid_file], self.mongodb.db)
            self.assertEqual(expected_output, fake_out.getvalue().strip(),
                             "Mismatch in message when FileNotFound caught")

    def test_set_q_list_valid_files(self):
        self.eb.set_question_list(self.q_list_expected, self.mongodb.db)
        self.assertListEqual(self.q_list_expected, [x.question_id for x in self.eb.question_list],
                             "Mismatch in list after setting question list")


if __name__ == "__main__":
    unittest.main()
