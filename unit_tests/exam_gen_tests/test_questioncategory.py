#!/usr/bin/python3

import sys
import math
import unittest
sys.path.append("scripts")
from lib.exam_generator import evalquestion, questioncategory


def calc_time_used(exp_quest_list: list, exam_list: list) -> int:
    time_used = 0
    for exam_q in exam_list:
        for q in exp_quest_list:
            if exam_q.question_id == q.question_id:
                time_used += q.complete_time
    return time_used


class ConstructorDefaultValueTests(unittest.TestCase):
    def setUp(self) -> None:
        self.topic = ""
        self.q_cat = questioncategory.QuestionCategory(self.topic)

    def test_q_list_def_value(self):
        self.assertListEqual([], self.q_cat.question_list, "Default 'question_list' value should be an empty list")

    def test_topic_def_value(self):
        self.assertEqual(self.topic, self.q_cat.topic, "'topic' value mismatch")

    def test_topic_size_def_value(self):
        self.assertEqual(1, self.q_cat.topic_size, "Default 'topic_size' value should be 1")

    def test_ksat_set_def_value(self):
        self.assertSetEqual(set(), self.q_cat.ksat_set, "Default 'ksat_set' value should be 0")


class ConstructorAssignedValueTests(unittest.TestCase):
    def setUp(self) -> None:
        self.topic = "My Topic"
        self.topic_size = 4
        self.q_list = ["q1", "q2", "q3"]
        self.ksat_set = {"1", "2", "3"}
        self.q_cat = questioncategory.QuestionCategory(self.topic, self.topic_size)
        self.q_cat.question_list = self.q_list
        self.q_cat.ksat_set = self.ksat_set

    def test_q_list_value(self):
        self.assertListEqual(self.q_list, self.q_cat.question_list,
                             f"'question_list' value not set to requested value {self.q_list}")

    def test_topic_value(self):
        self.assertEqual(self.topic, self.q_cat.topic, f"'topic' value not set to requested value {self.topic}")

    def test_topic_size_value(self):
        self.assertEqual(self.topic_size, self.q_cat.topic_size,
                         f"'topic_size' value not set to requested value {self.topic_size}")

    def test_ksat_set_value(self):
        self.assertSetEqual(self.ksat_set, self.q_cat.ksat_set,
                            f"'ksat_set' value not set to requested value {self.ksat_set}")


class WeightSortTests(unittest.TestCase):
    def setUp(self) -> None:
        self.q_list_expected = []
        self.q_unsorted_list_test = []
        self.q_sorted_list_test = []
        self.topic = "My Topic"
        self.topic_size = 5
        self.weight_unsorted = [289, 0, -23, 2, 1]
        self.weight_rsorted = [289, 2, 1, 0, -23]
        self.weight_sorted = [-23, 0, 2, 1, 289]
        for i in range(self.topic_size):
            q_unsorted = evalquestion.EvalQuestion()
            q_sorted = evalquestion.EvalQuestion()
            q_rsorted = evalquestion.EvalQuestion()
            q_unsorted.weight = self.weight_unsorted[i]
            q_sorted.weight = self.weight_sorted[i]
            q_rsorted.weight = self.weight_rsorted[i]
            self.q_unsorted_list_test.append(q_unsorted)
            self.q_sorted_list_test.append(q_sorted)
            self.q_list_expected.append(q_rsorted)
        self.q_cat = questioncategory.QuestionCategory(topic=self.topic, topic_size=self.topic_size)

    def test_unsorted_weight_sort(self):
        self.q_cat.question_list = self.q_unsorted_list_test
        self.q_cat.weight_sort()
        self.assertListEqual(self.weight_rsorted, [x.weight for x in self.q_cat.question_list],
                             "'question_list' should be in reverse order by weight")

    def test_sorted_weight_sort(self):
        self.q_cat.question_list = self.q_sorted_list_test
        self.q_cat.weight_sort()
        self.assertListEqual(self.weight_rsorted, [x.weight for x in self.q_cat.question_list],
                             "'question_list' should be in reverse order by weight")

    def test_rsorted_weight_sort(self):
        self.q_cat.question_list = self.q_list_expected
        self.q_cat.weight_sort()
        self.assertListEqual(self.weight_rsorted, [x.weight for x in self.q_cat.question_list],
                             "'question_list' should be in reverse order by weight")


class AddKsatsTests(unittest.TestCase):
    def setUp(self) -> None:
        self.q_cat = questioncategory.QuestionCategory("My Topic")
        self.unique_ksats = ["A0012", "A0013", "A0002", "K0123", "K0122", "K0124", "S0003", "S0010", "S0006"]
        self.dup_ksats = ["A0012", "A0013", "A0012", "K0123", "K0123", "K0124", "S0003", "S0010", "S0010"]
        self.unique_count = len(self.unique_ksats)
        self.dup_count = 6

    def test_unique_ksat_list(self):
        self.assertEqual(self.unique_count, self.q_cat.add_ksats(self.unique_ksats))
        self.assertSetEqual(set(self.unique_ksats), self.q_cat.ksat_set)

    def test_duplicate_ksat_list(self):
        self.assertEqual(self.dup_count, self.q_cat.add_ksats(self.dup_ksats))
        self.assertSetEqual(set(self.dup_ksats), self.q_cat.ksat_set)


class ApplyKsatWeightsTests(unittest.TestCase):
    def setUp(self) -> None:
        self.q_list_test = []
        self.topic = "My Topic"
        self.topic_size = 5
        self.weight = [289, 2, 1, 0, -23]
        self.ksats = [
            ["A0012", "A0013", "A0002", "K0123", "K0122", "K0124", "S0003", "S0010", "S0006"],
            ["A0012", "A0013", "A0012", "K0123", "K0123", "K0124", "S0003", "S0010", "S0010"],
            ["A0112", "A0113", "A0212", "K0123", "K0163", "K0124", "S0203", "S0010", "S0012"],
            ["A0022", "A0023", "A0022", "K0173", "K0723", "K0124", "S0015", "S0019", "S0119"],
            ["A0312", "K0323", "S0033"]
        ]
        self.ksats_added = []
        unique_ksats = []
        # Build the list of expected KSATs added to the list of unique KSATs
        for ksat_list in self.ksats:
            count = 0
            for ksat in ksat_list:
                if ksat not in unique_ksats:
                    unique_ksats.append(ksat)
                    count += 1
            self.ksats_added.append(count)
        del unique_ksats
        for i in range(self.topic_size):
            q = evalquestion.EvalQuestion()
            q.weight = self.weight[i]
            q.ksat_list = self.ksats[i]
            self.q_list_test.append(q)
        self.q_cat = questioncategory.QuestionCategory(topic=self.topic, topic_size=self.topic_size)
        self.q_cat.question_list += self.q_list_test

    def test_apply_ksat_weights(self):
        self.q_cat.apply_ksat_weights()
        for i, q in enumerate(self.q_cat.question_list):
            expected = self.weight[i] * math.log2(self.ksats_added[i] + 1)
            self.assertEqual(expected, q.weight)


class CreateQuestionSetTests(unittest.TestCase):
    def setUp(self) -> None:
        self.q_list_expected = []
        self.q_list_test = []
        self.q_id = "dir{num}/dir{num}/question{num}.rel-link.json"
        self.topic = "My Topic"
        self.topic_size = 5
        self.complete_time = [-2, 0, 1, 17, 120]
        self.weight = [289, 2, 1, 0, -23]
        self.ksats = [
            ["A0012", "A0013", "A0002", "K0123", "K0122", "K0124", "S0003", "S0010", "S0006"],
            ["A0012", "A0013", "A0012", "K0123", "K0123", "K0124", "S0003", "S0010", "S0010"],
            ["A0112", "A0113", "A0212", "K0123", "K0163", "K0124", "S0203", "S0010", "S0012"],
            ["A0022", "A0023", "A0022", "K0173", "K0723", "K0124", "S0015", "S0019", "S0119"],
            ["A0312", "K0323", "S0033"]
        ]
        self.time_allowed = 90
        self.time_used = 0
        self.set_size = 3
        for i in range(self.topic_size):
            q_test = evalquestion.EvalQuestion(question_id=self.q_id.format(num=i),
                                               topic=self.topic,
                                               complete_time=self.complete_time[i])
            q_expected = evalquestion.EvalQuestion(question_id=self.q_id.format(num=i),
                                                   topic=self.topic,
                                                   complete_time=self.complete_time[i])
            q_test.weight = self.weight[i]
            q_test.ksat_list = self.ksats[i]
            q_expected.weight = self.weight[i]
            q_expected.ksat_list = self.ksats[i]
            self.q_list_test.append(q_test)
            self.q_list_expected.append(q_expected)
        self.q_cat_test = questioncategory.QuestionCategory(topic=self.topic, topic_size=self.topic_size)
        self.q_cat_test.question_list += self.q_list_test
        self.q_cat_expected = questioncategory.QuestionCategory(topic=self.topic, topic_size=self.topic_size)
        self.q_cat_expected.question_list += self.q_list_expected
        self.q_cat_expected.apply_ksat_weights()
        self.q_cat_expected.weight_sort()

    def test_create_question_set_return(self):
        expected_time_used = 0
        for time_used in self.complete_time[:self.set_size]:
            if expected_time_used <= self.time_allowed:
                expected_time_used += time_used
        expected_qs = [x.question_id for x in self.q_cat_expected.question_list[:self.set_size]]
        exam_list, self.time_used = self.q_cat_test.create_question_set(set_size=self.set_size,
                                                                        time_allowed=self.time_allowed,
                                                                        time_used=self.time_used)
        self.assertEqual(expected_time_used, self.time_used, "time_used not calculated correctly")
        self.assertListEqual(expected_qs, [x.question_id for x in exam_list], "Question list mismatch")

    def test_create_question_set_exception(self):
        self.assertRaises(IndexError, self.q_cat_test.create_question_set, set_size=self.topic_size + self.set_size,
                          time_allowed=self.time_allowed, time_used=self.time_used)

    def test_create_rand_question_set_return(self):
        exam_list, self.time_used = self.q_cat_test.create_rand_quest_set(set_size=self.topic_size,
                                                                          time_allowed=self.time_allowed,
                                                                          time_used=self.time_used)
        expected_time_used = calc_time_used(self.q_cat_test.question_list, exam_list)
        # The last question won't get selected due to its completion time of 120 so exclude the last
        exp_q_list = [x.question_id for x in self.q_list_expected[:-1]]
        exp_q_list.sort()
        exam_list_qs = [x.question_id for x in exam_list]
        exam_list_qs.sort()
        self.assertEqual(expected_time_used, self.time_used, "time_used not calculated correctly")
        self.assertListEqual(exp_q_list, exam_list_qs, "Question list mismatch")
        self.time_used = 0
        exam_list, self.time_used = self.q_cat_test.create_rand_quest_set(set_size=self.set_size,
                                                                          time_allowed=self.time_allowed,
                                                                          time_used=self.time_used)
        self.assertEqual(self.set_size, len(exam_list), "Question list mismatch")
        expected_time_used = calc_time_used(self.q_cat_test.question_list, exam_list)
        self.assertEqual(expected_time_used, self.time_used, "time_used not calculated correctly")

    def test_create_rand_question_set_exception(self):
        self.assertRaises(IndexError, self.q_cat_test.create_question_set, set_size=self.topic_size + self.set_size,
                          time_allowed=self.time_allowed, time_used=self.time_used)


class UpdateQuestionSetTests(unittest.TestCase):
    def setUp(self) -> None:
        self.q_list_expected = []
        self.q_list_test = []
        self.q_id = "dir{num}/dir{num}/question{num}.rel-link.json"
        self.topic = "My Topic"
        self.uses_test = [2, -1, 0, 45, 300]
        self.uses_expected = [3, 0, 1, 46, 301]
        self.topic_size = 5
        self.weight = [289, 2, 1, 0, -23]
        self.num_tests = 2
        # q_weight_scalar * (max(exams_generated, 1) / max(uses_test, 1)); 1 is to prevent div by zero error
        self.weight_expected = [1 * (max(self.num_tests, 1) / max(use, 1)) for use in self.uses_expected]
        for i in range(self.topic_size):
            q_test = evalquestion.EvalQuestion(question_id=self.q_id.format(num=i), times_provisioned=self.uses_test[i])
            q_expected = evalquestion.EvalQuestion(question_id=self.q_id.format(num=i), times_provisioned=self.uses_expected[i])
            q_test.weight = self.weight[i]
            q_expected.weight = self.weight[i]
            self.q_list_test.append(q_test)
            self.q_list_expected.append(q_expected)
        self.q_cat_test = questioncategory.QuestionCategory(topic=self.topic, topic_size=self.topic_size)
        self.q_cat_test.question_list += self.q_list_test
        self.q_cat_expected = questioncategory.QuestionCategory(topic=self.topic, topic_size=self.topic_size)
        self.q_cat_expected.question_list += self.q_list_expected
        self.q_cat_expected.apply_ksat_weights()
        self.q_cat_expected.weight_sort()

    def test_update_quest_set(self):
        self.q_cat_test.update_question_set(self.q_cat_test.question_list, self.num_tests)
        self.assertListEqual(self.uses_expected, [x.times_provisioned for x in self.q_cat_test.question_list])
        self.assertListEqual(self.weight_expected, [x.weight for x in self.q_cat_test.question_list])


if __name__ == "__main__":
    unittest.main()
