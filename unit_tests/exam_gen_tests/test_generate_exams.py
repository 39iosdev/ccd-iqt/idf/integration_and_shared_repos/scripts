import os
import io
import json
import shutil
import copy
import unittest
import argparse
import unittest.mock as mock
from colorama import Fore
from lib.exam_generator import evalbuilder
import generate_exams
from lib.file_list_generator import dir_walk
from lib.unit_test_helpers import fake_mttl_mongo_db, test_bank_data
from lib.unit_test_helpers import unit_test_helpers
from lib import mttl_db_helpers


class GenerateExamsTests(unittest.TestCase):
    test_dir = "test_gen_exams_files"
    keyword = ".question.json"
    q_dirs = ["knowledge", "performance"]
    prod_stats_file = "lib/exam_generator/test_gen_stats.json"
    test_stats_file = "test_gen_stats.json"
    db_name = "mttl_unit_test"

    knowledge_topic_gen_amounts = {
        "Assembly": 5,
        "C Programming": 5,
        "Networking": 5,
        "Python": 5,
        "Algorithms": 0,
        "Data Structures": 0
    }

    performance_topic_gen_amounts = {
        "Assembly": 0,
        "C Programming": 2,
        "Python": 2,
        "Networking": 1,
        "Reverse Engineering": 0
    }

    knowledge = test_bank_data.knowledge
    knowl_new_topics = {
        "knowledge/New_Topic/question_1/question_1.question.json":
        {
            "_id": "BD_NT_0001",
            "rel-link_id": "5ee925fffffffffffffffff1",
            "question_name": "question_1",
            "question_path": "knowledge/New_Topic/question_1",
            "question_type": "knowledge",
            "topic": "New Topic",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "New Topic Question 1?",
            "choices": [
                "Choice A",
                "Choice B",
                "Choice C",
                "Choice D"
            ],
            "answer": 1,
            "explanation": [
                "The reason the correct answer is correct."
            ]
        },
        "knowledge/New_Topic/question_2/question_2.question.json":
        {
            "_id": "BD_NT_0002",
            "rel-link_id": "5ee925fffffffffffffffff2",
            "question_name": "question_2",
            "question_path": "knowledge/New_Topic/question_2",
            "question_type": "knowledge",
            "topic": "New Topic",
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "New Topic Question 2?",
            "choices": [
                "Choice A",
                "Choice B",
                "Choice C",
                "Choice D"
            ],
            "answer": 3,
            "explanation": [
                "The reason the correct answer is correct."
            ]
        }
    }
    performance = test_bank_data.performance
    perf_new_topics = {
        "performance/New_Topic/question_1/question_1.question.json":
        {
            "_id": "BD_NT_0001",
            "rel-link_id": "5ee925eeeeeeeeeeeeeeeee1",
            "question_name": "question_1",
            "question_path": "performance/New_Topic/question_1",
            "question_type": "performance_dev",
            "topic": "New Topic",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/New_Topic/question_1/Instructions.txt"
            },
            "support_files": [],
            "tests": [],
            "solution": "performance/New_Topic/question_1/solution_file",
            "dependencies": {
                "libraries": [],
                "req_software": []
            }
        },
        "performance/New_Topic/question_2/question_2.question.json":
        {
            "_id": "BD_NT_0002",
            "rel-link_id": "5ee925eeeeeeeeeeeeeeeee2",
            "question_name": "question_2",
            "question_path": "performance/New_Topic/question_2",
            "question_type": "performance_dev",
            "topic": "New Topic",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": {
                "filepath": "performance/New_Topic/question_2/Instructions.txt"
            },
            "support_files": [],
            "tests": [],
            "solution": "performance/New_Topic/question_2/solution_file",
            "dependencies": {
                "libraries": [],
                "req_software": []
            }
        }
    }

    class InputArgs(argparse.Namespace):
        # This is a mock argparse object used for testing purposes only.
        # This prevents argparse collisions in certain modules.
        def __init__(self, q_dirs: list = None, keyword: str = "", log_file: str = "", mql_file_name: str = "",
                     max_choices: int = 0, out_dir: str = "", exam_files: str = "", perf_exam_name: str = "", test_stats_file: str = "",
                     q_type: str = "", db_name: str = "", host: str = "", port: int = -1):
            self.root_quest_dirs = q_dirs
            if q_dirs is None:
                self.root_quest_dirs = []
            self.filter = keyword
            self.log_file = log_file
            self.nameof_mql_csv = mql_file_name
            self.max_choices = max_choices
            self.out_dir = out_dir
            self.exam_files = exam_files
            self.perf_exam_name = perf_exam_name
            self.stats_file = test_stats_file
            self.typeof_question = q_type
            self.db_name = db_name
            self.Host = host
            self.Port = port

    def set_gen_stats(self):
        self.expected_stats["knowledge_stats"]["topics"].update(self.knowledge_topic_gen_amounts)
        self.expected_stats["performance_stats"]["topics"].update(self.performance_topic_gen_amounts)

    def reset_gen_stats(self):
        self.expected_stats.update(self.original_stats)

    def save_gen_stats(self, stat_path):
        with open(stat_path, "w") as stat_fp:
            json.dump(self.expected_stats, stat_fp, indent=4)

    @classmethod
    def setUpClass(cls) -> None:
        # Setup test generator stats files; these are necessary for testing the gen stats output/data
        scripts_font = "lib/font_files/monaco.ttf"
        unit_test_helpers.create_files(cls.knowledge, cls.test_dir)
        unit_test_helpers.create_files(cls.performance, cls.test_dir)
        # The list of expected questions
        cls.expected_questions = dir_walk(cls.test_dir, cls.q_dirs, cls.keyword)
        # Copy font file to the repo directory so the text_to_image.py script will work
        shutil.copyfile(scripts_font, os.path.join(cls.test_dir, "monaco.ttf"))
        shutil.copyfile(cls.prod_stats_file, os.path.join(cls.test_dir, cls.test_stats_file))
        cls.mongodb = fake_mttl_mongo_db.FakeMttlDb(cls.test_dir)
        shutil.copytree(os.path.abspath('exam_files'), os.path.join(cls.test_dir, 'exam_files'), dirs_exist_ok=True)

    @classmethod
    def tearDownClass(cls) -> None:
        unit_test_helpers.clean_up(cls.test_dir)

    def setUp(self) -> None:
        self.not_found_file = "not_found.json"
        self.bad_json_file = "bad_json.json"
        with open(os.path.join(self.test_dir, self.test_stats_file), "r") as stats_fp:
            self.expected_stats = json.load(stats_fp)
        self.original_stats = copy.deepcopy(self.expected_stats)
        self.dst_dir = f"{self.test_dir}_copy_test"
        self.out_dir = "gend_exams"
        self.exam_files = "exam_files"
        self.max_choices = 5
        self.mql_file_name = f"{self.test_dir}.csv"
        self.perf_exam_name = "perf_exam"
        self.tested_main = False

    def tearDown(self) -> None:
        if os.path.exists(self.bad_json_file):
            os.remove(self.bad_json_file)
        if os.path.exists(self.dst_dir):
            shutil.rmtree(self.dst_dir)
        if os.path.exists(self.out_dir):
            shutil.rmtree(self.out_dir)
        if self.tested_main:
            # Go back to the original working directory
            os.chdir("..")
            # Reset the generator stats back to the original data
            self.reset_gen_stats()
            self.save_gen_stats(os.path.join(self.test_dir, self.test_stats_file))

    def test_get_test_gen_stats_rtn(self):
        self.assertDictEqual(self.expected_stats,
                             generate_exams.get_test_gen_stats(os.path.join(self.test_dir, self.test_stats_file)),
                             "Mismatch in test generator stats files")

    def test_get_test_gen_stats_err(self):
        # Test output when FileNotFound is raised
        self.assertRaises(ValueError, generate_exams.get_test_gen_stats, self.not_found_file)

        # Test output when json.JSONDecodeError is raised
        with open(self.bad_json_file, "w") as json_fp:
            json_fp.writelines("{'some key':\"some value\"}")  # A purposely invalid JSON format for testing
        self.assertRaises(ValueError, generate_exams.get_test_gen_stats, self.bad_json_file)

    def test_get_cur_topics_rtn(self):
        # Add the knowledge new topic questions for this test
        unit_test_helpers.create_files(self.knowl_new_topics, self.test_dir)
        # Add the performance new topic questions for this test
        unit_test_helpers.create_files(self.perf_new_topics, self.test_dir)
        # Get a new list of questions that include the New_Topic questions without mangling the list for other tests
        expected_questions = dir_walk(self.test_dir, self.q_dirs, self.keyword)
        expected_cur_topics = {
            "knowledge_stats": {
                "topics": {
                    "Algorithms": 0,
                    "Assembly": 0,
                    "C Programming": 0,
                    "Data Structures": 0,
                    "Networking": 0,
                    "New Topic": 0,
                    "Python": 0
                }
            },
            "performance_stats": {
                "topics": {
                    "Assembly": 0,
                    "C Programming": 0,
                    "Networking": 0,
                    "New Topic": 0,
                    "Python": 0,
                    "Reverse Engineering": 0
                }
            }
        }
        returned = generate_exams.get_cur_topics(expected_questions)
        # Cleanup files that are only used for this test
        unit_test_helpers.remove_folder(os.path.join(self.test_dir, "knowledge", "New_Topic"))
        unit_test_helpers.remove_folder(os.path.join(self.test_dir, "performance", "New_Topic"))
        self.assertDictEqual(expected_cur_topics, returned, "Mismatch in generated current topics")

    def test_get_cur_topics_err(self):
        # Test output when FileNotFound is raised
        self.assertRaises(ValueError, generate_exams.get_cur_topics, self.not_found_file)

        # Test output when json.JSONDecodeError is raised
        with open(self.bad_json_file, "w") as json_fp:
            json_fp.writelines("{'some key':\"some value\"}")  # A purposely invalid JSON format for testing
        self.assertRaises(ValueError, generate_exams.get_cur_topics, self.bad_json_file)

    def test_update_topics(self):
        # Add the knowledge new topic questions for this test
        unit_test_helpers.create_files(self.knowl_new_topics, self.test_dir)
        # Add the performance new topic questions for this test
        unit_test_helpers.create_files(self.perf_new_topics, self.test_dir)
        # Get a new list of questions that include the New_Topic questions without mangling the list for other tests
        expected_questions = dir_walk(self.test_dir, self.q_dirs, self.keyword)
        current_stats = {
            "exams_generated": 21,
            "knowledge_stats": {
                "exams_to_gen": 10,
                "time_allowed": 90,
                "delta": 0.2,
                "topics": {
                    "Algorithms": 0,
                    "Assembly": 5,
                    "C Programming": 15,
                    "Data Structures": 0,
                    "Networking": 5,
                    "Python": 15
                }
            },
            "performance_stats": {
                "exams_to_gen": 10,
                "time_allowed": 480,
                "delta": 0.36,
                "topics": {
                    "Assembly": 0,
                    "C Programming": 2,
                    "Networking": 1,
                    "Python": 2,
                    "Reverse Engineering": 0
                }
            }
        }
        exp_updated_stats = copy.deepcopy(current_stats)
        exp_updated_stats["knowledge_stats"]["topics"].update({"New Topic": 0})
        exp_updated_stats["performance_stats"]["topics"].update({"New Topic": 0})
        generate_exams.update_topics(generate_exams.get_cur_topics(expected_questions), current_stats)
        # Cleanup files that are only used for this test
        unit_test_helpers.remove_folder(os.path.join(self.test_dir, "knowledge", "New_Topic"))
        unit_test_helpers.remove_folder(os.path.join(self.test_dir, "performance", "New_Topic"))
        self.assertDictEqual(exp_updated_stats, current_stats, "Mismatch in updated topics")

    def test_check_old_topics(self):
        old_topic_stats = copy.deepcopy(self.expected_stats)
        old_topic_stats["knowledge_stats"]["topics"].update({"Old Topic": 4})
        old_topic_stats["performance_stats"]["topics"].update({"Old Topic": 0})
        cur_topics = generate_exams.get_cur_topics(self.expected_questions)
        topic = "[a-zA-Z_ -]+"
        number = "[0-9]+"
        expected_output = f"WARNING: Input '{topic}' topics configuration, with outdated topic '{topic}':'{number}', " \
            f"is not currently in use."
        with unittest.mock.patch('sys.stdout', new=io.StringIO()) as fake_out:
            generate_exams.check_old_topics(cur_topics, old_topic_stats)
            self.assertRegex(fake_out.getvalue().strip(), expected_output, "Old Topic not detected")

    def test_check_unused_category(self):
        topic = "[a-zA-Z_ -]+"
        number = "[0-9]+"
        expected_output = "WARNING: Input '{exam_t}' configuration is requesting '{number}' '{topic}' questions."
        expected_output = expected_output.format(exam_t=f"{self.q_dirs[1]}_stats", number=number, topic=topic)
        # Ensure something is set to zero for the test
        self.expected_stats["performance_stats"]["topics"].update({"Unused Topic": 0})
        with unittest.mock.patch('sys.stdout', new=io.StringIO()) as fake_out:
            generate_exams.check_unused_category(self.expected_stats, self.q_dirs[1])
            self.assertRegex(fake_out.getvalue().strip(), expected_output, "Topic with zero questions not detected")

    def test_copy_quest_files(self):
        generate_exams.copy_quest_files(self.test_dir, self.dst_dir)
        dst_files = unit_test_helpers.get_all_files(self.dst_dir)  # The list of files copied
        src_files = unit_test_helpers.get_all_files(self.test_dir)  # The list of files to copy
        self.assertListEqual(src_files, dst_files, "Mismatch in source & destination files")

    def test_copy_quest_files_err(self):
        # Test if an OSError is raised when file doesn't exist
        self.assertRaises(OSError, generate_exams.copy_quest_files, self.not_found_file, self.dst_dir)
        if not os.path.exists(self.dst_dir):
            os.makedirs(self.dst_dir)

    def test_generate_knowledge(self):
        # Change the number of questions per topic to a number that's available in the test environment
        self.set_gen_stats()
        # Setup the test environment
        eb = evalbuilder.EvalBuilder()
        eb.exams_generated = self.expected_stats["exams_generated"]
        eb.exams_to_generate = self.expected_stats["knowledge_stats"]["exams_to_gen"]
        eb.delta = self.expected_stats["knowledge_stats"]["delta"]
        eb.set_question_list(dir_walk(self.test_dir, self.q_dirs, self.keyword), self.mongodb.db)
        # Attempt to generate the requested exam(s)
        generate_exams.generate_knowledge(self.expected_stats, eb, self.mql_file_name, self.max_choices, self.out_dir,
                                          self.keyword, self.mongodb.db)
        # Should be a list of only exam CSV files
        exams = []
        for entry in os.listdir(self.out_dir):
            if os.path.isfile(os.path.join(self.out_dir, entry)):
                exams.append(entry)
        # Test to see if the requested number of exams were created
        self.assertEqual(eb.exams_to_generate, len(exams), "Mismatch in number of requested exams")

    def test_generate_performance(self):
        # Setup the test environment
        eb = evalbuilder.EvalBuilder()
        eb.exams_generated = self.expected_stats["exams_generated"]
        eb.exams_to_generate = self.expected_stats["performance_stats"]["exams_to_gen"]
        eb.delta = self.expected_stats["performance_stats"]["delta"]
        eb.set_question_list(dir_walk(self.test_dir, self.q_dirs, self.keyword), self.mongodb.db)
        # Attempt to generate the requested exam(s)
        generate_exams.generate_performance(self.expected_stats, eb, self.perf_exam_name, self.out_dir, self.exam_files)
        # Should be a list of only exam CSV files
        exam_paths = []
        for entry in os.listdir(self.out_dir):
            if os.path.isdir(os.path.join(self.out_dir, entry)):
                exam_paths.append(os.path.join(self.out_dir, entry))
        # Test to see if the requested number of exams were created
        self.assertEqual(eb.exams_to_generate, len(exam_paths), "Mismatch in number of requested exams")

    def test_main_knowledge(self):
        # This tests the main function within generate_exams.py
        self.tested_main = True
        # Setup the argparse arguments
        log_file = "generate_exams_log.json"
        args = self.InputArgs(self.q_dirs, self.keyword, log_file, self.mql_file_name, self.max_choices, self.out_dir, self.exam_files,
                              self.perf_exam_name, self.test_stats_file, "knowledge", self.db_name,
                              mttl_db_helpers.HOST, mttl_db_helpers.PORT)
        # Change the number of questions per topic to a number that's available in the test environment
        self.set_gen_stats()  # Set amounts in the self.expected_stats variable
        self.save_gen_stats(os.path.join(self.test_dir, self.test_stats_file))  # Save amounts back to disk for main()
        cwd = os.getcwd()
        print(f"\nCWD Before start: {os.path.join(os.path.basename(os.path.dirname(cwd)), os.path.basename(cwd))}\n")
        # Move into the expected start directory
        os.chdir(self.test_dir)
        cwd = os.getcwd()
        print(f"\nAfter changing CWD: {os.path.join(os.path.basename(os.path.dirname(cwd)), os.path.basename(cwd))}\n")
        print(f"{Fore.CYAN}Exam files available for unit testing:{Fore.RESET}\n")
        for path in dir_walk('.', self.q_dirs, self.keyword):
            print(path)
        generate_exams.main(args)
        kn_exams = []
        pf_exams = []
        for entry in os.listdir(self.out_dir):
            if os.path.isdir(os.path.join(self.out_dir, entry)):
                if "code_snippets" != entry:
                    pf_exams.append(os.path.join(self.out_dir, entry))
            elif os.path.isfile(os.path.join(self.out_dir, entry)):
                kn_exams.append(os.path.join(self.out_dir, entry))
        # Finally, check to see if the tests pass
        self.assertEqual(self.expected_stats["knowledge_stats"]["exams_to_gen"], len(kn_exams),
                         "Mismatch in number of requested knowledge exams")
        self.assertEqual(0, len(pf_exams), "Mismatch in number of requested performance exams")

    def test_main_performance(self):
        # This tests the main function within generate_exams.py
        self.tested_main = True
        # Setup the argparse arguments
        log_file = "generate_exams_log.json"
        args = self.InputArgs(self.q_dirs, self.keyword, log_file, self.mql_file_name, self.max_choices, self.out_dir, self.exam_files,
                              self.perf_exam_name, self.test_stats_file, "performance", self.db_name,
                              mttl_db_helpers.HOST, mttl_db_helpers.PORT)
        # Change the number of questions per topic to a number that's available in the test environment
        self.set_gen_stats()  # Set amounts in the self.expected_stats variable
        self.save_gen_stats(os.path.join(self.test_dir, self.test_stats_file))  # Save amounts back to disk for main()
        cwd = os.getcwd()
        print(f"\nCWD Before start: {os.path.join(os.path.basename(os.path.dirname(cwd)), os.path.basename(cwd))}\n")
        # Move into the expected start directory
        os.chdir(self.test_dir)
        cwd = os.getcwd()
        print(f"\nAfter changing CWD: {os.path.join(os.path.basename(os.path.dirname(cwd)), os.path.basename(cwd))}\n")
        print(f"{Fore.CYAN}Exam files available for unit testing:{Fore.RESET}\n")
        for path in dir_walk('.', self.q_dirs, self.keyword):
            print(path)
        generate_exams.main(args)
        kn_exams = []
        pf_exams = []
        for entry in os.listdir(self.out_dir):
            if os.path.isdir(os.path.join(self.out_dir, entry)):
                if "code_snippets" != entry:
                    pf_exams.append(os.path.join(self.out_dir, entry))
            elif os.path.isfile(os.path.join(self.out_dir, entry)):
                kn_exams.append(os.path.join(self.out_dir, entry))
        # Finally, check to see if the tests pass
        self.assertEqual(0, len(kn_exams), "Mismatch in number of requested knowledge exams")
        self.assertEqual(self.expected_stats["performance_stats"]["exams_to_gen"], len(pf_exams),
                         "Mismatch in number of requested performance exams")

    def test_main_both(self):
        # This tests the main function within generate_exams.py
        self.tested_main = True
        # Setup the argparse arguments
        log_file = "generate_exams_log.json"
        args = self.InputArgs(self.q_dirs, self.keyword, log_file, self.mql_file_name, self.max_choices, self.out_dir, self.exam_files,
                              self.perf_exam_name, self.test_stats_file, "both", self.db_name,
                              mttl_db_helpers.HOST, mttl_db_helpers.PORT)
        # Change the number of questions per topic to a number that's available in the test environment
        self.set_gen_stats()  # Set amounts in the self.expected_stats variable
        self.save_gen_stats(os.path.join(self.test_dir, self.test_stats_file))  # Save amounts back to disk for main()
        cwd = os.getcwd()
        print(f"\nCWD Before start: {os.path.join(os.path.basename(os.path.dirname(cwd)), os.path.basename(cwd))}\n")
        # Move into the expected start directory
        os.chdir(self.test_dir)
        cwd = os.getcwd()
        print(f"\nAfter changing CWD: {os.path.join(os.path.basename(os.path.dirname(cwd)), os.path.basename(cwd))}\n")
        print(f"{Fore.CYAN}Exam files available for unit testing:{Fore.RESET}\n")
        for path in dir_walk('.', self.q_dirs, self.keyword):
            print(path)
        generate_exams.main(args)
        kn_exams = []
        pf_exams = []
        for entry in os.listdir(self.out_dir):
            if os.path.isdir(os.path.join(self.out_dir, entry)):
                if "code_snippets" != entry:
                    pf_exams.append(os.path.join(self.out_dir, entry))
            elif os.path.isfile(os.path.join(self.out_dir, entry)):
                kn_exams.append(os.path.join(self.out_dir, entry))
        # Finally, check to see if the tests pass
        self.assertEqual(self.expected_stats["knowledge_stats"]["exams_to_gen"], len(kn_exams),
                         "Mismatch in number of requested knowledge exams")
        self.assertEqual(self.expected_stats["performance_stats"]["exams_to_gen"], len(pf_exams),
                         "Mismatch in number of requested performance exams")


if __name__ == "__main__":
    unittest.main()
