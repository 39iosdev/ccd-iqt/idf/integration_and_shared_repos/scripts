#!/usr/bin/python3

import os
import os.path
import json
import pickle
import argparse
from colorama import Fore
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import support

# If modifying these scopes, delete the file token.pickle.
SCOPES = [
          "https://www.googleapis.com/auth/drive",
          "https://www.googleapis.com/auth/drive.file"
          ]
TEST_BANK_FOLDER_ID = "1JhJEZW3xEr222MOyVXuz_u27nrkYMRRo"
CODE_SNIP_ROLE_FLDR_IDS = {
    "basic-dev": "1IY7ikrul3RhJYt9FodQNo33rrM0UTCba",
    "basic-po": "1SwyNDj7ylylv3Wz769ezN4pCNVjq2pkB"
}
CODE_SNIPPET_FOLDER_ID = "1LVDxanEo7E5Wmb4wmb58gYh532Rmy1IM"
COURSE_SURVEY_FOLDER_ID = "1sFHP487VDPpmm12r1bl8GewvAw7SwdWG"
MQL_BASE_FILE_NAME = "MQL.csv"
TRNG_SURVEY_BASE_NAME = "trng_surveys.csv"
IMAGE_EXT = "png"
SRC_SNIP_DIR = "code_snippets"

CLIENT_SECRET_DESC = "This is the client secret needed for Google Drive access."
CS_DIR_ID_DESC = "This is the Google Drive ID of the folder containing all the course survey data"
CSV_DESC = "This is the name of the input CSV file(S) to push to Google Drive and get converted to Google Sheets."
DESCRIPTION = "This will upload necessary files for the Google Form Creator, used to generate exams."
DST_SNIP_DIR_IDS_MAP_DESC = "This is a dictionary representing the Google Drive ID for each folder containing all the" \
                            " work-role/specialization specific image files. The map should have the following " \
                            "format: {\"nameof_repo\": \"Google Drive folder ID\", ...}."
DEF_DST_SNIP_DIR_ID_DESC = "This is the Google Drive ID of the folder containing all the image files."
IMAGE_EXT_DESC = "This is the image file extension."
SRC_SNIP_DIR_DESC = "This is the folder containing all images, needed for the questions in the MQL, to be uploaded."
UPLOAD_SURVEY_DESC = "When specified, indicates the files to upload are training survey data."
TB_DIR_ID_DESC = "This is the Google Drive ID of the folder containing all the test bank data."


def delete_old_data(service, csv_file: str):
    page_token = None
    while True:
        name = os.path.splitext(csv_file)[0]
        print(f"name = '{name}'")
        results = service.files().list(q=f"name = '{name}'",
                                       spaces='drive',
                                       fields='nextPageToken, files(id, name)',
                                       pageToken=page_token).execute()
        if len(results['files']) < 1:
            print(f"NO FILES FOUND: Please check the google drive to ensure all outdated '{name}' files are deleted.")
        for file in results.get('files', []):
            if name in file['name']:
                try:
                    service.files().delete(fileId=file.get('id')).execute()
                    print(f"Deleting file: {file.get('name')} {file.get('id')}")
                except Exception as e:
                    print(e)
                    return 1
        page_token = results.get('nextPageToken', None)
        if page_token is None:
            break


def delete_snippets(service, dst_snip_dir_id: str, image_ext: str = IMAGE_EXT):
    page_token = None
    while True:
        results = service.files().list(q=f"'{dst_snip_dir_id}' in parents and mimeType='image/{image_ext}'",
                                       spaces='drive',
                                       fields='nextPageToken, files(id, name)',
                                       pageToken=page_token).execute()
        for file in results.get('files', []):
            # Process change
            print(f"Deleting file: {file.get('name')} {file.get('id')}")
            try:
                service.files().delete(fileId=file.get('id')).execute()
            except Exception as e:
                print(e)
                return 1
        page_token = results.get('nextPageToken', None)
        if page_token is None:
            break


def upload(service, root, file, subject, google_folder, image_ext: str = IMAGE_EXT):
    file_metadata = {'name': file, 'mimeType': f"image/{image_ext}",
                     'parents': [google_folder]}
    media = MediaFileUpload(os.path.join(root, file))
    file = service.files().create(body=file_metadata, media_body=media, fields='id').execute()
    print(f"Uploaded {file} code snippet for {subject}")
    return 0


def find_path(file):
    for root, dirs, files in os.walk('.'):
        for name in files:
            if name == file:
                return os.path.abspath(os.path.join(root, name))


def authorize():
    global args
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    token_path = find_path('token.pickle')
    if os.path.exists(token_path):
        with open(token_path, 'rb') as token:
            creds = pickle.load(token)
    else:
        print("User authentication token missing")
        return 1
    # Insert the removed part of the creds
    try:
        creds._client_secret = args.client_secret
    except Exception as e:
        print("Please contact the 90 COS/CYT Evaluations Element Staff to get the updated credentials."
              f"The actual error is {e}")
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            ### Run this locally if you need a new credentials.json
            # It will redirect you to a login page and provde you with a new credentials.json
            try:
                creds_path = find_path('credentials.json')
                flow = InstalledAppFlow.from_client_secrets_file(
                    creds_path, SCOPES)
                creds = flow.run_local_server(port=0)
            except Exception as e:
                print("The google credentials file is not found, "
                      "please contact the 90 COS/CYT Evaluations Element Staff to get the updated credentials."
                      f"The actual error is {e}")
            #######################################################
        # Save the credentials for the next run
        with open(token_path, 'wb') as token:
            pickle.dump(creds, token)

    return creds


def upload_csv_data(service, csv_file: str, dst_dir_id: str, keep_csv_format: bool):
    delete_old_data(service, csv_file)
    # Find path to the Master_Question_List to be pushed
    csv_path = find_path(csv_file)
    if csv_path is None or csv_path == "":
        print("Unable to find the source CSV file. Please check the path and try again.")
        return 1
    # Upload the requested data
    try:
        if keep_csv_format:
            file_metadata = {'name': os.path.splitext(csv_file)[0], 'mimeType': 'text/csv', 'parents': [dst_dir_id]}
        else:
            file_metadata = {'name': os.path.splitext(csv_file)[0],
                             'mimeType': 'application/vnd.google-apps.spreadsheet',
                             'parents': [dst_dir_id]}
        media = MediaFileUpload(csv_path)
        file = service.files().create(body=file_metadata, media_body=media, fields='id').execute()
        print(f"File ID: {file.get('id')}")
        print(f"Uploaded {os.path.basename(csv_path)} successfully")
    except Exception as e:
        print(e)
        return 1


def upload_snippets(service, src_snip_dir: str, dst_snip_dir_id: str, image_ext: str):
    try:
        delete_snippets(service, dst_snip_dir_id)
        files_uploaded = 0
        # Look through the local copy of code snippets for .pngs
        for root, dirs, files in os.walk(src_snip_dir):
            subject = root.split("/")[-1]
            if "" == subject:
                subject = "Unknown Subject"
            for file in files:
                if file.endswith(".{ext}".format(ext=image_ext)):
                    upload(service, root, file, subject, dst_snip_dir_id)
                    files_uploaded += 1
        if files_uploaded > 0:
            print(f"Uploaded {files_uploaded} code_snippet(s)")
        else:
            print("No snippets to upload.")
    except Exception as e:
        print(e)
        return 1


def main():
    # Get crendentials
    global args
    creds = authorize()
    if args.csv_file_name == MQL_BASE_FILE_NAME and not args.upload_survey:
        args.csv_file_name = f"{os.path.basename(os.getcwd())}_{MQL_BASE_FILE_NAME}"
    if args.csv_file_name == TRNG_SURVEY_BASE_NAME and args.upload_survey:
        args.csv_file_name = f"{os.path.basename(os.getcwd())}_{TRNG_SURVEY_BASE_NAME}"

    if args.upload_survey:
        dst_dir_id = args.cs_dir_id
    else:
        dst_dir_id = args.tb_dir_id

    # Create the service to upload
    service = build('drive', 'v3', credentials=creds)

    # Upload the MQL
    upload_csv_data(service, args.csv_file_name, dst_dir_id, args.upload_survey)

    if not args.upload_survey:
        # Start with the default code snippet folder
        dst_snip_dir_id = args.def_dst_snip_dir_id
        # Determine the name of the current repository
        repo_name = os.path.basename(os.getcwd())
        # Force lowercase keys for repo_names
        args.dst_snip_dir_ids_map = dict((key.lower(), value) for key, value in args.dst_snip_dir_ids_map.items())
        # Determine the snippet folder to use
        if repo_name.lower() in args.dst_snip_dir_ids_map:
            dst_snip_dir_id = args.dst_snip_dir_ids_map[repo_name.lower()]
        else:
            print(f"{Fore.YELLOW}Warning:{Fore.CYAN} Unable to determine '{repo_name}'s' folder ID.{Fore.RESET} "
                  f"Using the default destination code snippet folder instead.")
        # Upload the snippets
        upload_snippets(service, src_snip_dir=args.src_snip_dir,
                        dst_snip_dir_id=dst_snip_dir_id,
                        image_ext=args.image_ext)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument("client_secret", type=str, help=CLIENT_SECRET_DESC)
    parser.add_argument("-C", "--cs_dir_id", type=str, default=COURSE_SURVEY_FOLDER_ID, help=CS_DIR_ID_DESC)
    parser.add_argument("-c", "--csv_file_name", type=str, default=MQL_BASE_FILE_NAME, help=CSV_DESC)
    parser.add_argument("-D", "--dst_snip_dir_ids_map", type=json.loads, default=CODE_SNIP_ROLE_FLDR_IDS,
                        help=DST_SNIP_DIR_IDS_MAP_DESC)
    parser.add_argument("-d", "--def_dst_snip_dir_id", type=str, default=CODE_SNIPPET_FOLDER_ID,
                        help=DEF_DST_SNIP_DIR_ID_DESC)
    parser.add_argument("-i", "--image_ext", type=str, default=IMAGE_EXT, help=IMAGE_EXT_DESC)
    parser.add_argument("-s", "--src_snip_dir", type=str, default=SRC_SNIP_DIR, help=SRC_SNIP_DIR_DESC)
    parser.add_argument("-t", "--tb_dir_id", type=str, default=TEST_BANK_FOLDER_ID, help=TB_DIR_ID_DESC)
    parser.add_argument("-u", "--upload_survey", action="store_true", help=UPLOAD_SURVEY_DESC)
    args = parser.parse_args()
    authorize()
    main()
