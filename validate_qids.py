#!/usr/bin/python3

import os
import json
import sys
from colorama import Fore
import argparse
from lib.errors import ErrorCode
from lib import file_list_generator
from lib.default_strings import param_root_dir_desc, param_root_dir_default, param_question_dir_desc, \
    opt_param_changed_desc, keyword_default, keyword_desc, question_type_key, error_str, warning_str, id_key

avoid_dirs_default = ['.git', '.vscode', 'scripts', 'mttl']
avoid_dirs_desc = f"A list of directories to ignore. The default is '{avoid_dirs_default}'."
description = "This collects a list of all question IDs and verifies only one question exists per ID."
log_file_default = "ValidateQidsReport.json"
log_file_desc = f"The name of the report file; the default is '{log_file_default}'."
out_dir_default = "."
out_dir_desc = "The directory to store the resulting validation report, relative to root_dir. " \
               f"The default is '{out_dir_default}'."

msg_er_color = f"{Fore.RED + os.path.basename(__file__)} {error_str + Fore.RESET}:"
msg_er = f"{os.path.basename(__file__)} {error_str}:"
msg_wn_color = f"{Fore.YELLOW + os.path.basename(__file__)} {warning_str + Fore.RESET}:"
msg_wn = f"{os.path.basename(__file__)} {warning_str}:"
msg_gd_color = f"{Fore.GREEN + os.path.basename(__file__) + Fore.RESET}:"
msg_gd = f"{os.path.basename(__file__)}:"


def get_id_map(paths: list) -> dict:
    '''
    Purpose: This collects a map of all question IDs with a list of questions using each ID.
    :param paths: A list of strings representing the path to each question.
    :return: id_map - A dictionary representing the question ID map.
    '''
    id_map = {}
    for path in paths:
        with open(path, "r") as q_fp:
            question = json.load(q_fp)
        if question_type_key not in question:
            # This is an older file that uses a one-way hash for the ID or it's not a question file so skip
            continue
        _id = question[id_key]
        name = os.path.relpath(path, os.getcwd()).replace("\\", "/")
        if _id not in id_map:
            id_map[_id] = []
        id_map[_id].append(name)
    return id_map


def check_for_duplicate_ids(id_map: dict) -> dict:
    '''
    Purpose: This builds a dictionary of all question IDs used more than once.
    :param id_map: A dictionary representing the total collection of question ID maps to questions.
    :return: duplicate_ids - A dictionary of all duplicate question IDs; it's empty when no duplicates exist.
    '''
    duplicate_ids = {}
    for id, qnames in id_map.items():
        if len(qnames) > 1:
            duplicate_ids.update({id: qnames})
    return duplicate_ids


def main(args: argparse.Namespace) -> dict:
    '''
    Purpose: This function verifies each question ID is used only once.
    :return: this_status - a dictionary with keys representing error codes according to class Error or the exception,
             when present.
    '''
    this_status = {}

    if args.question_dirs is None:
        args.question_dirs = ["."]

    if args.changed:
        # In case only changed files are requested
        paths = file_list_generator.changed_paths(repo_base_dir=args.root_dir, keyword=args.filter,
                                                  avoid_list=args.avoid_dirs)
    else:
        # In case all files are requested
        paths = file_list_generator.dir_walk(root_dir=args.root_dir, json_dirs=args.question_dirs,
                                             keyword=args.filter, avoid_list=args.avoid_dirs)
    if len(paths) == 0:
        # In case the list is empty
        msg = "{0} Received empty file list."
        print(msg.format(msg_wn_color))
        this_status[ErrorCode.EMPTY_LIST.value] = [msg.format(msg_wn)]
        return this_status
    out_dir = args.out_dir
    if not os.path.exists(out_dir):
        # Make sure the output directory exists to prevent write-to-disk errors
        os.makedirs(out_dir)

    id_map = get_id_map(paths)  # Build the map of question IDs
    duplicate_ids = check_for_duplicate_ids(id_map)

    if len(duplicate_ids) > 0:
        # In case there are duplicates
        this_status[ErrorCode.DUPLICATE_QUESTION_IDS.value] = [duplicate_ids]

    try:
        if len(this_status) == 0:
            # In case this_status has no errors or warnings
            msg = "All question IDs are validated."
            print(Fore.GREEN + msg + Fore.RESET)
            if ErrorCode.SUCCESS.value not in this_status:
                this_status[ErrorCode.SUCCESS.value] = []
            this_status[ErrorCode.SUCCESS.value].append(msg)
        elif len(this_status) == 1 and ErrorCode.SUCCESS.value in this_status:
            # In case there are only warnings
            msg = "All question IDs appear to be valid; however, warnings are present."
            print(Fore.YELLOW + msg + Fore.RESET)
            print(f"{status}")
            if ErrorCode.SUCCESS.value not in this_status:
                this_status[ErrorCode.SUCCESS.value] = []
            this_status[ErrorCode.SUCCESS.value].append(msg)
        else:
            msg = "{0} {1}: Duplicate question IDs are present and require manual deconfliction."
            print(msg.format(msg_er_color, Fore.RED + ErrorCode.DUPLICATE_QUESTION_IDS.name + Fore.RESET))
            for dup_id, qnames in duplicate_ids.items():
                print(f"{Fore.CYAN + dup_id + Fore.RESET}")
                for qname in qnames:
                    print(f"\t{Fore.YELLOW + qname + Fore.RESET}")
            if ErrorCode.DUPLICATE_QUESTION_IDS.value not in this_status:
                this_status[ErrorCode.DUPLICATE_QUESTION_IDS.value] = []
            this_status[ErrorCode.DUPLICATE_QUESTION_IDS.value].insert(0, msg.format(
                msg_er,
                ErrorCode.DUPLICATE_QUESTION_IDS.name
            ))
        with open(os.path.join(out_dir, args.log_file), "w") as log_fp:
            json.dump(this_status, log_fp, indent=4)
    except (IOError, json.JSONDecodeError, ValueError, TypeError, OverflowError) as err:
        msg = "{0} Could not create the json report file; {1}"
        print(msg.format(msg_er_color, err))
        if ErrorCode.JSON_DECODE_ERROR.value not in this_status:
            this_status[ErrorCode.JSON_DECODE_ERROR.value] = []
        this_status[ErrorCode.JSON_DECODE_ERROR.value].append(msg.format(msg_er, err))
        return this_status
    return this_status


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("-a", "--avoid_dirs", nargs='+', type=str, default=avoid_dirs_default, help=avoid_dirs_desc)
    parser.add_argument("-c", "--changed", action="store_true", help=opt_param_changed_desc)
    parser.add_argument("-f", "--filter", type=str, default=keyword_default, help=keyword_desc)
    parser.add_argument("-l", "--log_file", type=str, default=log_file_default, help=log_file_desc)
    parser.add_argument("-o", "--out_dir", type=str, default=out_dir_default, help=out_dir_desc)
    parser.add_argument("-q", "--question_dirs", nargs='+', type=str, default=None, help=param_question_dir_desc)
    parser.add_argument("-R", "--root_dir", type=str, default=param_root_dir_default, help=param_root_dir_desc)
    params = parser.parse_args()
    status = main(params)
    if not (len(status) == 1 and ErrorCode.SUCCESS.value in status):
        # In case status has errors, prevent returning zero
        if ErrorCode.SUCCESS.value in status:
            # For cases where ErrorCode.SUCCESS.value isn't present
            del status[ErrorCode.SUCCESS.value]
    sys.exit(list(status.keys())[0])
