var courseFolderName = "CourseSurveys";
var headers = {};
var rellinkKey = "rel-link path";
var moduleKey = "module";
var topicKey = "topic";
var subjectKey = "subject";
var workroleKey = "work-role";
var ksatIdKey = "ksat id";
var descriptionKey = "description";
var v0Key = "0"
var v1Key = "i";
var v2Key = "ii";
var v3Key = "iii";
var v4Key = "iv";
var otherKey = "other";

function capitalize(string) {
  // This capitalizes the first character in the input string
  // Parameter: string - The string to have its first character capitalized
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function headerParser(csvData){
  // Searches the csvData and finds the indexes of each column header
  // Parameter: csvData array of strings,
  // Returns: Dictionary of headers and their associated indexes
  var indexDict = new Map();
  header = csvData[0];
  for(var i = 0; i< csvData[0].length; i++){
    indexDict[(header[i]).toLowerCase()] = i;
  }
  return indexDict;
}

function findSpan(csvData, searchIndex){
  // Searches the csvData and finds when the content of an index changes.
  //    It is used to find when a topic/subject changes.
  // Parameter: csvData array of strings, searchIndex index of array to seperate span
  // Returns: Array of ints for the span of each topic
  var spanArray = [];
  var originalTopic = (csvData[0][searchIndex]).toLowerCase();
  for(var i = 0; i < csvData.length; i++){
    var tempTopic = (csvData[i][searchIndex]).toLowerCase();
    if (originalTopic !== tempTopic){
      spanArray.push(i);
      originalTopic = (csvData[i][searchIndex]).toLowerCase();
    }
  }
  return spanArray;
}

function getCSVsInFolder(folderName){
  // Searches a directory and finds all csvs
  // Parameter: folderName string  
  // Returns: Array of string file names
  var folder = DriveApp.getFoldersByName(folderName);
  var files = folder.next().getFilesByType('text/csv');
  var fileArray = [];
  while (files.hasNext()){
    var file = files.next();
    fileArray.push(file.getName());
  }
  return fileArray;
}

function setCSVsInFolder(){
  // Generates a drop down of csv files in the UI sheet.
  var sheet = SpreadsheetApp.getActiveSheet();
  var folder = DriveApp.getFoldersByName(courseFolderName);
  var files = folder.next().getFilesByType('text/csv');
  var fileArray = [];
  fileArray.push("All");
  while (files.hasNext()){
    var file = files.next();
    fileArray.push(file.getName());
  }
  var dropDownCell = sheet.getRange('B3');
  dropDownCell.clearDataValidations().clearContent();
  var rule = SpreadsheetApp.newDataValidation().requireValueInList(fileArray, true).build();
  dropDownCell.setDataValidation(rule);
  dropDownCell.setValue(fileArray[0]);
}

function copyFile(fileOriginal, fileNew){
  // Make a copy of a file and return the new copy with the new filename
  // Parameter: fileOriginal string, fileNew string  
  // Returns: Class File
  var file = DriveApp.getFilesByName(fileOriginal);
  var fileId = file.next().getId();
  var form = DriveApp.getFileById(fileId).makeCopy();
  form.setName(fileNew);
  return form;
}

function checkForFile(filename){
  // Checks for a file in the entire google drive.
  // Parameter: Class File Returns: boolean
  return DriveApp.getFilesByName(filename).hasNext();
}

function checkForFolder(foldername){
  // Checks for a folder in the entire google drive.
  // Parameter: Class File Returns: boolean
  return DriveApp.getFoldersByName(foldername).hasNext();
}

function moveFiles(item, targetFolderId) {
  // Moves a file into a specific folder
  // Parameter: Class File Returns: boolean
  var id = item.getId();
  var file = DriveApp.getFileById(id);
  file.getParents().next().removeFile(file);
  DriveApp.getFolderById(targetFolderId).addFile(file);
}

function folderCreator(rootFolderName, subFolderName){
  // Creates folders inside of a folder
  // Parameter: rootFolderName string, subFolderName string
  var folderID = DriveApp.getFoldersByName(rootFolderName).next().getId();
  var parentFolder = DriveApp.getFolderById(folderID);
  var subFolders = parentFolder.getFolders();
  var doesntExists = true;
  var newFolder = '';
  
  // Check if folder already exists.
  while(subFolders.hasNext()){
    var folder = subFolders.next();
  };
  
  //If the name doesn't exists, then create a new folder
  if(doesntExists = true){
    newFolder = parentFolder.createFolder(subFolderName);
  };
}

function makeDirStructure(workrole, module){
  // Make the required directory structure <CouseSurveys>/<workrole>/<module>
  // Parameter: workrole string, module string
  
  // Make <CouseSurveys>/<workrole> path if it doesnt exist
    if (checkForFolder(workrole) === false){
      folderCreator(courseFolderName, workrole);
    }
    // Make <workrole>/<module> path if it doesnt exist
    if (checkForFolder(module) === false){
      folderCreator(workrole, module);
    }
}

function openAndReadCSV(fileName) {
  // Opens and parse CSV.
  // Parameter: String(name of csv) 
  // Returns: Dictionary of headers(key) and indexes(value), Array ksats data
  var file = DriveApp.getFilesByName(fileName).next();
  var csvString = file.getBlob().getDataAsString();
  csvString = csvString.replace(/,{2}/g, ', ,');
  var csvData = Utilities.parseCsv(csvString);
  // Assign headers using the header value as a key and the index as a value 
  headers = headerParser(csvData);
  // Get the KSAT info, this will be an array of arrays excluding the headers
  var contents = csvData.slice(1);
  var headersAndContent = [headers, contents];
  return headersAndContent;
}

function addPreviousExperienceQuestionToForm(form){
  // Adds the previous experience question of the form
  // Parameter: form Class Form
  var previousExperience = "What kind of experience did you have before starting this course?";
  form.addParagraphTextItem()
  .setTitle(previousExperience)
  .setRequired(false);
}

function addPreviousKsatExperienceQuestionToForm(form, ksatId){
  // Adds the previous KSAT experience question of the form
  // Parameter: form Class Form
  // Parameter: ksatId - A string representing the KSAT ID in which this question applies
  var previousExperience = "What was your experience with '" + ksatId + "' before starting the course?";
  var lowerLabel = "None";
  var upperLabel = "Expert";
  form.addScaleItem()
  .setTitle(previousExperience)
  .setBounds(0, 5)
  .setLabels(lowerLabel, upperLabel);
}

function addKsatCommentQuestionToForm(form, ksatId){
  // Adds the KSAT comments question of the form
  // Parameter: form Class Form
  // Parameter: ksatId - A string representing the KSAT ID in which this question applies
  var comments = "Please enter any additional comments you may have regarding your training in '" + ksatId + "'.";
  form.addParagraphTextItem()
  .setTitle(comments)
  .setRequired(false);
}

function addContactQuestionToForm(form){
  // Adds the contact information
  // Parameter: form Class Form
  form.addTextItem()
  .setTitle("Please enter your name:");
  
  form.addTextItem()
  .setTitle("Please enter your email:");
}

function addCourseStartDate(form){
  // Adds the date time to the form
  // Parameter: form Class Form
  form.addDateItem().setTitle('Date the course/class started.');
}

function addKSATItemsToForm(form, ksats) {
  // Adds the date time to the form
  // Parameter: form Class Form, array of ksats
  var ksatPages = [];
  for(var i = 0; i < ksats.length; i++){
    var ksat = ksats[i][headers[ksatIdKey]];
    var description = ksats[i][headers[descriptionKey]];
    var otherVerbage = ksats[i][headers[otherKey]];
    // Check if the otherVerbage option is blank, else add it to the slice.
    if (otherVerbage === ""){
      var numOfQuestionVerbage = headers[otherKey] - headers[v0Key];
      var questionVerbage = ksats[i].splice(headers[v0Key], numOfQuestionVerbage);
    } else {
      var numOfQuestionVerbage = headers[otherKey] - headers[v0Key];
      var questionVerbage = ksats[i].splice(headers[v0Key], numOfQuestionVerbage+1);
    }
  
    var ksatPage = form.addPageBreakItem().setTitle(ksat + ' Page');
    ksatPages.push(ksatPage);
    
    form.addMultipleChoiceItem()
    .setTitle(ksat + ' ' + description)
    .setChoiceValues(questionVerbage)
    .showOtherOption(false);
    // Add the other KSAT specific questions to the form
    addPreviousKsatExperienceQuestionToForm(form, ksat);
    addKsatCommentQuestionToForm(form, ksat);
  }
  return ksatPages;
}

function addOptionsToFatigueQuestion(multipleChoiceItem, pageBreakForComments){
  // Adds the options for the fatigue feedback question
  // Parameter: multipleChoiceItem Class MultipleChoiceItem
  // Parameter: pageBreakForComments Class PageBreakItem
  multipleChoiceItem.setTitle("Did you feel this survey was manageable?")
      .setChoices([
        multipleChoiceItem.createChoice("Yes", FormApp.PageNavigationType.SUBMIT),
        multipleChoiceItem.createChoice("No", FormApp.PageNavigationType.CONTINUE),
        multipleChoiceItem.createChoice("Provide Comments", pageBreakForComments)
      ]);
}

function addReasoningQuestionToForm(form){
  // Adds the reasoning question for choosing 'No' to the fatigue question
  // Parameter: form Class Form
  form.addTextItem()
      .setTitle("Please explain why this survey wasn't manageable.")
      .setRequired(true);
}

function addCommentsQuestionToForm(form, title){
  // Adds a way to provide comments to the fatique question
  // Parameter: form Class Form
  form.addTextItem()
      .setTitle(title);
}

function createForm(form, formTitle, fileName, ksats) {
  // Checks if the form passed in is the template and fills out the form
  // Parameter: from Class Form, formTitle string(title of the form), fileName string, ksats array of strings  
  // Returns: Array of Class Form and the Class Spreadsheet
  
  var description = "This form will be used to assist surveyors in evaluating whether the course you are attending meets the 90th's " + 
    "requirements in the proficiencies for Knowledge, Skills, Abilities & Tasks (KSAT) items." +
    "\n\nPlease answer the following questions on what you were taught in the course; please don't consider any previous experience." +
    "\n\nQuestions using a scale from 0 - 5 are defined as follows:\n" +
    "\t0: None, 1: Little, 2: Below Average, 3: Average, 4: Above Average, 5: Expert";
    
  // If form is null, the template file is gone, continue creating but with out using the template.
  if (form === undefined){
    var form = FormApp
    .create(fileName)
    .setTitle(formTitle)
    .setDescription(description)
    .setProgressBar(true)
    .setPublishingSummary(true)
    .setShowLinkToRespondAgain(true)
    .setAllowResponseEdits(true);
  }
  
  form = FormApp.openById(form.getId())
    
  form.setDescription(description)
  .setTitle(formTitle)
  .setProgressBar(true)
  .setPublishingSummary(true)
  .setShowLinkToRespondAgain(true)
  .setAllowResponseEdits(true);
  
  
  // Create the response sheet
  var responseFileName = fileName + "_response";
  if (checkForFile(responseFileName) === false){
    var responseSheet = SpreadsheetApp.create(responseFileName);
    form.setDestination(FormApp.DestinationType.SPREADSHEET, responseSheet.getId());
  } else {
    console.log("File " + responseFileName + " exists.")
  }
  
  // Add the "course date started" question
  addCourseStartDate(form);
  
  // Add the "ask name and email" question
  addContactQuestionToForm(form);
  
  // Add the "ask experience" question
  addPreviousExperienceQuestionToForm(form);
  
  // Add all the KSATs survey questions to the form; the variable is a list and could facilitate jumping to other pages
  var ksatPages = addKSATItemsToForm(form, ksats);
  
  // Ask about survey fatigue
  var surveyFeedbackPage = form.addPageBreakItem().setTitle('Survey Feedback');
  var feedbackQuestion = form.addMultipleChoiceItem();
  
  // If 'No' was chosen to the Fatigue question then this allows providing an explanation
  var reasoningPage = form.addPageBreakItem().setTitle("Further Details");
  addReasoningQuestionToForm(form);
  
  // In case other comments are desired
  var commentsPage = form.addPageBreakItem().setTitle("Provide Comments");
  addCommentsQuestionToForm(form, "Please enter your comments about this survey's manageability");
  
  // This is saying, when finished with the page before the commentsPage, go to Submit
  commentsPage.setGoToPage(FormApp.PageNavigationType.SUBMIT);
  
  addOptionsToFatigueQuestion(feedbackQuestion, commentsPage);
    
  var formSheet = [form, responseSheet];
  return formSheet;
}

function generateSurveys(csvData, scopeSpanArray, module, topic){
  // Given the csv data, this will create the files neccesary files for each topic/subject
  // Parameter: csvData array of strings, topicSpanArray array of ints to designate the span of each topic/subject, 
  // module string, topic string
  
  //Because there are different numerous topics, I need to split by topic
  var topicCounter = 0;
  var curTopic = topic;
  for(var i = 0; i < csvData.length;){
    
    if (curTopic !== (csvData[i][headers[topicKey]]).toLowerCase()){
      topic = curTopic = (csvData[i][headers[topicKey]]).toLowerCase();
    }
    
    var theme_template = "theme_template";
    var fileName = module + "_" + topic;
    var title = "Course Survey for the " + capitalize(module) + " module's " + capitalize(topic) + " topic";
    var form;
    
    // If the survey exists, dont override it.
    // This will also prevent creating the same topic multiple times.
    //Need a new file
    if (checkForFile(fileName) === false){
      // Copy the template solely to use the theme.
      if (checkForFile(theme_template) === true){
        form = copyFile(theme_template, fileName);
      }
    }
    var topicsKsats = csvData.slice(i, scopeSpanArray[topicCounter]);
    i = scopeSpanArray[topicCounter];
    topicCounter++;
    // Fill in the form
    var formAndSheetName = createForm(form, title, fileName, topicsKsats);
    // Move the form and response sheet to the correct folder
    var courseFolderId = DriveApp.getFoldersByName(module).next().getId();
    moveFiles(formAndSheetName[0], courseFolderId);
    moveFiles(formAndSheetName[1], courseFolderId);
  }
}

function getKsatRows(csvData, customKsatList){
  // This generates a new csvData structure containing only the desired custom KSATs
  // Parameter: csvData - A multidemensional array representing the survey data within a CSV file, minus the header row
  // Parameter: customKsatList - An array representing the list of custom KSATs to be included in the survey
  // Return: newCsvData - A multidemensional array with the wanted survey data
  var newCsvData = [];
  if(customKsatList.length === 0){
    // Since there's nothing to do
    return newCsvData;
  }
  csvData.forEach(function(row){
    var ksat = row[headers[ksatIdKey]];
    if(customKsatList.includes(ksat)){
      newCsvData.push(row);
    }
  });
  return newCsvData;
}

function getRandomInt(min, max) {
  // This generates a random integer between the range of min and max. Code taken from w3schools.com tutorial.
  // Parameter: min - A number representing the start of the range, this value is inclusive
  // Parameter: max - A number representing the end of the range, this value is exclusive
  // Return: number - The randomly chosen integer from min to max.
  return Math.floor(Math.random() * (max - min) ) + min;
}

function getTopicMap(csvData, topicSpanArray){
  // This creates an object capable of storing chosen index values for each topic in the survey.
  // Parameter: csvData - A multidemensional array representing the survey data within a CSV file, minus the header row
  // Parameter: topicSpanArray - An array of numbers representing the first index of the next topic; the first topic is assumed to be at index 0
  // Return: chosenIndexes - An object with topic name keys that each have an empty array value; each array will store the chosen KSAT indexes per topic
  var chosenIndexes = {};
  topicSpanArray.forEach(function(nextTopicIndex){
    // Establish separate arrays for each topic to determine if all options have been chosen for said topics
    if(nextTopicIndex - 1 >= 0){
      // Avoid creating nagative indexes
      --nextTopicIndex;
    }
    var topic = csvData[nextTopicIndex][headers[topicKey]];
    chosenIndexes[topic] = [];
  });
  return chosenIndexes;
}

function addCustomKsatIndexes(csvData, customKsatList, chosenIndexes){
  // This adds the custom KSAT row indexes to the chosenIndexes structure; this assists with putting both random and custom KSAT rows in topic order.
  // Parameter: csvData - A multidemensional array representing the survey data within a CSV file, minus the header row
  // Parameter: customKsatList - An array representing the list of custom KSATs to be included in the survey
  // Parameter: chosenIndexes - An object with topic name keys that each have an array value; each array will store the chosen KSAT indexes per topic
  if(customKsatList.length === 0){
    // Since there's nothing to do
    return;
  }
  csvData.forEach(function(row, index){
    var ksat = row[headers[ksatIdKey]];
    var topic = row[headers[topicKey]];
    if(customKsatList.includes(ksat)){
      chosenIndexes[topic].push(index);
    }
  });
}

function addAllShortListTopicKsats(csvData, numRandKsats, topicSpanArray, chosenIndexes, addToCustomList){
  // When a topic has equal to or less than the requested amount of KSATs unpicked, this adds all remaining KSAT row indexes to the chosenIndexes structure; 
  // this assists with reducing the amount of topics needing the random index picker to run.
  // Parameter: csvData - A multidemensional array representing the survey data within a CSV file, minus the header row
  // Parameter: numRandKsats - A number representing the amount of random KSAT IDs to include in the survey
  //                           May be expressed as total per survey or in addition to a custom KSAT list
  // Parameter: topicSpanArray - An array of numbers representing the first index of the next topic; the first topic is assumed to be at index 0
  // Parameter: chosenIndexes - An object with topic name keys that each have an array value; each array will store the chosen KSAT indexes per topic
  // Parameter: addToCustomList - A boolean determining if the picked random KSATs should be in addition to the custom list or the minimum total per survey.
  //                              Note, only the custom KSAT list can cause the total per survey to be greater than this number.
  var startIndex = 0
  topicSpanArray.forEach(function(nextTopicIndex){
    var numNeeded = numRandKsats;  // Preserve requested number of Random KSAT questions
    var topicSize = nextTopicIndex - startIndex;  // The number of available KSATs in this topic
    var thisTopicLastIndex = nextTopicIndex - 1;
    if(thisTopicLastIndex < 0){
      // Avoid creating nagative indexes
      thisTopicLastIndex = 0;
    }
    var topic = csvData[thisTopicLastIndex][headers[topicKey]];
    if(!addToCustomList){
      // In case the resulting number of KSAT questions should equal the amount requested in numRandKsats
      if(numNeeded - chosenIndexes[topic].length >= 0){
        // Avoid numbers less than zero
        numNeeded -= chosenIndexes[topic].length;
      } else {
        numNeeded = 0;
      }
    }
    if(topicSize - chosenIndexes[topic].length <= numNeeded){
      // Since we know all remaining KSATs in this topic will be chosen
      for(var i = startIndex; i < nextTopicIndex; i++){
        if(!chosenIndexes[topic].includes(i)){
          // In case this KSAT isn't in the list, add it to its topic list
          chosenIndexes[topic].push(i);
        }
      }
    }
    startIndex = nextTopicIndex;
  });
}

function addRandKsatIndexes(csvData, numRandKsats, topicSpanArray, chosenIndexes, addToCustomList){
  // This fills each array in the chosenIndexes object with the randomly chosen KSATs per topic. Each topic will either have the total number available
  // or the number requested, whichever is less.
  // Parameter: csvData - A multidemensional array representing the survey data within a CSV file, minus the header row
  // Parameter: numRandKsats - A number representing the amount of random KSAT IDs to include in the survey
  //                           May be expressed as total per survey or in addition to a custom KSAT list
  // Parameter: topicSpanArray - An array of numbers representing the first index of the next topic; the first topic is assumed to be at index 0
  // Parameter: chosenIndexes - An object with topic name keys that each have an array value; each array will store the chosen KSAT indexes per topic
  // Parameter: addToCustomList - A boolean determining if the picked random KSATs should be in addition to the custom list or the minimum total per survey.
  //                              Note, only the custom KSAT list can cause the total per survey to be greater than this number.
  var startIndex = 0;
  topicSpanArray.forEach(function(nextTopicIndex){
    // In case more random questions are needed
    var numNeeded = numRandKsats;  // Preserve requested number of Random KSAT questions
    var numAdded = 0;
    var thisTopicLastIndex = nextTopicIndex - 1;
    if(thisTopicLastIndex < 0){
      // Avoid creating nagative indexes
      thisTopicLastIndex = 0;
    }
    var topic = csvData[thisTopicLastIndex][headers[topicKey]];
    var topicSize = nextTopicIndex - startIndex;  // The number of available KSATs in this topic
    if(!addToCustomList){
      // In case the resulting number of KSAT questions should equal the amount requested in numRandKsats
      if(numNeeded - chosenIndexes[topic].length >= 0){
        // Avoid numbers less than zero
        numNeeded -= chosenIndexes[topic].length;
      } else {
        numNeeded = 0;
      }
    }
    // Keep choosing a random KSAT until the max available KSATs, for this topic, have already been chosen or the requested number is reached
    while((chosenIndexes[topic].length < topicSize) && (numAdded < numNeeded)){
      var pick = getRandomInt(startIndex, nextTopicIndex);
      if(!chosenIndexes[topic].includes(pick)){
        // In case a unique KSAT was chosen, add it to its topic list
        chosenIndexes[topic].push(pick);
        ++numAdded;
      }
    }
    startIndex = nextTopicIndex;
  });
}

function getRandKsatRows(csvData, customKsatList, numRandKsats, topicSpanArray, addToCustomList){
  // This builds a new csvData object containing the custom and/or randomly chosen KSAT ID questions.
  // Parameter: csvData - A multidemensional array representing the survey data within a CSV file, minus the header row
  // Parameter: customKsatList - An array representing the list of custom KSATs to be included in the survey
  // Parameter: numRandKsats - A number representing the amount of random KSAT IDs to include in the survey
  //                           May be expressed as total per survey or in addition to a custom KSAT list
  // Parameter: topicSpanArray - An array of numbers representing the first index of the next topic; the first topic is assumed to be at index 0
  // Parameter: addToCustomList - A boolean determining if the picked random KSATs should be in addition to the custom list or the minimum total per survey.
  //                              Note, only the custom KSAT list can cause the total per survey to be greater than this number.
  // Return: newCsvData - A multidemensional array representing the chosen survey data from the CSV file, minus the header row
  var newCsvData = [];
  var chosenIndexes = getTopicMap(csvData, topicSpanArray);
  if(numRandKsats <= 0){
    // Handle numbers less than or equal to zero
    return newCsvData;
  }
  // Start with any requested custom KSATs
  addCustomKsatIndexes(csvData, customKsatList, chosenIndexes);
  // Go ahead and add all KSATs for topics that have less than or equal to the requested amount remaining
  addAllShortListTopicKsats(csvData, numRandKsats, topicSpanArray, chosenIndexes, addToCustomList)
  // Get the list of picked KSATs per topic; this will be stored in chosenIndexes
  addRandKsatIndexes(csvData, numRandKsats, topicSpanArray, chosenIndexes, addToCustomList);
  // Add Each picked KSAT to the newCsvData array
  Object.keys(chosenIndexes).forEach(function(topic){
    chosenIndexes[topic].forEach(function(csvRowIndex){
      newCsvData.push(csvData[csvRowIndex]);
    });
  });
  return newCsvData;
}

function parser(csvData, customKsats, numRandKsats, addToCustomList){
  // Given the csv data, this will parse the contents and make the directory structure and files then generate the form.
  // Parameter: csvData array of strings
  // Parameter: customKsats - An array of strings representing the KSAT IDs to include in the survey
  // Parameter: numRandKsats - A number representing the amount of random KSAT IDs to include in the survey
  //                           May be expressed as total per survey or in addition to a custom KSAT list
  // Parameter: addToCustomList - A boolean determining if the picked random KSATs should be in addition to the custom list or the minimum total per survey.
  //                              Note, only the custom KSAT list can cause the total per survey to be greater than this number.
  var module = (csvData[0][headers[moduleKey]]).toLowerCase();
  var topic = (csvData[0][headers[topicKey]]).toLowerCase();
  var subject = (csvData[0][headers[subjectKey]]).toLowerCase();
  var workrole = (csvData[0][headers[workroleKey]]).toLowerCase();
  
  // If the topic is not assigned, assign the subject.
  if (topic === " "){
    topic = subject;
    // Find topic span
    var scopeSpanArray = findSpan(csvData, headers[subjectKey]);
  } else {
    // Find topic span
    var scopeSpanArray = findSpan(csvData, headers[topicKey]);
  }
  // Determine if Custom or Random KSATs were requested
  if(customKsats !== undefined){
    // Since both Custom and Random options could consist of a custom KSAT list
    var newKsatsData = [];
    if(customKsats.length > 0 && numRandKsats === undefined){
      // If only custom KSATs are requested, then strip out all survey data that's not desired
      newKsatsData = getKsatRows(csvData, customKsats);
    } else if(numRandKsats !== undefined && addToCustomList !== undefined){
      // If there's a request for random KSAT questions per topic, in addition to any custom KSATs
      newKsatsData = getRandKsatRows(csvData, customKsats, numRandKsats, scopeSpanArray, addToCustomList);
    }
    if(newKsatsData.length > 0){
      // Allows retaining the orginal csvData should the newKsatsData return empty
      csvData = newKsatsData;
    }
    // Since the original rows are now different, a new scopeSpanArray needs built
    var neededIndex = headers[topicKey];
    if(csvData[0][headers[topicKey]] === ""){
      neededIndex = headers[subjectKey];
    }
    scopeSpanArray = findSpan(csvData, neededIndex);
  }
  
  makeDirStructure(workrole, module);
  
  // Start generating surveys
  generateSurveys(csvData, scopeSpanArray, module, topic);
}

function singleFile(file, ksatsToGen, customKsats, numRandKsats, addToCustomList){
  // Drives given a single file
  // Parameter: file - string of file name
  // Parameter: ksatsToGen - A string representing the method of KSAT generatation
  // Parameter: customKsats - An array of strings representing the KSAT IDs to include in the survey
  // Parameter: numRandKsats - A number representing the amount of random KSAT IDs to include in the survey
  //                           May be expressed as total per survey or in addition to a custom KSAT list
  // Parameter: addToCustomList - A boolean determining if the picked random KSATs should be in addition to the custom list or the minimum total per survey.
  //                              Note, only the custom KSAT list can cause the total per survey to be greater than this number.
  Browser.msgBox("File selected: "+file);
  var csvData = openAndReadCSV(file);
  // headers is an array of strings(header information)
  var headers = csvData[0];
  // ksatsData is an array of arrays with the ksat item information
  var ksatsData =  csvData[1];
  if(ksatsToGen === "All_KSATs"){
    parser(ksatsData);
  } else if(ksatsToGen === "Custom_KSATs"){
    parser(ksatsData, customKsats);
  } else if(ksatsToGen === "Random_KSATs"){
    parser(ksatsData, customKsats, numRandKsats, addToCustomList);
  }
}

function allCSVs(){
  // Drives using all the files from the course survey directory.
  var csvFiles = getCSVsInFolder(courseFolderName);
  Browser.msgBox("Files found: "+csvFiles);
  // Start looping through all the csv's
  for(var i = 0; i < csvFiles.length; i++){
    // Parse the csv to get data and number of surveys needed
    var csvData = openAndReadCSV(csvFiles[i]);
    headers = csvData[0];
    var ksatsData = csvData[1]
    parser(ksatsData);
  }
}

function getType(elem) {
  // A more reliable way to determine an Object's type; the '.slice(8, -1)' removes the '[object ' and ']' from a returned string of '[object Object]'
  // Curtosy of Todd Motto, Google Developer Expert
  // Parameter: elem - an object
  // Return: string - The name of the object's type
  return Object.prototype.toString.call(elem).slice(8, -1);
}

function isValidInput(genSurveyOption, ksatsToGen, customKsatList, numRandKsats, addToCustomList){
  // This validates user input to see if the user specified all necessary values
  // Parameter: genSurveyOption - A string representing the survey name to generate, or 'All' to generate every survey
  // Parameter: ksatsToGen - A string representing the method of KSAT generatation
  // Parameter: customKsatList - An array of strings representing the KSAT IDs to include in the survey
  // Parameter: numRandKsats - A number representing the amount of random KSAT IDs to include in the survey
  //                           May be expressed as total per survey or in addition to a custom KSAT list
  // Parameter: addToCustomList - A boolean determining if the picked random KSATs should be in addition to the custom list or the minimum total per survey.
  //                              Note, only the custom KSAT list can cause the total per survey to be greater than this number.
  // Return: isValid - A boolean returning true if the user entered all necessary information for survey generation
  // Set default values in case the parameters are not passed
  if(genSurveyOption === undefined){
    genSurveyOption = "";
  }
  if(ksatsToGen === undefined){
    ksatsToGen = "";
  }
  if(customKsatList === undefined){
    customKsatList = [];
  }
  if(numRandKsats === undefined){
    numRandKsats = 0;
  }
  if(addToCustomList === undefined){
    addToCustomList = false;
  }
  var isValid = false;
  if(genSurveyOption !== "All" && genSurveyOption !== ""){
    // Validates necessary input for options other than 'All', which needs to know if All, Custom or Random KSATs are used
    if(ksatsToGen === "All_KSATs"){
      // Validates necessary input for the 'All_KSATs' option, which needs no other input
      isValid = true;
    } else if(ksatsToGen === "Custom_KSATs" &&
       getType(customKsatList) === "Array" && customKsatList.length > 0){
      // Validates necessary input for the 'Custom_KSATs' option, which needs an array of KSAT IDs
      isValid = true;
    } else if(ksatsToGen === "Random_KSATs" &&
       getType(customKsatList) === "Array" && customKsatList.length >= 0 &&
       getType(numRandKsats) === "Number" && numRandKsats > 0 && 
       getType(addToCustomList) === "Boolean"){
      // Validates necessary input for the 'Random_KSATs' option, which needs: 
      //     an optional array of KSAT IDs, 
      //     a number greater than zero for random KSAT generation, 
      //     and a boolean to see if the randomly chosen list should be in addition to the custom list or a part of it
      isValid = true;
    }
  } else if(genSurveyOption === "All"){
    // Validates necessary input for the 'All' option, which needs no other input
    isValid = true;
  }
  return isValid;
}

function validateInput(genSurveyOption, ksatsToGen, customKsatList, numRandKsats, addToCustomList){
  // This validates user input and displays a message to help users understand what information is needed
  // Parameter: genSurveyOption - A string representing the survey name to generate, or 'All' to generate every survey
  // Parameter: ksatsToGen - A string representing the method of KSAT generatation
  // Parameter: customKsatList - An array of strings representing the KSAT IDs to include in the survey
  // Parameter: numRandKsats - A number representing the amount of random KSAT IDs to include in the survey
  //                           May be expressed as total per survey or in addition to a custom KSAT list
  // Parameter: addToCustomList - A boolean determining if the picked random KSATs should be in addition to the custom list or the minimum total per survey.
  //                              Note, only the custom KSAT list can cause the total per survey to be greater than this number.
  // Return: msg - A string representing the message from validation
  // Determine if chosen options have necessary data
  if(!isValidInput(genSurveyOption, ksatsToGen, customKsatList, numRandKsats, addToCustomList)){
    if(genSurveyOption === ""){
      msg = "You must choose an option to 'Generate all surveys or select a single one'.";
    } else if(genSurveyOption !== "All" && ksatsToGen === ""){
      msg = "When selecting a single survey, you must 'choose the amount of KSATs to include'.";
    } else if(genSurveyOption !== "All" && ksatsToGen === "Custom_KSATs" && customKsatList.length === 0){
      msg = "When selecting a single survey and custom KSATs, you must 'enter a comma seperated list of the desired KSAT IDs'.";
    } else if(genSurveyOption !== "All" && ksatsToGen === "Random_KSATs" && (numRandKsats === "" || numRandKsats <= 0)){
      msg = "When selecting a single survey and Random KSATs, you must enter a 'number of random KSATs to use' that's greater than zero.";
    } else if(genSurveyOption !== "All" && ksatsToGen === "Random_KSATs" && addToCustomList === ""){
      msg = "When selecting a single survey and Random KSATs, you must answer the question 'Make the random KSAT list an addition to the custom list?'.";
    } else {
      // Execution should never get here but, in case it does, some kind of helpful message will appear
      msg = "One or more required items is missing. Please ensure you have entered all required information.";
    }
    Browser.msgBox(msg);
  } else {
    msg = "VALID"
  }
  return msg
}

function main(){
  // Check to see if the CouseSurveys directory exists
  if (checkForFolder(courseFolderName) === false){
    return;
  }
  var genSurveyOptionCell = "B3";
  var ksatsToGenCell = "B4";
  var customKsatListCell = "B5";
  var numRandKsatsCell = "B6";
  var addToCustomListCell = "B7";
  
  // Used for testing outside of the sheet UI
//  singleFile("scrum.org-professional-scrum-product-owner-i_trng_surveys");
//  allCSVs();
  ////////////////////////////////////////////
    
  // Check the user input from the sheet
  var sheet = SpreadsheetApp.getActiveSheet();
  var genSurveyOption = sheet.getRange(genSurveyOptionCell).getValue();
  var ksatsToGen = sheet.getRange(ksatsToGenCell).getValue();
  var customKsatList = sheet.getRange(customKsatListCell).getValue();
  var numRandKsats = sheet.getRange(numRandKsatsCell).getValue();
  var addToCustomList = sheet.getRange(addToCustomListCell).getValue();
  // Ensure the custom KSAT list is an array
  if(customKsatList !== ""){
    customKsatList = customKsatList.replace(/\s/g, "").split(",");
    customKsatList.pop();  // Since the last element will be an empty string
  } else {
    customKsatList = [];
  }
  // Ensure the addToCustomList variable stores boolean values
  if(addToCustomList === "Yes"){
    addToCustomList = true;
  } else if(addToCustomList === "No"){
    addToCustomList = false;
  }
  // Determine if chosen options have necessary data
  if(validateInput(genSurveyOption, ksatsToGen, customKsatList, numRandKsats, addToCustomList) !== "VALID"){
    return;
  }
  // Clear customKsatList if 'All' options are chosen to avoid iterating the list unnecessarily
  if(genSurveyOption === "All" || ksatsToGen === "All_KSATs"){
    customKsatList = [];
  }
  // Specific file or all
  if(genSurveyOption === "All"){
    allCSVs();
  } else {
    var csvFile = sheet.getRange(genSurveyOptionCell).getValue();
    singleFile(csvFile, ksatsToGen, customKsatList, numRandKsats, addToCustomList);
  }
}
